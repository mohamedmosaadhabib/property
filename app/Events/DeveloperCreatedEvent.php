<?php

namespace App\Events;

use App\Models\Developer;
use Illuminate\Foundation\Events\Dispatchable;

class DeveloperCreatedEvent {
    use Dispatchable;

    public $developer;

    public function __construct(Developer $developer) {

        $this->developer = $developer;
    }
}
