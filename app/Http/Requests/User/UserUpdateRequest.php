<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    public function rules(){
        return [
            'name' => ['sometimes:string:max:255'],
            'email' => ['sometimes:string:email:max:255', Rule::unique('users')->ignore(request()->route()->parameter('user'))],
            'is_admin' => ['sometimes:boolean'],
        ];
    }

    public function authorize()
    {
        return true;
    }
}
