<?php

namespace App\Http\Requests\Owner;

use Illuminate\Foundation\Http\FormRequest;

class OwnerCreateRequest extends FormRequest
{
    public function rules()
    {
        return [
            ""
        ];
    }

    public function authorize()
    {
        return true;
    }
}
