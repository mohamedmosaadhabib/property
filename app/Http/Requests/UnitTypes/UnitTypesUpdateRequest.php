<?php

namespace App\Http\Requests\UnitTypes;

use Illuminate\Foundation\Http\FormRequest;

class UnitTypesUpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            //
        ];
    }

    public function authorize()
    {
        return true;
    }
}
