<?php

namespace App\Http\Requests\Blog;

use Illuminate\Foundation\Http\FormRequest;

class BlogUpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            "name"=>['sometimes:array:size:2'],
            "name.*"=>['sometimes:min:3:max:255'],
            "description"=>['sometimes:array:size:2'],
            "description.*"=>['sometimes:min:3'],
            "image"=>['sometimes:image'],
        ];
    }

    public function authorize()
    {
        return true;
    }
}
