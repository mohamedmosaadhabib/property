<?php

namespace App\Http\Requests\Blog;

use Illuminate\Foundation\Http\FormRequest;

class BlogCreateRequest extends FormRequest
{
    public function rules()
    {
        return [
            "name"=>['required','array','size:2'],
            "name.*"=>['required','min:3','max:255'],
            "description"=>['required','array','size:2'],
            "description.*"=>['required','min:3'],
            "image"=>['required','image'],
        ];
    }

    public function authorize(){
        return true;
    }
}
