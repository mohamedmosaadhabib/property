<?php

namespace App\Http\Requests\Slider;

use Illuminate\Foundation\Http\FormRequest;

class SliderCreateRequest extends FormRequest
{
    public function rules()
    {
        return [
            "image"=>['sometimes:image:max:2048'],
            "order_by"=>['sometimes','unique:sliders,order_by',]
        ];
    }

    public function authorize()
    {
        return true;
    }
}
