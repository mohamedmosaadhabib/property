<?php

namespace App\Http\Requests\Slider;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SliderUpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            "image"=>['sometimes:image:max:2048'],
            "order_by"=>['sometimes',Rule::unique('sliders')->ignore(request()->route()->parameter('slider'))]
        ];
    }

    public function authorize()
    {
        return true;
    }
}
