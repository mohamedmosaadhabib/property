<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactCreateRequest extends FormRequest {

    public function rules() {
        return [
            "name"=>['required','min:3','max:255'],
            "email"=>['required','email','min:5','max:255'],
            "subject"=>['required','min:5','max:255'],
            "message"=>['required','min:5','max:255'],
            "project_id"=>['nullable','exists:projects,id'],
        ];
    }

    public function authorize() { return true; }

}
