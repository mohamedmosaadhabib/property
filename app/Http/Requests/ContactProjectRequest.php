<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactProjectRequest extends FormRequest {

    public function rules() {
        return [
            "name"=>['required','min:5','max:255'],
            "email"=>['required','email','min:5','max:255'],
            "phone"=>['required','min::11',"max:16"],
            "message"=>['required','min:5'],
        ];
    }

    public function authorize() { return true; }
}
