<?php

namespace App\Http\Requests\Region;

use Illuminate\Foundation\Http\FormRequest;

class RegionCreateRequest extends FormRequest
{
    public function rules()
    {
        return [
            "name"=>["required",'array',"size:2"],
            "name.*"=>["min:3","max:255"]
        ];
    }

    public function authorize()
    {
        return true;
    }
}
