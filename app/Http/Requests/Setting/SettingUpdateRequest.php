<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class SettingUpdateRequest extends FormRequest
{
    public function rules(){
        return [
            "value"=>["required",'min:1']
        ];
    }

    public function authorize()
    {
        return true;
    }
}
