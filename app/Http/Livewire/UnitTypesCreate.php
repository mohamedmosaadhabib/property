<?php

namespace App\Http\Livewire;

use App\Models\UnitTypes;
use Livewire\Component;

class UnitTypesCreate extends Component{

    public $name;
    public $edit=false;
    public $unitTypes;
    public $unit_types_id;
    private $validateUpdate=[
        "name"=>['sometimes:array:size:2'],
        "name.*"=>['sometimes:min:3'],
        "unit_types_id"=>['sometimes:exists:unitTypes,id'],
    ];
    private $validateCreate=[
        "name"=>['required','array','size:2'],
        "name.*"=>['required','min:3'],
        "unit_types_id"=>['sometimes:exists:unitTypes,id'],
    ];

    public function mount($edit=false,UnitTypes $unitTypes=null){
        $this->edit=(bool)$edit;
        if ($this->edit){
            $this->name['ar']=$unitTypes->getTranslation('name', 'ar');
            $this->name['en']=$unitTypes->getTranslation('name', 'en');
            $this->unit_types_id=$unitTypes->unit_types_id;
            $this->unitTypes=$unitTypes;

        }
    }
    public function updated($field){ $this->validateOnly($field,$this->edit?$this->validateUpdate:$this->validateCreate); }

    public function createUnitTypes(){
        $validated=$this->validate($this->validateCreate);
        foreach($validated as $k=> $data){
            if(empty($data) || is_null($data)) unset($validated[$k]);
        }
        UnitTypes::create($validated);
        $this->name['ar']=null;
        $this->name['en']=null;
        session()->flash('success',__('main.unitTypes_created'));
    }

    public function updateUnitTypes(){
        $validated=$this->validate($this->validateUpdate);
        $this->unitTypes->update($validated);
        session()->flash('success',__('main.unitTypes_updated'));

    }
    public function render()
    {
        return view('livewire.unit-types-create');
    }
}
