<?php

namespace App\Http\Livewire;

use Livewire\Component;

class RegionIndex extends Component
{
    public function render()
    {
        return view('livewire.region-index');
    }
}
