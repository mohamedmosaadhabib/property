<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PropertyIndex extends Component
{
    public function render()
    {
        return view('livewire.property-index');
    }
}
