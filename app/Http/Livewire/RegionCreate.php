<?php

namespace App\Http\Livewire;

use App\Models\Region;
use Livewire\Component;
use Livewire\WithFileUploads;

class RegionCreate extends Component{
    use WithFileUploads;
    public $name;
    public $edit=false;
    public $region;
    public $type=2;
    public $image;
    public $show=false;
    public $region_id=null;
    private $validateUpdate=[
        "name"=>['sometimes:array:size:2'],
        "name.*"=>['sometimes:min:3'],
        "type"=>['sometimes:in:2,3,4'],
        "image"=>['sometimes:image'],
        "show"=>['sometimes:boolean'],
        "region_id"=>['nullable:exists:regions,id'],
    ];
    private $validateCreate=[
        "name"=>['required','array','size:2'],
        "name.*"=>['required','min:3'],
        "type"=>['required','in:2,3,4'],
        "image"=>['sometimes:image'],
        "show"=>['required','boolean'],
        "region_id"=>['nullable:exists:regions,id'],
    ];
    public $urlCurrent;

    public function mount($edit=false,Region $region=null){
        $this->urlCurrent=url()->current();
        $this->edit=(bool)$edit;
        if ($this->edit){
            $this->name['ar']=$region->getTranslation('name', 'ar');
            $this->name['en']=$region->getTranslation('name', 'en');
            $this->region_id=$region->region_id;
            $this->type=$region->type ?? null;
            $this->show=$region->show ?? false;
            $this->region=$region;
        }
    }

    public function updated($field){
        if ($field=="type")$this->region_id='';
        $this->validateOnly($field,$this->edit?$this->validateUpdate:$this->validateCreate);
    }

    public function createRegion(){
        $validated=$this->validate($this->validateCreate);
        if (isset($validated['image'])) $validated['image']=uploader($this->image,'uploads',['required','image','max:2048']);
        foreach($validated as $k=> $data) if(empty($data) || is_null($data)) unset($validated[$k]);

        $validated['region_id']=$validated['region_id'] ?? null;
        $validated['show']=$this->show===true;
        Region::create($validated);
        $this->name['ar']=null;
        $this->name['en']=null;
        $this->type=1;
        session()->flash('success',__('main.region_created'));
        $this->redirect($this->urlCurrent);
    }

    public function updateRegion(){
        $validated=$this->validate($this->validateUpdate);
        if (isset($validated['image'])) $validated['image']=uploader($this->image,'uploads',['required','image','max:2048']);
        foreach($validated as $k=> $data) if(empty($data) || is_null($data)) unset($validated[$k]);
        $validated['show']=$this->show===true;
        $this->region->update($validated);
        session()->flash('success',__('main.region_updated'));
        $this->redirect($this->urlCurrent);
    }

    public function toggleActive(bool $status)
    {
        if ($this->edit) {
            $this->region->update(['show'=>$status]);
        }
        $this->show = $status;
    }
    public function render(){

        return view('livewire.region-create');
    }
}
