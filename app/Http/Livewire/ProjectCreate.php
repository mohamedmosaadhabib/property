<?php

namespace App\Http\Livewire;

use App\Models\Project;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithFileUploads;

class ProjectCreate extends Component{
    use WithFileUploads;
    public $name;
    public $delivery_date;
    public $types;
    public $down_payment;
    public $price;
    public $description;
    public $image;
    public $photos=[];
    public $developer_id;
    public $currency;
    public $location=[
        "region"=>'',
        "city"=>""
    ];
    public $edit=false;
    /**
     * @var Project  project
     */
    public $project;
    public $tap='base';
    public $type=[
        "min"=>null,
        "max"=>null,
        "price"=>null,
        "unit_type"=>null
    ];

    private $validateUpdate=[
        "name"=>['sometimes:array:size:2'],
        "name.*"=>['sometimes:min:3'],
        "currency"=>['sometimes:string:min:3'],
        "types"=>['sometimes:array'],
        "types.*.unit_type"=>['sometimes:string'],
        "types.*.max"=>['sometimes:numeric'],
        "types.*.min"=>['sometimes:numeric'],
        "types.*.price"=>['sometimes:numeric'],
        "price"=>['sometimes:numeric'],
        "delivery_date"=>['sometimes:date'],
        "down_payment"=>['sometimes:numeric'],
        "developer_id"=>['sometimes:exists:developers,id'],
        "photos"=>['sometimes:array'],
        "photos.*"=>['sometimes:image:max:2048'],
        "installments_years"=>['sometimes:numeric'],
        "description"=>['sometimes:size:2:array'],
        "location"=>['sometimes:array'],
        "location.city"=>['sometimes:exists:regions,id'],
        "location.region"=>['sometimes:exists:regions,id'],
        "description.*"=>['sometimes:min:3'],
    ];
    private $validateCreate=[
        "name"=>['required','array','size:2'],
        "name.*"=>['required','min:3'],
        "currency"=>['required','string','min:3'],
        "types"=>['required','array'],
        "types.*.unit_type"=>['required','string'],
        "types.*.max"=>['required','numeric'],
        "types.*.min"=>['required','numeric'],
        "types.*.price"=>['required','numeric'],
        "price"=>['required','numeric'],
        "delivery_date"=>['required','date'],
        "down_payment"=>['required','numeric'],
        "developer_id"=>['required','exists:developers,id'],
        "photos"=>['sometimes:array'],
        "photos.*"=>['sometimes:image:max:2048'],
        "description"=>['required','size:2','array'],
        "description.*"=>['required','min:3'],
        "installments_years"=>['required','numeric'],
        "location"=>['required','array'],
        "location.city"=>['required','exists:regions,id'],
        "location.region"=>['required','exists:regions,id'],
    ];
    public $installments_years;
    public $images=[];
    /**
     * @var string
     */
    public $urlCurrent;

    public function mount($edit=false,Project $project=null){
        $this->urlCurrent=url()->current();
        $this->edit=(bool)$edit;
        if ($this->edit){
            $project->makeHidden(['id','photos']);
            foreach ($project->toArray() as $key=> $item) $this->{$key}=$item;
            $this->delivery_date=Carbon::parse($project->delivery_date)->format('Y-m-d');
            $this->images=$project->photos ?? [];
            $this->project=$project;
        }
    }
    public function updated($field){ $this->validateOnly($field,$this->edit?$this->validateUpdate:$this->validateCreate); }

    public function createProject(){
        $validated=$this->validate($this->validateCreate);

        if (isset($validated['photos'])){
            unset($validated['photos']);
            foreach ($this->photos as $photo) {
                $validated['photos'][]=uploader($photo,'uploads',['required','image','max:2048']);
            }
        }
        foreach($validated as $k=> $data){
            if(empty($data) || is_null($data)) unset($validated[$k]);
        }
        Project::create($validated);
        foreach ($validated as $index => $value) if (property_exists($this, $index))  $this->{$index}=is_array($index)?[]:null;
        session()->flash('success',__('main.project_created'));
        $this->redirect($this->urlCurrent);
    }

    public function updateProject(){
        $validated=$this->validate($this->validateUpdate);
        if (isset($validated['photos']) && count($validated['photos'])>0){
            unset($validated['photos']);
            foreach ($this->photos as $photo) {
                $validated['photos'][]=uploader($photo,'uploads',['required','image','max:2048']);
            }
            $validated['photos']=collect($validated['photos'])->merge($this->images);
        }
        foreach($validated ?? [] as $k=> $data){
            if(empty($data) || is_null($data)) unset($validated[$k]);
        }
        $this->project->update($validated);
        session()->flash('success',__('main.project_updated'));
        $this->redirect($this->urlCurrent);
    }

    public function addType(){
        $type = $this->type;
        if (is_array($type)) {
            $this->types[]=$type;
        }
    }
    public function NewImageDelete($key=null){
        if (isset($this->photos[$key])) {
            $photo=$this->photos[$key];
            unset($this->photos[$key]);
            $photo->delete();
        }
    }

    public function deleteType($index){
        if (isset($this->types[$index])) unset($this->types[$index]);
    }

    public function ImageDelete($key=null){
        if (isset($this->photos[$key])) unset($this->photos[$key]);
        $this->project->photos=$this->photos;
        $this->project->save();
    }
    public function primaryImage($key)
    {
        $this->project->update(['image' => $key]);
        $this->image=$key;
    }
    public function changeTap(string $string='base')
    {
        $this->tap=$string;
    }
    public function render() { return view('livewire.project-create',[
        "unit_types"=>\App\Models\UnitTypes::latest()->whereNull('unit_types_id')->with('units')->get()
    ]); }

}
