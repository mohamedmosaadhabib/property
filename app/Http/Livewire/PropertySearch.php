<?php

namespace App\Http\Livewire;

use App\Models\Property;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class PropertySearch extends Component{
    use WithPagination;
    public $search;
    public $minimum=0;
    public $maximum=0;
    public $bedrooms;
    public $bathrooms;
    public $ptypes;
    public $region_id;
    public $typeOfPage='';
    protected $updatesQueryString=[
        "search"=>["except"=>""],
        "minimum"=>["except"=>0],
        "maximum"=>["except"=>0],
        "bedrooms"=>["except"=>""],
        "bathrooms"=>["except"=>""],
        "region_id"=>["except"=>""],
        "ptypes"=>["only"=>Property::TYPES,"except"=>""],
    ];

    public function mount(){
        $this->typeOfPage=explode('?', url()->current())[0] == route('rent') ? 'rent' :'resale';
        if ($this->typeOfPage == 'rent'){
            $types= [ "rent_unfurnished" , "rent_furnished"  , "rent_semi_furnished" , ];

        }
        if ($this->typeOfPage == 'resale'){
            $types= ['resale','sale'];
        }

        $this->search=request('search');
        $this->minimum=request('minimum');
        $this->maximum=request('maximum');
        $this->bedrooms=request('bedrooms');
        $this->bathrooms=request('bathrooms');
        $this->ptypes=request('ptypes',$types[0] ?? '');
        $this->region_id=request('region_id');
    }
    public function render() {
        $properties=Property::latest();
        if ($this->typeOfPage == 'rent'){
            $types= [ "rent_unfurnished" , "rent_furnished"  , "rent_semi_furnished" , ];
            $properties->filterRent();
        }
        if ($this->typeOfPage == 'resale'){
            $types= ['resale','sale'];
            $properties->filterSale();
        }

        $properties
            ->when($this->search,   function (Builder $builder){ return $builder->where('name->'.app()->getLocale(),'LIKE','%'.$this->search.'%'); })
            ->when($this->bedrooms, function (Builder $builder){ return $builder->where('bed_rooms',$this->bedrooms); })
            ->when($this->bedrooms, function (Builder $builder){ return $builder->where('bed_rooms',$this->bedrooms); })
            ->when($this->region_id,function (Builder $builder){ return $builder
                ->where(function (Builder $builder){
                    return $builder
                        ->orWhere('location->city',$this->region_id)
                        ->orWhere('location->region',$this->region_id)
                        ->orWhere('location->street',$this->region_id);
                }); })

            ->when($this->ptypes && $this->minimum , function (Builder $builder){ return $builder->where($this->ptypes.'->value','>',$this->minimum); })
            ->when($this->ptypes && $this->maximum , function (Builder $builder){ return $builder->where($this->ptypes.'->value','<',$this->maximum); })
//            ->when($this->ptypes && $this->maximum && $this->minimum, function (Builder $builder){ return $builder->whereBetween($this->ptypes.'->value',[$this->minimum,$this->maximum]); })
//            ->dd()
        ;
        $counter=$properties->count();
        $properties=$properties->paginate();


        return view('livewire.property-search',compact('counter','properties','types'));
    }
}
