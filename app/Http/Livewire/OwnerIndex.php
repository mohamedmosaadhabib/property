<?php

namespace App\Http\Livewire;

use App\Models\Owner;
use Livewire\Component;

class OwnerIndex extends Component
{
    public function render()
    {
        return view('livewire.owner-index', [
            "owners"=>Owner::paginate()
        ]);
    }
}
