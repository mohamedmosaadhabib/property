<?php

namespace App\Http\Livewire;

use App\Models\Project;
use Livewire\Component;

class ProjectIndex extends Component
{
    public function render()
    {
        return view('livewire.project-index',[
            "projects"=>Project::paginate()
        ]);
    }
}
