<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SearchBox extends Component{
    public $select="property";
    public $ptypes;
    public $city;
    public $maximum;
    public $minimum;
    public $search;

    public function goUrl(){
        if($this->select==="property"){
            ($this->redirect(route('rent_resale').'?search='.$this->search.'&region_id='.$this->city.'&minimum='.$this->minimum.'&maximum='.$this->maximum.'&ptypes='.$this->ptypes));
        }else{
            ($this->redirect(route('project').'?search='.$this->search.'&region_id='.$this->city.'&minimum='.$this->minimum.'&maximum='.$this->maximum.'&unit_type_id='.$this->ptypes));
        }
    }
    public function render(){
        return view('livewire.search-box');
    }
}
