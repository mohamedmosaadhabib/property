<?php

namespace App\Http\Livewire;

use App\Models\Owner;
use Livewire\Component;

class OwnerCreate extends Component{
    public $name;
    public $email;
    public $phone;
    public $address;
//    public $image;
    public $edit=false;
    public $owner;
    private $validateUpdate=[
        "name"=>['sometimes:array:size:2'],
        "name.*"=>['sometimes:min:3'],
        "email"=>['sometimes:email:filter:min:3:max:255'],
        "phone"=>['sometimes:size:11'],
    ];
    private $validateCreate=[
        "name"=>['required','array','size:2'],
        "name.*"=>['required','min:3'],
        "email"=>['sometimes:email:filter:min:3:max:255'],
        "phone"=>['required','size:11'],
    ];
    public $urlCurrent;
    public function mount($edit=false,Owner $owner=null){
        $this->urlCurrent=url()->current();
        $this->edit=(bool)$edit;
        if ($this->edit){
            $this->name['ar']=$owner->getTranslation('name', 'ar');
            $this->name['en']=$owner->getTranslation('name', 'en');
            $this->email=$owner->email;
            $this->phone=$owner->phone;
            $this->owner=$owner;
        }
    }
    public function updated($field){ $this->validateOnly($field,$this->edit?$this->validateUpdate:$this->validateCreate); }

    public function createOwner(){
        $validated=$this->validate($this->validateCreate);
        foreach($validated as $k=> $data){
            if(empty($data) || is_null($data)) unset($validated[$k]);
        }
        Owner::create($validated);
        $this->name=null;
        $this->email=null;
        $this->phone=null;
        $this->address=null;
        session()->flash('success',__('main.owner_created'));
        $this->redirect($this->urlCurrent);
    }

    public function updateOwner(){
        $validated=$this->validate($this->validateUpdate);
        foreach($validated as $k=> $data){
            if(empty($data) || is_null($data)) unset($validated[$k]);
        }
        $this->owner->update($validated);
        session()->flash('success',__('main.owner_updated'));
        $this->redirect($this->urlCurrent);
    }

    public function render() { return view('livewire.owner-create'); }

}
