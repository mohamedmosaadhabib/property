<?php

namespace App\Http\Livewire;

use App\Events\DeveloperCreatedEvent;
use App\Models\Developer;
use Livewire\Component;
use Livewire\WithFileUploads;

class DeveloperCreate extends Component {
    use WithFileUploads;
    public $name;
    public $image;
    public $description;

    protected $casts=[];
    private $validateCreate=[
        "name"=>['required','array'],
        "image"=>['required:image:max:2048'],
        "description"=>['required','array'],
    ];
    public $edit=false;

    public $developer;
    private $validateUpdate=[
        "name"=>['sometimes:array'],
        "image"=>['sometimes:image:max:2048'],
        "description"=>['sometimes:array'],
    ];
    public $urlCurrent;

    public function mount($edit=false,Developer $developer=null){
        $this->urlCurrent=url()->current();
        $this->edit=(bool)$edit;
        if ($this->edit){
            $this->name['ar']=$developer->getTranslation('name', 'ar');
            $this->name['en']=$developer->getTranslation('name', 'en');
            $this->description['ar']=$developer->getTranslation('description', 'ar');
            $this->description['en']=$developer->getTranslation('description', 'en');
            $this->developer=$developer;
        }
    }
    public function updated($field){$this->validateOnly($field,$this->edit?$this->validateUpdate:$this->validateCreate); }

    public function addDeveloper(){
        $validated=$this->validate($this->validateCreate);
        if (isset($validated['image'])) $validated['image']=uploader($this->image,'uploads',['required','image','max:2048']);
        foreach($validated as $k=> $data) if(empty($data) || is_null($data)) unset($validated[$k]);
        Developer::create($validated);
        session()->flash('success',__('main.developer_created'));
        foreach ($validated as $key => $value)  if (property_exists($this, $key)) $this->{$key}=null;
        $this->redirect($this->urlCurrent);
    }

    public function render() { return view('livewire.developer-create'); }

    public function editDeveloper(){
        $validated=$this->validate($this->validateUpdate);
        if (isset($validated['image'])) $validated['image']=uploader($this->image,'uploads',['required','image','max:2048']);
        foreach($validated as $k=> $data) if(empty($data) || is_null($data)) unset($validated[$k]);

        $this->developer->update($validated);
        session()->flash('success',__('main.developer_updated'));
        $this->redirect($this->urlCurrent);
    }

}
