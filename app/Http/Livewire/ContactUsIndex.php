<?php

namespace App\Http\Livewire;

use App\Models\Contact;
use Livewire\Component;
use Livewire\WithPagination;

class ContactUsIndex extends Component {
    use WithPagination;

    public function render(){
        return view('livewire.contact-us-index',['contacts'=>Contact::paginate()]);
    }
}
