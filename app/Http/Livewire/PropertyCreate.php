<?php

namespace App\Http\Livewire;

use App\Models\Owner;
use App\Models\Property;
use App\Models\Region;
use Livewire\Component;
use Livewire\TemporaryUploadedFile;
use Livewire\WithFileUploads;

class PropertyCreate extends Component
{
    use WithFileUploads;

    public $name;
    public $building_number;
    public $floor_number;
    public $apartment_number;
    public $property_type;
    public $property_views;
    public $currency;
    public $region_id;
    public $owner_id;
    public $bed_rooms = 1;
    public $bathrooms = 1;
    public $images = [];
    public $photos = [];
    public $image = null;
    public $description;
    public $address;
    public $notes;
    public $edit = false;
    /**
     * @var Property  Property
     */
    public $property;
    public $photo;
    public $tag;
    public $tags = [];
    public $cities = [];
    public $streets = [];
    public $tap = 'base';
    public $location = [
        "city" => "",
        "region" => "",
        "street" => "",
    ];
    public $sale = [
        "check" => false,
        "value" => null
    ];
    public $resale = [
        "check" => false,
        "value" => null,
        "been_paid" => null,
        "delivery_date" => null
    ];
    public $rent_unfurnished = [
        "check" => false,
        "value" => null
    ];
    public $rent_furnished = [
        "check" => false,
        "value" => null
    ];
    public $rent_semi_furnished = [
        "check" => false,
        "value" => null
    ];
    public $active = true;

    public $rooms = [
        ["name" => "roof", "value" => 1]
    ];
    public $urlCurrent;
//    protected $casts = [];
    private $validateUpdate = [
        "name" => ['sometimes:array:size:2'],
        "name.*" => ['sometimes:min:3'],
        "address" => ['sometimes:size:1:array'],
        "address.*" => ['sometimes:min:3'],
        "description" => ['sometimes:size:2:array'],
        "description.*" => ['sometimes:min:3'],
        "rooms" => ['sometimes:array'],
        "rooms.*.name" => ['sometimes:string:min:3'],
        "rooms.*.value" => ['sometimes:numeric:min:1'],
        "building_number" => ['sometimes:numeric'],
        "floor_number" => ['sometimes:numeric'],
        "apartment_number" => ['sometimes:numeric'],
        "owner_id" => ['sometimes:exists:owners,id'],
        "property_type" => ['sometimes:in:apartment,twin_house,chalet,cafe,office,i_villa,penthouse,studio,town_house,ground_floor,duplex,villa'],
        "property_views" => ['sometimes:in:landscape,pool,sea,golf,lake,open_view,club,garden,nile'],
        "currency" => ['sometimes:in:egp,usd'],
        "images" => ['sometimes:array'],
        "images.*" => ['image:max:2048'],
        "resale.delivery_date" => ['sometimes:date'],
        "resale.value" => ['sometimes:numeric'],
        "resale.been_paid" => ['sometimes:numeric'],
        "active" => ['boolean'],
        "bed_rooms" => ['required', "min:1", 'numeric'],
        "bathrooms" => ['required', "min:1", 'numeric'],
        "sale" => ['sometimes:array'],
        "resale" => ['sometimes:array'],
        "rent_unfurnished" => ['sometimes:array'],
        "rent_furnished" => ['sometimes:array'],
        "rent_semi_furnished" => ['sometimes:array'],
        "tags" => ['sometimes:array'],
        "location" => ['sometimes:array'],
        "location.city" => ['sometimes:exists:regions,id'],
        "location.region" => ['sometimes:exists:regions,id'],
        "location.street" => ['sometimes:exists:regions,id'],
    ];
    private $validateCreate = [
        "name" => ['required', 'array', 'size:2'],
        "name.*" => ['required', 'min:3'],
        "address" => ['required', 'size:1', 'array'],
        "address.*" => ['required', 'min:3'],
        "description" => ['required', 'size:2', 'array'],
        "description.*" => ['required', 'min:3'],
        "rooms" => ['required', 'array'],
        "rooms.*.name" => ['sometimes:string:min:3'],
        "rooms.*.value" => ['sometimes:numeric:min:1'],
        "building_number" => ['sometimes:numeric:max:1000'],
        "floor_number" => ['sometimes:numeric:max:1000'],
        "apartment_number" => ['sometimes:numeric:max:1000'],
        "owner_id" => ['required', 'exists:owners,id'],
        "property_type" => ['required', 'in:apartment,twin_house,chalet,cafe,office,i_villa,penthouse,studio,town_house,ground_floor,duplex,villa'],
        "property_views" => ['required', 'in:landscape,pool,sea,golf,lake,open_view,club,garden,nile'],
        "currency" => ['required', 'in:egp,usd'],
        "images" => ['required', 'array'],
        "images.*" => ['image', 'max:2048'],
        "resale.delivery_date" => ['sometimes:date'],
        "resale.value" => ['sometimes:numeric'],
        "resale.been_paid" => ['sometimes:numeric'],
        "active" => ['boolean'],
        "bed_rooms" => ['required', "min:1", 'numeric'],
        "bathrooms" => ['required', "min:1", 'numeric'],
        "sale" => ['sometimes'],
        "resale" => ['sometimes:array'],
        "rent_unfurnished" => ['sometimes'],
        "rent_furnished" => ['sometimes'],
        "rent_semi_furnished" => ['sometimes'],
        "tags" => ['required', 'array'],
        "location" => ['required', 'array'],
        "location.city" => ['required', 'exists:regions,id'],
        "location.region" => ['required', 'exists:regions,id'],
        "location.street" => ['sometimes:exists:regions,id'],
    ];

    public function mount($edit = false, Property $property = null)
    {

        $this->urlCurrent = url()->current();
        $this->edit = (bool)$edit;
        if ($this->edit) {
            $property->makeHidden('id', 'images');
            foreach ($property->toArray() as $key => $value) if (property_exists($this, $key)) $this->{$key} = $value;
            $this->photos = $property->images;
            $this->property = $property;

        }
    }

    public function addroom()
    {
        $this->rooms[] = ["name" => "roof", "value" => 1];
    }

    public function updated($field)
    {
        if (preg_match('/location./',$field)) {
            $this->cities = optional(Region::find($this->location['region']))->children ?? [];
            $this->streets =optional(Region::find($this->location['city']))->children ?? [];
//            dd($this->cities,$this->streets);
        }
        $this->validateOnly($field, $this->edit ? $this->validateUpdate : $this->validateCreate);
    }

    public function createProperty()
    {
        $validated = $this->validate($this->validateCreate);
        if (isset($validated['images'])) {
            unset($validated['images']);
            foreach ($this->images as $image) if ($image instanceof TemporaryUploadedFile) $validated['images'][] = uploader($image);
        }
        foreach ($validated as $k => $data) {
            if (empty($data) || is_null($data)) unset($validated[$k]);
        }
        Property::create($validated);
        foreach ($validated as $key => $item) {
            $this->{$key} = null;
        }
        session()->flash('success', __('main.property_created'));
        $this->redirect($this->urlCurrent);
    }

    public function updateProperty()
    {

        $validated = $this->validate($this->validateUpdate);

        if (isset($validated['images'])) {
            unset($validated['images']);
            foreach ($this->images as $image) if ($image instanceof TemporaryUploadedFile) $validated['images'][] = uploader($image);
            if (isset($validated['images']) && count($validated['images']))
                $validated['images'] = collect($validated['images'])->merge($this->photos);
        }

        foreach ($validated as $k => $data) if (empty($data) || is_null($data)) unset($validated[$k]);

        $this->property->update($validated);
        session()->flash('success', __('main.property_updated'));
        $this->redirect($this->urlCurrent);
    }

    public function deleteroom($key)
    {
        if (isset($this->rooms[$key])) unset($this->rooms[$key]);
    }

    public function ImageDelete($key = null)
    {
        $photos = $this->property->images;
        if (isset($photos[$key])) unset($photos[$key]);
        if (isset($this->photos[$key])) unset($this->photos[$key]);
        $this->property->images = $photos;
        $this->property->save();
    }

    public function NewImageDelete($key = null)
    {
        if (isset($this->images[$key]))
            unset($this->images[$key]);
    }

    public function addtags()
    {
        $this->validate(['tag' => ['required', 'min:3']]);
        $this->tags[] = $this->tag;
        $this->tag = null;
    }

    public function deletetags($key)
    {
        if (isset($this->tags[$key])) unset($this->tags[$key]);
    }

    public function primaryImage($key)
    {
        if ($this->edit) {
            $this->property->update(['image' => $key]);
        }
        $this->image = $key;
    }

    public function changeTap(string $string = 'base')
    {
        $this->tap = $string;
    }

    public function render()
    {
        $owners = Owner::all();


        return view('livewire.property-create', compact('owners'));
    }

}
