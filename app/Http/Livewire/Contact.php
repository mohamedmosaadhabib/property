<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Contact extends Component {
    public $name;
    public $email;
    public $subject;
    public $message;
    public function updated($field)
    {
        $this->validateOnly($field, [
            "name"=>['min:3','max:255'],
            "email"=>['email:filter','min:5','max:255'],
            "subject"=>['min:5','max:255'],
            "message"=>['min:5','max:255'],
        ]);
    }

    public function addContact(){
        $validated=$this->validate([
            "name"=>['required','min:3','max:255'],
            "email"=>['required','email:filter','min:5','max:255'],
            "subject"=>['required','min:5','max:255'],
            "message"=>['required','min:5','max:255'],
        ]);

        \App\Models\Contact::create($validated);
        session()->flash( 'success', __(__('main.contact_success')) );
        $this->name=null;
        $this->email=null;
        $this->subject=null;
        $this->message=null;
    }
    public function render() {
        return view('livewire.contact');
    }
}
