<?php
namespace App\Http\Livewire;

use App\Models\Developer;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class Developers extends Component{
   use WithPagination;

   public function paginationView(){
       return 'livewire::pagination-links';
   }

    public $search;

   protected $updatesQueryString=[
        "search"=>["except"=>""]
    ];

    public function mount(){
        $this->search=request('search','');
    }

    public function render(){
        $text=strip_tags($this->search);
        return view('livewire.developers',[
            "developers"=> Developer::where(function (Builder $builder) use ($text) {
                return $builder
                    ->where('name','like','%'.$text.'%');
            })->orderBy('name')->paginate()
        ]);
    }
}
