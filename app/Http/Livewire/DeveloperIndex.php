<?php

namespace App\Http\Livewire;

use App\Models\Developer;
use Livewire\Component;

class DeveloperIndex extends Component {

    public function render() {
        return view('livewire.developer-index',[
            "developers"=>Developer::paginate()
        ]);
    }
}
