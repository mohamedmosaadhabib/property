<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\SettingCreateRequest;
use App\Http\Requests\Setting\SettingUpdateRequest;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingCtrl extends Controller{
    public function __construct(){ $this->middleware(['auth']); }

    public function index() { return view('dashboard.setting.index'); }

    public function update(SettingUpdateRequest $request, Setting $setting){
        $validated=$request->validated();
        //if ($request->hasFile(Setting::avatarSelectedFiled()??'image')) $validated[Setting::avatarSelectedFiled()??'image'];
        $setting->update($validated);
        alert()->success(__('main.setting'),__('main.updated'));
        return  back();

    }

    public function destroy(Setting $setting){
        $setting->delete();
        alert()->success(__('main.setting'),__('main.deleted'));
        return  back();
    }

}
