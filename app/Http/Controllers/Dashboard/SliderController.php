<?php


namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Slider\SliderCreateRequest;
use App\Http\Requests\Slider\SliderUpdateRequest;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller{
    public function __construct(){
        //$this->middleware(['']);
    }
    public function index() {
        $sliders=Slider::latest()->get();
        return view('dashboard.slider.index',compact('sliders'));
    }

    public function create(){ return view('dashboard.slider.create'); }


    public function store(SliderCreateRequest $request){
        $validated=$request->validated();
        if ($request->hasFile(Slider::avatarFiled()??'image')) $validated[Slider::avatarFiled()??'image']=uploader($request->file(Slider::avatarFiled()??'image'));
        Slider::create($validated);
        alert()->success(__('main.slider'),__('main.created'));
        return  back();
    }

    public function show(Slider $slider){ return view('dashboard.slider.show',compact('slider')); }

    public function edit(Slider $slider){ return view('dashboard.slider.edit',compact('slider')); }

    public function update(SliderUpdateRequest $request, Slider $slider){
        $validated=$request->validated();
        if ($request->hasFile(Slider::avatarFiled()??'image')) $validated[Slider::avatarFiled()??'image']=uploader($request->file(Slider::avatarFiled()??'image'));
        $slider->update($validated);
        alert()->success(__('main.slider'),__('main.updated'));
        return  back();

    }

    public function destroy(Slider $slider){
        $slider->delete();
        alert()->success(__('main.slider'),__('main.deleted'));
        return  back();
    }

}
