<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\OwnerCreateRequest;
use App\Http\Requests\Owner\OwnerUpdateRequest;
use App\Models\Owner;
use Illuminate\Http\Request;

class OwnerCtrl extends Controller{
    public function __construct(){
        //$this->middleware(['']);
    }
    public function index() { return view('dashboard.owner.index'); }

    public function create(){ return view('dashboard.owner.create'); }


    public function store(OwnerCreateRequest $request){
        $validated=$request->validated();
        //if ($request->hasFile(Owner::avatarSelectedFiled()??'image')) $validated[Owner::avatarSelectedFiled()??'image'];
        Owner::create($validated);
        alert()->success(__('main.owner'),__('main.created'));
        return  back();
    }

    public function show(Owner $owner){ return view('dashboard.owner.show',compact('owner')); }

    public function edit(Owner $owner){ return view('dashboard.owner.edit',compact('owner')); }

    public function update(OwnerUpdateRequest $request, Owner $owner){
        $validated=$request->validated();
        //if ($request->hasFile(Owner::avatarSelectedFiled()??'image')) $validated[Owner::avatarSelectedFiled()??'image'];
        $owner->update($validated);
        alert()->success(__('main.owner'),__('main.updated'));
        return  back();

    }

    public function destroy(Owner $owner){
        $owner->delete();
        alert()->success(__('main.owner'),__('main.deleted'));
        return  back();
    }

}
