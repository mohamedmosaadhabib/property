<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserCreateRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserCtrl extends Controller{
    public function __construct(){
        //$this->middleware(['']);
    }
    public function index() { return view('dashboard.user.index'); }

    public function create(){ return view('dashboard.user.create'); }


    public function store(UserCreateRequest $request){
        $validated=$request->validated();
        if ($request->hasFile(User::avatarSelectedFiled()??'image')) $validated[User::avatarSelectedFiled()??'image'];
        User::create($validated);
        alert()->success(__('main.'),__('main.created'));
        return  back();
    }

    public function show(User $user){ return view('dashboard.user.show',compact('user')); }

    public function edit(User $user){ return view('dashboard.user.edit',compact('user')); }

    public function update(UserUpdateRequest $request, User $user){
        $validated=$request->validated();
        if ($request->hasFile(User::avatarSelectedFiled()??'image')) $validated[User::avatarSelectedFiled()??'image'];
        $user->update($validated);
        alert()->success(__('main.'),__('main.updated'));
        return  back();
    }

    public function destroy(User $user){
        $user->delete();
        alert()->success(__('main.'),__('main.deleted'));
        return  back();
    }

}
