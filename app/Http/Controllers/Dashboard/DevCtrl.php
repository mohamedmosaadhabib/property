<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Developer;
use Exception;

class DevCtrl extends Controller{

    public function index(){ return view('dashboard.developer.index'); }
    public function create(){ return view('dashboard.developer.create'); }
    public function show(Developer $developer){ return view('dashboard.developer.show',compact('developer')); }

    public function delete(Developer $developer){
        try {
            $developer->delete();
            alert()->success(__('main.developer'),__('main.developer_deleted'));
        } catch (Exception $e) {
        alert()->error(__('main.error_title_ERORR',$e->getMessage()));
        }
        return back();
    }

    public function edit(Developer $developer){ return view('dashboard.developer.edit',compact('developer')); }

}
