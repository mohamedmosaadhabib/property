<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Property\PropertyCreateRequest;
use App\Http\Requests\Property\PropertyUpdateRequest;
use App\Models\Property;
use Illuminate\Http\Request;

class PropertyCtrl extends Controller{
    public function __construct(){
        //$this->middleware(['']);
    }
    public function index() { return view('dashboard.property.index'); }

    public function create(){ return view('dashboard.property.create'); }

    public function toggleActive(Property $property){
        $property->update(['active' => !$property->active]);
        return back()->withSuccess($property->active?__('main.active'):__('main.not_active'));
    }

    public function store(PropertyCreateRequest $request){
        $validated=$request->validated();
        //if ($request->hasFile(Property::avatarSelectedFiled()??'image')) $validated[Property::avatarSelectedFiled()??'image'];
        Property::create($validated);
        alert()->success(__('main.property'),__('main.created'));
        return  back();
    }

    public function show(Property $property){ return view('dashboard.property.show',compact('property')); }

    public function edit(Property $property){ return view('dashboard.property.edit',compact('property')); }

    public function update(PropertyUpdateRequest $request, Property $property){
        $validated=$request->validated();
        //if ($request->hasFile(Property::avatarSelectedFiled()??'image')) $validated[Property::avatarSelectedFiled()??'image'];
        $property->update($validated);
        alert()->success(__('main.property'),__('main.updated'));
        return  back();

    }

    public function destroy(Property $property){
        $property->delete();
        alert()->success(__('main.property'),__('main.deleted'));
        return  back();
    }

}
