<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\BlogCreateRequest;
use App\Http\Requests\Blog\BlogUpdateRequest;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogCtrl extends Controller{
    public function __construct(){
        //$this->middleware(['']);
    }
    public function index() {
        return view('dashboard.blog.index',['blogs'=>Blog::latest()->get()]);
    }

    public function create(){ return view('dashboard.blog.create'); }


    public function store(BlogCreateRequest $request){
        $validated=$request->validated();
        if ($request->hasFile(Blog::avatarFiled()??'image')) $validated[Blog::avatarFiled()??'image']=uploader($request->file(Blog::avatarFiled()??'image'));
        Blog::create($validated);
        alert()->success(__('main.blog'),__('main.created'));
        return  back();
    }

    public function show(Blog $blog){ return view('dashboard.blog.show',compact('blog')); }

    public function edit(Blog $blog){ return view('dashboard.blog.edit',compact('blog')); }

    public function update(BlogUpdateRequest $request, Blog $blog){
        $validated=$request->validated();
        if ($request->hasFile(Blog::avatarFiled()??'image')) $validated[Blog::avatarFiled()??'image']=uploader($request->file(Blog::avatarFiled()??'image'));
        $blog->update($validated);
        alert()->success(__('main.blog'),__('main.updated'));
        return  back();

    }

    public function destroy(Blog $blog){
        $blog->delete();
        alert()->success(__('main.blog'),__('main.deleted'));
        return  back();
    }

}
