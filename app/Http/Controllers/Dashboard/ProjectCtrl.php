<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project\ProjectCreateRequest;
use App\Http\Requests\Project\ProjectUpdateRequest;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectCtrl extends Controller{
    public function __construct(){
        //$this->middleware(['']);
    }
    public function index() { return view('dashboard.project.index'); }

    public function create(){ return view('dashboard.project.create'); }


    public function store(ProjectCreateRequest $request){
        $validated=$request->validated();
        //if ($request->hasFile(Project::avatarSelectedFiled()??'image')) $validated[Project::avatarSelectedFiled()??'image'];
        Project::create($validated);
        alert()->success(__('main.project'),__('main.created'));
        return  back();
    }

    public function show(Project $project){ return view('dashboard.project.show',compact('project')); }

    public function edit(Project $project){ return view('dashboard.project.edit',compact('project')); }

    public function update(ProjectUpdateRequest $request, Project $project){
        $validated=$request->validated();
        //if ($request->hasFile(Project::avatarSelectedFiled()??'image')) $validated[Project::avatarSelectedFiled()??'image'];
        $project->update($validated);
        alert()->success(__('main.project'),__('main.updated'));
        return  back();

    }

    public function destroy(Project $project){
        $project->delete();
        alert()->success(__('main.project'),__('main.deleted'));
        return  back();
    }

}
