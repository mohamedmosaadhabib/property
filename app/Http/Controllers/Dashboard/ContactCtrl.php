<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactCreateRequest;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactCtrl extends Controller{
    public function __construct(){
        //$this->middleware(['']);
    }

    public function toggleStatus(Contact $contact){
        $contact->update(['read_at' => now()]);
        return back()->withSuccess(__('main.mark_as_read'));
    }
    public function index() { return view('dashboard.contact.index'); }

//    public function create(){ return view('dashboard.contact.create'); }


    public function store(ContactCreateRequest $request){
        $validated=$request->validated();
        //if ($request->hasFile(Contact::avatarSelectedFiled()??'image')) $validated[Contact::avatarSelectedFiled()??'image'];
        Contact::create($validated);
        alert()->success(__('main.contact'),__('main.created'));
        return  back();
    }

//    public function show(Contact $contact){ return view('dashboard.contact.show',compact('contact')); }

//    public function edit(Contact $contact){ return view('dashboard.contact.edit',compact('contact')); }
//
//    public function update(ContactUpdateRequest $request, Contact $contact){
//        $validated=$request->validated();
//        //if ($request->hasFile(Contact::avatarSelectedFiled()??'image')) $validated[Contact::avatarSelectedFiled()??'image'];
//        $contact->update($validated);
//        alert()->success(__('main.contact'),__('main.updated'));
//        return  back();
//
//    }

    public function destroy(Contact $contact){
        $contact->delete();
        alert()->success(__('main.contact'),__('main.deleted'));
        return  back();
    }

}
