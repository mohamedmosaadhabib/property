<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Region\RegionCreateRequest;
use App\Http\Requests\Region\RegionUpdateRequest;
use App\Models\Region;
use Illuminate\Http\Request;

class RegionCtrl extends Controller{
    public function __construct(){
        //$this->middleware(['']);
    }
    public function index() { return view('dashboard.region.index'); }

    public function create(){ return view('dashboard.region.create'); }


    public function store(RegionCreateRequest $request){
        $validated=$request->validated();
        //if ($request->hasFile(Region::avatarSelectedFiled()??'image')) $validated[Region::avatarSelectedFiled()??'image'];
        Region::create($validated);
        alert()->success(__('main.region'),__('main.created'));
        return  back();
    }

    public function show(Region $region){ return view('dashboard.region.show',compact('region')); }

    public function edit(Region $region){ return view('dashboard.region.edit',compact('region')); }

    public function update(RegionUpdateRequest $request, Region $region){
        $validated=$request->validated();
        //if ($request->hasFile(Region::avatarSelectedFiled()??'image')) $validated[Region::avatarSelectedFiled()??'image'];
        $region->update($validated);
        alert()->success(__('main.region'),__('main.updated'));
        return  back();

    }

    public function destroy(Region $region){
        $region->delete();
        alert()->success(__('main.region'),__('main.deleted'));
        return  back();
    }

}
