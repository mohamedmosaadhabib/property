<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\UnitTypes\UnitTypesCreateRequest;
use App\Http\Requests\UnitTypes\UnitTypesUpdateRequest;
use App\Models\UnitTypes;
use Illuminate\Http\Request;

class UnitTypesCtrl extends Controller{
    public function __construct(){  }
    public function index() { return view('dashboard.unitTypes.index'); }

    public function create(){ return view('dashboard.unitTypes.create'); }

    public function store(UnitTypesCreateRequest $request){
        $validated=$request->validated();
        //if ($request->hasFile(UnitTypes::avatarFiled()??'image')) $validated[UnitTypes::avatarFiled()??'image']=uploader($request->file(UnitTypes::avatarFiled()??'image'));
        UnitTypes::create($validated);
        alert()->success(__('main.unitTypes'),__('main.created'));
        return  back();
    }

    public function show($unitTypes){ return view('dashboard.unitTypes.show',['unitTypes'=>UnitTypes::findOrFail($unitTypes)]); }

    public function edit($unitTypes){return view('dashboard.unitTypes.edit',['unitTypes'=>UnitTypes::findOrFail($unitTypes)]); }

    public function update(UnitTypesUpdateRequest $request, $unitTypes){
        $unitTypes=UnitTypes::findOrFail($unitTypes);
        $validated=$request->validated();
        //if ($request->hasFile(UnitTypes::avatarFiled()??'image')) $validated[UnitTypes::avatarFiled()??'image']=uploader($request->file(UnitTypes::avatarFiled()??'image'));
        $unitTypes->update($validated);
        alert()->success(__('main.unitTypes'),__('main.updated'));
        return  back();
    }

    public function destroy($unitTypes){
        UnitTypes::findOrFail($unitTypes)->delete();
        alert()->success(__('main.unitTypes'),__('main.deleted'));
        return  back();
    }

}
