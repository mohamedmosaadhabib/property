<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\User;
use foo\bar;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class DashboardController extends Controller{

    public function index() { return view('dashboard.blank'); }

    public function contactUs(){ return view('dashboard.contact'); }
    //
    public function change_password(){ return view('dashboard.profile.change_password'); }
    public function change_password_user(Request $request){
        $request->validate([
            'current_password' => ['required', 'string', 'min:8'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $user=auth()->user();
        /**
         * @var $user User
         */
        if (!$user->comparePassword($request->get('current_password'))){
            return back()->withErrors([ "current_password"=>__('main.current_password_not_match')]);
        }



        if ($user->comparePassword($request->get('password'))){
            alert()->info(__('main.profile'),__('main.password_old_and_new_same'));
        }else{
            $user->update([
                'password' => $request->get('password')
            ]);
            alert()->success(__('main.profile'),__('main.password_changed_success'));
        }
        return back();
    }

}
