<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactProjectRequest;
use App\Models\Blog;
use App\Models\Developer;
use App\Models\Project;
use App\Models\Property;
use App\Models\Region;
use App\Models\Slider;
use App\Models\UnitTypes;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Route;
use function request;
use function url;

/**
 * Class FrontController
 * @package App\Http\Controllers
 */
class FrontController extends Controller
{


    /**
     * @var string
     */
    private $typeOfPage;

    public function setLocale(string $locale = "en")
    {
        session()->put('locale', $locale);
        if (auth()->check()) auth()->user()->update(['locale' => $locale]);
        return redirect()->back();
    }

    public function main()
    {
        $sliders = Slider::orderBy('order_by')->limit(6)->get();
        SEOTools::setTitle(trans('main.home_page'));
        SEOTools::opengraph()->setUrl(url()->current());
        SEOTools::setCanonical(url()->current());
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::addImages([collect($sliders)->transform(function ($s) {
            return $s->imagePath;
        })]);

        $regions = Region::show()->latest()->whereNotNull('image')->take(6)->get();
        $blogs = Blog::latest()->inRandomOrder()->take(6)->get();

        return view('front.home', compact('sliders', 'regions', 'blogs'));
    }

    public function about()
    {
        SEOTools::setTitle(trans('main.about_us'));
        return view('front.about');
    }

    public function dashboard()
    {
        return view('front.dashboard');
    }

    public function pricing()
    {
        return view('front.show-property', ['project' => Project::active()->first()]);
    }

    public function location(Region $location)
    {
        SEOTools::setTitle($location->name);
        SEOTools::opengraph()->setUrl(url()->current());
        SEOTools::setCanonical(url()->current());
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::addImages([$location->imagePath]);
//        SitemapGenerator::create(\url()->current())
//            ->getSitemap()
//            ->add(Url::create(url()->current())
//                ->setLastModificationDate($location->created_at)
//                ->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)
//                ->setPriority(0.1))
//            ->writeToFile(public_path('sitemap/developer/developer.xml'));

        $projects = Project::active()->where(function (Builder $builder) use ($location) {
            return $builder->orWhere('location->city', $location->id)->orWhere('location->region', $location->id);
        })->paginate();
        return view('front.project', compact('projects', 'location'));
    }

    public function developers()
    {
        SEOTools::setTitle(trans('main.developers'));
        return view('front.developer.developers');
    }

    public function developers_page()
    {
        SEOTools::setTitle(trans('main.developers'));
        return view('front.developer.developers');
    }

    public function create()
    {
        return view('front.developer.create');
    }

    public function show(Developer $developer)
    {
        SEOTools::setTitle($developer->name);
        SEOTools::setDescription($developer->description);
        SEOTools::opengraph()->setUrl(url()->current());
        SEOTools::setCanonical(url()->current());
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::addImages([$developer->imagePath]);

//        SitemapGenerator::create(\url()->current())
//            ->getSitemap()
//            ->add(Url::create(url()->current())
//                ->setLastModificationDate($developer->created_at)
//                ->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)
//                ->setPriority(0.1))
//            ->writeToFile(public_path('sitemap/developer/developer.xml'));

        $region = $developer->projects->pluck('location.region')->unique()->toArray();
        $city = $developer->projects->pluck('location.city')->unique()->toArray();
        $regions = Region::show()->findMany(collect($region)->merge($city ?? []));
        return view('front.developer-page', compact('developer', 'regions'));
    }

    public function contact_create()
    {
        SEOTools::setTitle(trans('main.contact'));
        return view('front.contact');
    }

    public function contact_project_post(ContactProjectRequest $contactProjectRequest, Project $project)
    {
        $project->contacts()->create($contactProjectRequest->validated());
        return back()->withSuccess(__('main.contact_success'));
    }

    public function contact_property_post(ContactProjectRequest $contactProjectRequest, Property $property)
    {
        $property->contacts()->create($contactProjectRequest->validated());
        return back()->withSuccess(__('main.contact_success'));
    }

    public function SubmitProperty()
    {
        return view('front.submit-property');
    }

//    public function SingleProperty(Property $property){ return view('front.single-property-1',compact('property')); }

    // region_id
    // unit_type_id
    // search
    // minimum
    // maximum
    public function project()
    {
        SEOTools::setTitle(trans('main.project'));
        return view('front.project', [
            "projects" => Project::active()->latest()
                ->when(request('city'), function (Builder $builder) {
                    return $builder->orWhere('location->city', request('region_id'))->orWhere('location->region', request('region_id'));
                })
                ->when(request('unit_type_id'), function (Builder $builder) {
                    return $builder->orWhereJsonContains('types', ["unit_type" => request('unit_type_id')]);
                })
                ->when(request('search'), function (Builder $builder) {
                    return $builder->orWhere('name->' . app()->getLocale(), 'LIKE', '%' . request('search') . '%');
                })
                ->when(request('minimum'), function (Builder $builder) {
                    return $builder->where('price', '>=', request('minimum'));
                })
                ->when(request('maximum'), function (Builder $builder) {
                    return $builder->where('price', '<=', request('maximum'));
                })
                ->paginate()->withQueryString(request()->all())
        ]);
    }

    /**
     * @param string $property_type
     * @param string $region
     * @param string $city
     * @param Property $property
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function SingleProperty(string $property_type, string $region, string $city, Property $property)
    {

        SEOTools::setTitle($property->name);
        SEOTools::setDescription($property->description);
        SEOTools::setDescription($property->description);
        SEOTools::metatags()->setKeywords($property->tags);
        $type = $property->guessType();

        return view('front.show-rent', compact('property', 'type', 'property_type', 'region', 'city'));
    }

    public function SingleProject($location, Developer $developer, Project $project)
    {
        $location = (new Region())->resolveRouteBinding($location, Route::current()->bindingFieldFor('location') ?? 'id');
        SEOTools::setTitle($project->name);
        SEOTools::setDescription($project->description);
        SEOTools::opengraph()->setUrl(url()->current());
        SEOTools::setCanonical(url()->current());
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::addImages(collect($project->photos)->transform(function ($image) {
            return url($image);
        })->toArray());

        $types = UnitTypes::whereIn('id', $project->types->pluck('unit_type')->toArray())->with('parent')->orderBy('unit_types_id')->get()->filter();
        $parents = $types->pluck('parent')->unique()->filter();
        return view('front.show-project', compact('project', 'types', 'parents', 'developer'));
    }

    public function blogs()
    {
        SEOTools::setTitle(trans('main.blogs'));
        $blogs = Blog::latest()->paginate();
        return view('front.blog', compact('blogs'));
    }

    /**
     * @param Blog $blog
     * @return Factory|View
     */
    public function SingleBlog(Blog $blog)
    {
        SEOTools::setTitle($blog->name);
        SEOTools::setDescription($blog->description);
        SEOTools::opengraph()->setUrl(url()->current());
        SEOTools::setCanonical(url()->current());
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::addImages([$blog->imagePath]);

        return view('front.blog-detail', compact('blog'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function autocomplete(Request $request)
    {
        $type = $request->get('type');
        $search = $request->get('search');
        $max = $request->get('max', 15);
        $data = [];
        if ($type == 'project') {
            $data = Project::active()->where('name', 'like', '%' . $search . '%')
//                ->orWhere('name->ar','like','%'.$search.'%')
//                ->orWhere('name->en','like','%'.$search.'%')
                ->orderBy('name')->take($max)->get()->pluck('name')->toArray();
        } elseif ($type == 'rent') {
            $data = Property::active()->SearchRent($search)->take($max)->get(['name'])->pluck('name')->toArray();
        } elseif ($type == 'sale') {
            $data = Property::active()->where('sale->check', true)->where('name', 'like', '%' . $search . '%')->take($max)->get(['name'])->pluck('name')->toArray();
        }

        return response()->json($data);
    }


    public function filterProperties($region = null, $city = null, $street = null)
    {
        $this->typeOfPage = preg_match('/rent/',url()->current()) ? 'rent':'resale';
//        dd($this->typeOfPage);
        $properties = Property::latest()->active()->with(['user', 'owner']);
        if (in_array($this->typeOfPage,['rant','rent'])) {
            $types = ["rent_unfurnished", "rent_furnished", "rent_semi_furnished",];
            if (!request('ptypes')) $properties = $properties->filterRent();
        }
        if (in_array($this->typeOfPage,['resale','sale'])) {
            $types = ['resale', 'sale'];
            if (!request('ptypes')) $properties = $properties->filterSale();
        }

        $properties
            ->when(request('search'), function (Builder $builder) {
                return $builder->where('name->' . app()->getLocale(), 'LIKE', '%' . request('search') . '%');
            })->when(request('bedrooms'), function (Builder $builder) {
                return $builder->where('bed_rooms', request('bedrooms'));
            })->when(request('bedrooms'), function (Builder $builder) {
                return $builder->where('bed_rooms', request('bedrooms'));
            })->when(request('ptypes'), function (Builder $builder) {
                return $builder->where(request('ptypes') . '->check', true);
            })
            ->when(request('region_id'), function (Builder $builder) {
                return $builder
                    ->where(function (Builder $q) {
                        return $q
                            ->orWhere('location->city', request('region_id'))
                            ->orWhere('location->region', request('region_id'))
                            ->orWhere('location->street', request('region_id'));
                    });
            })->when(request('ptypes') && request('minimum'), function (Builder $builder) {
                return $builder->where(request('ptypes') . '->value', '>=', (float)request('minimum'));
            })->when(request('ptypes') && request('maximum'), function (Builder $builder) {
                return $builder->where(request('ptypes') . '->value', '<=', (float)request('maximum'));
            })->when(isset($region->id), function (Builder $q) use ($region) {
                return $q->where('location->region', $region->id);
            })->when(isset($city->id), function (Builder $q) use ($city) {
                return $q->where('location->city', $city->id);
            })->when(isset($street->id), function (Builder $q) use ($street) {
                return $q->where('location->street', $street->id);
            })->when( !($region instanceof Region) && !($city instanceof Region) && !($street instanceof Region) , function (Builder $q) {
                foreach (explode('/',request()->url()) as $item) {
                    if (preg_match('/-for-/',$item)) {
                        $filterText = explode('-',$item)[0];
                        return $q->where('property_type',$filterText);
                    }
                }
                return  $q;
            });


//            ->when(request('ptypes') && request('maximum') && request('minimum'),function (Builder $builder){ return $builder
//                ->whereBetween(request('ptypes').'->value',[request('minimum'),request('maximum')]); });
        return compact('properties', 'types');
    }

    public function resale(string $region = null, string $city = null, string $street = null, string $type_for = null)
    {
        $word = 'resale';
        $regions = collect();
        $properties_types = collect();
        if (!preg_match('/-for-' . $word . '/', $region) && $region) {
            $region = (new Region())->resolveRouteBinding(($region), 'slug');

        }
        if (!preg_match('/-for-' . $word . '/', $city) && $city) {
            $city = (new Region())->resolveRouteBinding($city, 'slug');
        }
        if (!preg_match('/-for-' . $word . '/', $street) && $street) {
            $street = (new Region())->resolveRouteBinding($street, 'slug');
        }

        if (preg_match('/-for-' . $word . '/', $region) && $region) {
            $regions = Region::region()->where('show', true)->get();

        } elseif (preg_match('/-for-' . $word . '/', $city) && $city) {
            $regions = Region::city()->where('show', true)->get();
        } elseif (preg_match('/-for-' . $word . '/', $street) && $street) {
            $regions = Region::street()->where('show', true)->get();
        }
        if (is_null($region)) {
            $properties_types = collect(Property::TYPES_ROOMS ?? []);
        }

        $filter = $this->filterProperties($region, $city, $street);
        $properties = $filter['properties'];
        $counter = $properties->count();
        SEOTools::setTitle($trans = trans('main.resale'));
        return view('front.grid', [
            'pageTitle' => $trans,
            'counter' => $counter,
            'properties' => $properties->paginate(),
            'regions' => $regions ?? collect(),
            'properties_types' => $properties_types,
            "region" => $region,
            "city" => $city,
            "street" => $street,
            "word" => $word,
            "type_for"=>$type_for,
            "type"=>$this->typeOfPage
        ]);
    }

    public function rant(string $region = null, string $city = null, string $street = null, string $type_for = null)
    {
        $word = 'rent';
        $regions = collect();
        $properties_types = collect();
        if (!preg_match('/-for-' . $word . '/', $region) && $region) {
            $region = (new Region())->resolveRouteBinding(($region), 'slug');

        }
        if (!preg_match('/-for-' . $word . '/', $city) && $city) {
            $city = (new Region())->resolveRouteBinding($city, 'slug');
        }
        if (!preg_match('/-for-' . $word . '/', $street) && $street) {
            $street = (new Region())->resolveRouteBinding($street, 'slug');
        }

        if (preg_match('/-for-' . $word . '/', $region) && $region) {
            $regions = Region::region()->get();
        } elseif (preg_match('/-for-' . $word . '/', $city) && $city) {
            $regions = Region::city()->where('region_id',$region->id)->get();
        } elseif (preg_match('/-for-' . $word . '/', $street) && $street) {
            $regions = Region::street()->where('region_id',$city->id)->get();
        }

        if (is_null($region)) {
            $properties_types = collect(Property::TYPES_ROOMS ?? []);
        }
        $filter = $this->filterProperties(isset($region) ? $region : null, isset($city) ? $city : null, isset($street) ? $street : null);

        $properties = $filter['properties'];
        $counter = $properties->count();
        SEOTools::setTitle($trans = trans('main.rent'));
        return view('front.grid', [
            'pageTitle' => $trans,
            'counter' => $counter,
            'properties' => $properties->paginate(),
            'regions' => $regions ?? collect(),
            'properties_types' => $properties_types,
            "region" => $region,
            "city" => $city,
            "street" => $street,
            "word" => $word,
            "type_for"=>$type_for,
            "type"=>$this->typeOfPage
        ]);
    }

}
