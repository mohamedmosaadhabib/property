<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Config;
use Session;

class AppDefaultLanguage{

    public function handle($request, Closure $next,$locale=null){
        $locale=session()->get('locale',$locale ?? config('app.locale','en'));
        if (auth()->check() && isset(auth()->user()->locale)) $locale=auth()->user()->locale;
        config()->set('app.locale',$locale);
        Carbon::setLocale($locale);
        view()->share('locale',mb_strtolower(substr($locale, 0,2)));

        app()->setLocale(request('locale' , $locale ) );
        return $next($request);
    }
}
