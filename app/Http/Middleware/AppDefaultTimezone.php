<?php

namespace App\Http\Middleware;

use Closure;

class AppDefaultTimezone{

    public function handle($request, Closure $next,$timezone=null){
        $default=$timezone ?? 'africa/cairo';
        $timezone= $timezone ?? config('app.timezone',$default);
        if (auth()->check() && isset(auth()->user()->timezone)) $timezone=auth()->user()->timezone;
        date_default_timezone_set($timezone);
        config()->set('app.timezone',$timezone);
        view()->share('timezone',mb_strtoupper($timezone));

        return $next($request);
    }
}
