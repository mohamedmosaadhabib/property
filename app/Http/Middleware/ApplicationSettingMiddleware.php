<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Artesaos\SEOTools\Facades\SEOTools;
use Closure;

class ApplicationSettingMiddleware{
    public function handle($request, Closure $next) {
        config()->set('app.setting',
            cache()->remember('users', now()->addMinute(), function () {
                return Setting::orderBy('key')->get();
            }));
        $name=setting('app_name_'.app()->getLocale());
        $description=setting('app_description_'.app()->getLocale(),'description description description description description description ');
        config()->set('app.name',$name);
        foreach (['meta', 'opengraph', 'json-ld'] as $item)  config(['seotools.'.$item.'.defaults.title'=>$name,'seotools.'.$item.'.defaults.description'=>$description]);


        SEOTools::setDescription($description);
        SEOTools::setTitle($name);

        return $next($request);
    }
}
