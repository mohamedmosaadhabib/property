<?php

namespace App\Models;

use App\Traits\HasContact;
use App\Traits\HasUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;

class Property extends Model
{
    #traits
    use SoftDeletes, HasUser, HasTranslations, HasSlug, HasContact;
    use LogsActivity;

    const TYPES = ["sale", "rent_unfurnished", "rent_furnished", "resale", "rent_semi_furnished",];
    const TYPES_ROOMS = ["apartment", "twin_house", "chalet", "cafe", "office", "i_villa", "penthouse", "studio", "town_house", "ground_floor", "duplex", "villa"];
    protected static $logFillable = true;
    #model properties
    protected static $logName = 'Property';
    protected static $logOnlyDirty = true;
    public $translatable = ["name", "address", "description"];
    protected $table = "properties";
    protected $casts = [
        "resale" => "array",
        "sale" => "array",
        "rent_unfurnished" => "array",
        "rent_furnished" => "array",
        "rent_semi_furnished" => "array",
        "rooms" => "array",
        "tags" => "array",
        "location" => "array",
        "images" => "array",
        "building_number" => "integer",
        "floor_number" => "integer",
        "apartment_number" => "integer",
    ];
    protected $fillable = [
        "name", "bed_rooms", "bathrooms", "description", "tags", "images", "image", "slug", "property_type", "property_views", "currency", "sale",
        "rent_unfurnished", "rent_furnished", "resale", "rent_semi_furnished", "building_number", "floor_number",
        "apartment_number", "address", "rooms", "owner_id", "active", "location"
    ];
    protected $with = ['city', 'region', 'street'];
    #start mutators

    #end mutators

    #start Relations

    public static function typesStatic()
    {

        return self::TYPES;
    }
    #end Relations

    #start Scopes

    #end Scopes

    #handle route

    public function owner()
    {
        return $this->belongsTo(Owner::class, 'owner_id')->withDefault([
            "name" => [
                "ar" => trans("main.deleted", [], 'ar'),
                "en" => trans("main.deleted", [], 'en'),
            ]
        ]);
    }

    /**
     * @return mixed
     */
    public function getLowPrice():float
    {
        $array = collect([]);
        foreach (self::TYPES as $type) {
            $array->push($this->{$type}['value'] ?? null);
        }
        return (float)$array->filter()->min();
    }

    /**
     * @param string $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return "This Property has been {$eventName}";
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    protected function generateNonUniqueSlug(): string
    {
        $slugField = $this->slugOptions->slugField;

        if ($this->hasCustomSlugBeenUsed() && ! empty($this->$slugField)) {
            return $this->$slugField;
        }

        $region = optional(Region::find($this->location['region']))->name;
        $city = optional(Region::find($this->location['city']))->name;
        $slug = $this->guessType();
        return Str::slug("$this->property_type for $slug in $region $city {$this->getSlugSourceString()}", $this->slugOptions->slugSeparator, $this->slugOptions->slugLanguage);
    }
    /**
     * @return string
     */
    public function getUrl():string
    {
        return showDeveloper($this->developer, $this);
    }

    /**
     * @return string
     */
    public function getImagePathAttribute():string
    {
        return asset($this->images[$this->image] ?? '');
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeFilterRent(Builder $builder):Builder
    {
        return $builder->where(function (Builder $query) {
            return $query
                ->orWhere('rent_furnished->check', true)
                ->orWhere('rent_semi_furnished->check', true)
                ->orWhere('rent_unfurnished->check', true);
        });
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeFilterSale(Builder $builder):Builder
    {
        return $builder->where(function (Builder $query) {
                return $query
                    ->orWhere('sale->check', true)
                    ->orWhere('resale->check', true);
        });
    }

    /**
     * @param Builder $builder
     * @param string $search
     * @return Builder
     */
    public function scopeSearchRent(Builder $builder,string $search):Builder
    {
        return $builder->where(function (Builder $query) {
            return $query
                    ->orWhere('rent_furnished->check', true)
                    ->orWhere('rent_semi_furnished->check', true)
                    ->orWhere('rent_unfurnished->check', true);
        })
            ->where('name', 'like', '%' . $search . '%')
//                ->orWhere('name->ar','like','%'.$search.'%')
//                ->orWhere('name->en','like','%'.$search.'%')
            ->orderBy('name');
    }

    /**
     * @param string $default
     * @return string
     */
    public function guessType(string $default = 'resale')
    {
        foreach (self::TYPES as $TYPE) {
            if ((!empty($this->{$TYPE}) && $this->{$TYPE}['check'] ?? false) === true) return $TYPE;
        }
        return $default;
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeActive(Builder $builder):Builder
    {
        return $builder->where('active', true);
    }

    /**
     * @return BelongsTo
     */
    public function street(): BelongsTo
    {
        return $this->belongsTo(Region::class, 'location->street');
    }

    /**
     * @return BelongsTo
     */
    public function region(): BelongsTo
    {
        return $this->belongsTo(Region::class, 'location->region');
    }

    /**
     * @return BelongsTo
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(Region::class, 'location->city');
    }

}
