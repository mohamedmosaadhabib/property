<?php

namespace App\Models;

use App\Traits\HasAvatar;
use App\Traits\HasUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\HasTranslatableSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;

class Developer extends Model{
    #traits
    use SoftDeletes,HasUser,LogsActivity,HasAvatar,HasTranslations,HasSlug;

    public static function avatarFiled(){ return 'image'; }

    protected static $logFillable = true;
    protected static $logName = 'Developer';
    protected static $logOnlyDirty = true;
    public $translatable   = ['name','description'];
    #model properties
    protected $table="developers";

    protected $casts=[
        "active"=>"boolean"
    ];
    protected $attributes=[

    ];

    protected $fillable=[ "name",  "description", "image",'active' ];

    #start mutators

    #end mutators

    #start Relations
    public function projects(){ return $this->hasMany(Project::class,'developer_id'); }
    #end Relations

    #start Scopes

    #end Scopes

   public function getDescriptionForEvent(string $eventName): string
    {
        return "This Developer has been {$eventName}";
    }

    public function getSlugOptions() : SlugOptions{
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function getUrl() { return route('developer.show',$this); }
}
