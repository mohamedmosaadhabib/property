<?php

namespace App\Models;

use App\Traits\HasUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Sluggable\HasTranslatableSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;

class UnitTypes extends Model{
    #traits
    use SoftDeletes,HasUser,HasTranslations,HasTranslatableSlug;
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logName = 'UnitTypes';
    protected static $logOnlyDirty = true;
    public $translatable   = ['name','slug'];
    #model properties
    protected $table="unit_types";
    protected $casts=[ ];
    protected $appends=[ ];
    protected $attributes=[ ];
    protected $fillable=[ "name" , "unit_types_id" ];

    #start mutators

    #end mutators

    #start Relations
    public function units(){ return $this->hasMany(self::class,'unit_types_id'); }
    public function parent(){ return $this->BelongsTo(self::class ,'unit_types_id'); }
    #end Relations

    #start Scopes

    #end Scopes

    #handle route
   public function resolveRouteBinding($value, $field = null){ return parent::resolveRouteBinding($value, $field);  }

   public function getDescriptionForEvent(string $eventName): string { return "This UnitTypes has been {$eventName}"; }

   public function getSlugOptions() : SlugOptions{
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
}
