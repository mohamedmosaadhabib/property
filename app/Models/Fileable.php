<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\HasUser;
use Spatie\Activitylog\Traits\LogsActivity;

class Fileable extends Model{
    #traits
    use SoftDeletes,HasUser;
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logName = 'Fileable';
    protected static $logOnlyDirty = true;
    #model properties
    protected $table="fileables";
    public const FILEABLE="fileable";
    protected $casts=[ ];
    protected $appends=[ 'filePath'];
    protected $fillable=[ "filename", "type", "extension", "album_id", User::USER_ID,'options' ];

    #start mutators
    public function getFilePathAttribute(){
        if (!file_exists(public_path($this->filename))) {
            return  asset('uploads/uploads/propertymaster_5f675f73e253620_09_2020_15_56_03.jpeg');
        }
        return asset($this->filename ?? 'uploads/uploads/propertymaster_5f675f73e253620_09_2020_15_56_03.jpeg');
    }
    #end mutators

    #start Relations

    public function fileable(){ return $this->morphTo(); }

    #end Relations

    #start Scopes

    #end Scopes

    #handle route
    public function resolveRouteBinding($value, $field = null){
        return parent::resolveRouteBinding($value, $field);
    }

   public function getDescriptionForEvent(string $eventName): string { return "This Fileable has been {$eventName}"; }

}
