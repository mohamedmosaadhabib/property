<?php

namespace App\Models;

use App\Traits\HasAvatar;
use App\Traits\HasUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\HasTranslatableSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;

class Region extends Model{
    #traits
    use SoftDeletes,HasUser,HasTranslations,HasSlug, LogsActivity,HasAvatar;
    public static function avatarFiled(){ return 'image'; }

    protected static $logFillable = true;
    protected static $logName = 'Region';
    protected static $logOnlyDirty = true;
    public $translatable   = ['name'];
    #model properties
    protected $table="regions";
    protected $casts=['show'=>"boolean"];
    protected $fillable=[ 'name','type','image','show','region_id'];
    protected $appends=['typeText'];
    #start mutators

    #end mutators

    #start Relations
    public function parent(){ return $this->belongsTo(self::class,'region_id'); }
    public function children(){ return $this->hasMany(self::class,'region_id'); }
    public function getTypeTextAttribute(){
        if ($this->type ===1) return __('main.region');
        if ($this->type ===2) return __('main.city');
        if ($this->type ===3) return __('main.streets_and_compounds');
        if ($this->type ===4) return __('main.governorate');
    }
    #end Relations

    #start Scopes

    #end Scopes

    #handle route
    public function resolveRouteBinding($value, $field = null){
        if ($field=="slug"){
            if ($model=$this->firstWhere($field.'->ar', $value)) return $model;
            if ($model=$this->firstWhere($field.'->en', $value)) return $model;
        }
        return parent::resolveRouteBinding($value, $field);
    }

   public function getDescriptionForEvent(string $eventName): string
    {
        return "This Region has been {$eventName}";
    }
    public function getSlugOptions() : SlugOptions{
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    // public function scopeCity(Builder $builder)
    // {
    //     return $builder->where('type',2);
    // }
    // public function scopeRegion(Builder $builder)
    // {
    //     return $builder->where('type',1);
    // }
    // public function scopeStreet(Builder $builder)
    // {
    //     return $builder->where('type',3);
    // }
    public function scopeGovernorate(Builder $builder)
    {
        return $builder->where('type',4);
    }
    public function scopeShow(Builder $builder)
    {
        return $builder->where('show',true);
    }

    public function ProjectCities()
    {
        return $this->hasMany(Project::class,'location->city');
    }
    public function ProjectRegion()
    {
        return $this->hasMany(Project::class,'location->region');
    }
    public function ProjectStreet()
    {
        return $this->hasMany(Property::class,'location->street');
    }
    public function scopeHasManyProjects(Builder $builder)
    {
        return $builder->whereHas('ProjectCities',function ($q) {
            return $q;
        },1)->orWhereHas('ProjectRegion',function ($q) {
            return $q;
        },1);
    }

    public function scopeRegion(Builder $builder)
    {
        return $builder->where('type',4);
    }
    public function scopeCity(Builder $builder)
    {
        return $builder->where('type',2);
    }
    public function scopeStreet(Builder $builder)
    {
        return $builder->where('type',3);
    }
}
