<?php

namespace App\Models;

use Illuminate\Notifications\DatabaseNotification;
use Spatie\Activitylog\Traits\LogsActivity;

class Notification extends DatabaseNotification{
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logName = 'social';
    protected static $logOnlyDirty = true;
    public function getDescriptionForEvent(string $eventName): string{
        return "This ".strtolower(class_basename(static::class))." has been {$eventName}";
    }

}
