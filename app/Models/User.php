<?php

namespace App\Models;

use App\Traits\HasAvatar;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\HasPassword;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class User extends Authenticatable{
    use Notifiable,SoftDeletes,LogsActivity,HasPassword,HasSlug,HasAvatar;
    public static function avatarFiled(){ return 'image'; }
    public const USER_ID = "user_id";

    protected $fillable = [
        'name', 'email', 'password','image','phone','locale','is_admin'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $dateFormat="Y-m-d";
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_admin' => 'boolean',
    ];

    public function getSlugOptions() : SlugOptions{
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function isAdmin(){ return $this->is_admin; }

    protected static function boot(){
        parent::boot();
        static::creating(function (Model $model) {
            $model->locale = $model->locale  ?? app()->getLocale();
            $model->is_admin = $model->is_admin  ?? false;
            $model->phone = $model->phone  ?? null;
        });
    }

}
