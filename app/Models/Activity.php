<?php


namespace App\Models;


use Spatie\Activitylog\Models\Activity as Activitylog;

class Activity extends Activitylog{


    public function getIconAttribute(){
        switch (strtolower(class_basename($this->subject_type))) {
            case "user":
                return "fa fa-user";
            break;
            case "phone":
                return "fa fa-phone";
            break;
            default:
            return "fa fa-cube";
        }
    }
}
