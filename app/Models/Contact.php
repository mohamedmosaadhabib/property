<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Spatie\Activitylog\Traits\LogsActivity;

class Contact extends Model{
    //traits
    use SoftDeletes,LogsActivity;
    public const STARTED=0,DONE=1,PROCESS=2;
    protected static $logFillable = true;
    protected static $logName = 'Contact';
    protected static $logOnlyDirty = true;
    //model properties
    protected $table="contacts";
    protected $casts=[
        "read_at"=>"datetime"
    ];

    protected $fillable=[ "contactable_id" , "contactable_type" , "name" , "email" , "subject" , "message" , "read_at" , "type" , "phone"  ];

    //start mutators

    //end mutators

    //start Relations

    public function contactable(){ return $this->morphTo(); }

    //end Relations

    //start Scopes
    public static function scopeDone(Builder $builder){ return $builder->where('type',self::DONE); }
    public static function scopeStarted(Builder $builder){ return $builder->where('type',self::STARTED); }
    public static function scopeProcess(Builder $builder){ return $builder->where('type',self::PROCESS); }
    //end Scopes

    //handle route
    public function resolveRouteBinding($value, $field = null){
        return parent::resolveRouteBinding($value, $field);
    }

   public function getDescriptionForEvent(string $eventName): string
    {
        return "This Contact has been {$eventName}";
    }
    // boot
    protected static function boot(){
        parent::boot();
        static::created(function (Model $model){

            // fire notification for admin

        });
    }
}
