<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Sitemap\SitemapGenerator;

class GenerateSitemap extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // modify this to your own needs
        SitemapGenerator::create(url('/project'))->writeToFile(public_path('sitemap\project\project.xml'));
        SitemapGenerator::create(url('/developer'))->writeToFile(public_path('sitemap\developer\developer.xml'));
        SitemapGenerator::create(config('app.url'))->writeToFile(public_path('sitemap\sitemap.xml'));
    }
}
