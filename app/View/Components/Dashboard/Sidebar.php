<?php

namespace App\View\Components\Dashboard;

use Illuminate\View\Component;

class Sidebar extends Component{

    public function render(){
        return view('components.dashboard.sidebar');
    }
}
