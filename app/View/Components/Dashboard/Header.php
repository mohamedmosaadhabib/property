<?php

namespace App\View\Components\Dashboard;

use Illuminate\View\Component;

class Header extends Component{

    public function render(){
        return view('components.dashboard.header');
    }
}
