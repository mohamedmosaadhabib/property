<?php

namespace App\View\Components;

use Illuminate\View\Component;

class DeveloperIndex extends Component {

    public function render(){
        return view('components.developer-index',[
            "developers"=>\App\Models\Developer::all()
        ]);
    }
}
