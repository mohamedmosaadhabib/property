<?php

namespace App\View\Components;

use Illuminate\View\Component;

class navLink extends Component{

    public $href;

    public $hasMenu;

    public $linkName;
    /**
     * @var string
     */
    public $class;
    public $linkClass;

    public function __construct($href="", $hasMenu=false, $linkName="",$class="",$linkClass=""){

        $this->href = $href;
        $this->hasMenu = $hasMenu;
        $this->linkName = $linkName;
        $this->class = $class;
        $this->linkClass = $linkClass;
    }
    public function render($href="",$hasMenu=false,$linkName="")
    {
        return view('components.nav-link');
    }
}
