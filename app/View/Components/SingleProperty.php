<?php

namespace App\View\Components;

use App\Models\Property;
use Illuminate\View\Component;

class SingleProperty extends Component {
    /**
     * @var Property $property
     */
    public $property;

    public function __construct(Property $property){
        $this->property = $property;
    }

    public function render() { return view('components.single-property'); }

}
