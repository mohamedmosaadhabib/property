<?php

namespace App\View\Components;

use Illuminate\View\Component;

class DataTable extends Component{

    public $tableID;

    public function __construct($tableID=null){
        $this->tableID = $tableID;
    }

    public function render()
    {
        return view('components.data-table');
    }
}
