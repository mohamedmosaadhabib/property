<?php

namespace App\View\Components;

use App\Models\UnitTypes;
use Illuminate\View\Component;

class UnitTypesIndex extends Component{
    public function render(){
        return view('components.unit-types-index',[
            "unitTypes"=>UnitTypes::latest()->get()
        ]);
    }
}
