<?php

namespace App\View\Components\layouts;

use Illuminate\View\Component;
use Illuminate\View\View;

class Title extends Component {

    public $title;
    public $subtitle;

    public function __construct(string $title="",string $subtitle="") {

        $this->title = $title;
        $this->subtitle = $subtitle;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.layouts.title');
    }
}
