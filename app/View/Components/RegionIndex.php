<?php

namespace App\View\Components;

use App\Models\Region;
use Illuminate\View\Component;

class RegionIndex extends Component{

    public function render(){
        return view('components.region-index',[
            "regions"=>Region::latest()->get()
        ]);
    }
}
