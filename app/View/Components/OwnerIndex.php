<?php

namespace App\View\Components;

use App\Models\Owner;
use Illuminate\View\Component;

class OwnerIndex extends Component{

    public function render(){
        return view('components.owner-index',[
            "owners"=>Owner::all()
        ]);
    }
}
