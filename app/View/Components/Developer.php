<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Developer extends Component{

    public $developer;

    public function __construct(\App\Models\Developer $developer ){
        $this->developer=$developer;
    }

    public function render(){
        return view('components.developer');
    }
}
