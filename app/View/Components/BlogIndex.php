<?php

namespace App\View\Components;

use App\Models\Blog;
use Illuminate\View\Component;

class BlogIndex extends Component{
    public $blogs;

    public function __construct($blogs){
        $this->blogs = $blogs;
    }

    public function render(){ return view('components.blog-index'); }
}
