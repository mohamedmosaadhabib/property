<?php

namespace App\View\Components;

use App\Models\Project;
use Illuminate\View\Component;

class ProjectComponent extends Component {
    /**
     * @var Project $project
     */
    public $project;

    public function __construct(Project $project){
        $this->project = $project;
    }

    public function render() { return view('components.project-component'); }

}
