<?php

namespace App\View\Components;

use App\Models\Property;
use Illuminate\View\Component;
use Illuminate\View\View;

class PropertyIndex extends Component
{
    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.property-index',[
            "properties"=>Property::all()
        ]);
    }
}
