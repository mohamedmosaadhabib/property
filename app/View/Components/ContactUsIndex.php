<?php

namespace App\View\Components;

use App\Models\Contact;
use Illuminate\View\Component;

class ContactUsIndex extends Component{

    public function render(){
        return view('components.contact-us-index',[
            "contacts"=>Contact::latest()->paginate()
        ]);
    }
}
