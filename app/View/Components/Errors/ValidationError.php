<?php

namespace App\View\Components\Errors;

use Illuminate\View\Component;

class ValidationError extends Component {
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $bag;

    /**
     * ValidationError constructor.
     * @param string $name
     * @param string $bag
     */
    public function __construct(string $name="",string $bag=""){
        $this->name = $name;
        $this->bag = $bag;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function render() {
        return view('components.errors.validation-error');
    }
}
