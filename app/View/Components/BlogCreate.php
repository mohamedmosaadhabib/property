<?php

namespace App\View\Components;

use App\Models\Blog;
use Illuminate\View\Component;

class BlogCreate extends Component{

    /**
     * @var Blog
     */
    public $blog;
    /**
     * @var bool
     */
    public $edit;
    public $image=null;
    public $name=[];
    public $description=[];

    public function __construct(Blog $blog=null, $edit=false){
        $this->blog = $blog;
        $this->edit = (bool)$edit;
        if ($edit){
            $this->image=$blog->imagePath;
            $this->name=$blog->getTranslations('name');
            $this->description=$blog->getTranslations('description');
        }
    }
    public function render()
    {
        return view('components.blog-create');
    }
}
