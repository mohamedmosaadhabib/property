<?php
namespace App\Traits;

use Illuminate\Database\Eloquent\Model;

trait HasAvatar{

    public static function avatarFiled():string{ return 'avatar'; }

    public static function avatarSelectedFiled():string{ return 'name'; }

    public function getImagePathAttribute():string { return url($this->{static::avatarFiled()} ?? "https://avatars.dicebear.com/api/initials/".$this->{static::avatarSelectedFiled()}.'.svg'); }

    public static function bootHasAvatar():void{
        static::creating(function (Model $model) {
            $model->{static::avatarFiled()} = $model->{static::avatarFiled()} ?? "https://avatars.dicebear.com/api/initials/".$model->{static::avatarSelectedFiled()}.'.svg';
        });
    }


}
