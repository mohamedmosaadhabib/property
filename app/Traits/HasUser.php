<?php


namespace App\Traits;


use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasUser
{
    /**
     * @return Guard|StatefulGuard
     */
    public static function UserGuard()
    {
//        $guard ?? config('auth.defaults.guard','web');
        return auth()->guard();
    }

    protected static function bootHasUser()
    {
        static::creating(function (Model $model) {
            $model->{self::UserIdField()} = $model->{self::UserIdField()} ?? (auth()->check() ? auth()->id() : null);
        });
    }

    /**
     * @return string
     */
    protected static function UserIdField(): string
    {
        return User::USER_ID ?? 'user_id';
    }

    public function initializeHasUser()
    {
        if (!in_array(static::UserIdField(), $this->fillable)) {
            $this->fillable[] = static::UserIdField();
        }
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, self::UserIdField())->withDefault([
            "name" => " No User "
        ]);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeOwn(Builder $builder): Builder
    {
        if (auth()->check()) {
            return $builder->where(self::UserIdField(), auth()->id());
        }
        return $builder;
    }

    /**
     * @param Builder $builder
     * @param $user_id
     * @return Builder
     */
    public function scopeFindByUserId(Builder $builder, $user_id): Builder
    {
        return $builder->where(self::UserIdField(), $user_id);
    }

}
