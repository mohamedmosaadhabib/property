<?php

namespace App\Traits;

use App\Models\Fileable;

trait HasFiles{

    public function files(){ return $this->morphMany(Fileable::class, Fileable::FILEABLE); }

}
