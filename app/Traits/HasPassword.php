<?php


namespace App\Traits;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

trait HasPassword{

    public static function passwordFiled(){ return 'password'; }

    public static function bootHasPassword(){
        static::creating(function (Model $model) {
            $model->attributes[static::passwordFiled()] = bcrypt($model->attributes[static::passwordFiled()] ?? Str::random(100));
        });
    }

    public function comparePassword($password){
        return Hash::check($password,$this->{static::passwordFiled()});
    }

    public function updatePassword(string $password){
        $this->update([self::passwordFiled()=>$password]);
        return $this;
    }
}
