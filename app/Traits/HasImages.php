<?php
namespace App\Traits;

use Illuminate\Database\Eloquent\Model;

trait HasImages{

    public static function imagesFiled():string{ return 'images'; }

    public static function imagesSelectedFiled():string{ return 'name'; }

    public function getImagesPathAttribute():array{
        return  collect($this->{static::avatarFiled()}?? ["https://avatars.dicebear.com/api/initials/".$this->{static::avatarSelectedFiled()}.".svg"])
                ->transform(function ($array){ return url($array ?? "https://avatars.dicebear.com/api/initials/".$this->{static::avatarSelectedFiled()}.".svg");})
                ->toArray();
    }

    public static function bootHasAvatar():void{
        static::creating(function (Model $model) {
            $model->{static::avatarFiled()} = $model->{static::avatarFiled()} ?? ["https://avatars.dicebear.com/api/initials/".$model->{static::avatarSelectedFiled()}.".svg"];
        });
    }


}
