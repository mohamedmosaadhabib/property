<?php


namespace App\Traits;


use App\Models\Notification;
use Illuminate\Notifications\HasDatabaseNotifications;
use Illuminate\Notifications\RoutesNotifications;

trait Notifiable{
    use HasDatabaseNotifications, RoutesNotifications;

    public function notifications(){ return $this->morphMany(Notification::class, 'notifiable')->orderBy('created_at', 'desc'); }


}
