<?php

use App\Models\Developer;
use App\Models\Project;
use App\Models\Property;
use App\Models\Region;
use App\Models\Setting;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Exceptions\UrlGenerationException;

/**
 * @param $key
 * @param null $default
 * @return string
 */
function setting($key, $default = null)
{
    return (config('app.setting', collect([])))->firstWhere('key', $key)->value ??
        Setting::firstOrCreate(['key' => $key], ['key' => $key, 'value' => $default ?? $key])->value ??
        $default;
}

/**
 * @return string
 */
function fileNameGenerator(): string
{
    return uniqid(str_replace(' ', '_', strtolower(config('app.name'))) . '_', false) . date('d_m_Y_H_i_s');
}

if (!function_exists('uploader')) {
    function uploader($file, string $folder = "", array $validation = []): string
    {
        $request = request();
        $isFile = $file instanceof UploadedFile || $file instanceof Livewire\TemporaryUploadedFile;
        // remove any / char form var
        $path = rtrim($folder, '/');
        // validate Image
        if (!$isFile) {
            if (empty($validation)) $request->validate([$file => ['required', 'image', 'mimes:jpeg,jpg,png']]);
            else $request->validate([$file => $validation]);
        }

        $image = $isFile ? $file : $request->file($file);
//        dd($isFile,fileNameGenerator());
        $filename = fileNameGenerator() . '.' . $image->getClientOriginalExtension();
        $image->storeAs($path, $filename);
        return str_replace('//', '/', 'uploads/' . $path . '/' . $filename);
    }
}
/**
 * @param Developer $developer
 * @param Project $project
 * @param Region|null $region
 * @return string
 */
function showDeveloper(Developer $developer, Project $project, Region $region = null): string
{
    if (!isset($region->id)) {
        $region = Region::withoutTrashed()->firstWhere('id', $project->location['region'] ??1) ?? Region::first();
    }
    try {

        return route('developer_project.show', [
            "developer" => $developer,
            "project" => $project,
            "location" => $region->slug,
        ]);
    } catch (UrlGenerationException $exception) {
        return url('');
    }
}

/**
 * @param $word
 * @param Region $value
 * @param null $region
 * @param null $city
 * @param null $street
 * @param null $type_for
 * @return array
 */
function routeHandler($word,Region $value,$region=null,$city=null,$street=null,$type_for=null) {
    $data = [];

    if (($region instanceof \App\Models\Region)) {
        $data['region']=$region->slug;
    }elseif (is_string($region)) {
        $data['region']=$value->slug;
        $data['city']=$region;
    }

    if (($city instanceof \App\Models\Region)) {
        $data['city']=$city->slug;
    }elseif (is_string($city)){
        $data['city']=$value->slug;
        $data['street']=$city;
    }

    if (($street instanceof \App\Models\Region)) {
        $data['street']=$street->slug;
    }elseif (is_string($street)){
        $data['street']=$value->slug;
        $data['type_for']=$street;
    }
    if (is_string($type_for)) {
        $data['type_for']=$type_for;
    }
    return ["route"=>($word=='rent' ? 'rent':$word) ,"data"=>$data];
}

function handelBackRoute($backTo=null) {
    $p = request()->route()->parameters();
    $routeName= request()->route()->getName();
    if (preg_match('/-for-/',$p[$backTo])) {
        return url()->current();
    }
    $data['region']=$p['region'];

    if ($backTo=="region") {
        $data['region']=$p['region'];
        $data['city']=$p['type_for'] ?? $p['street'] ?? $p['city'] ?? null;
        return route($routeName,$data);
    }
    $data['city']=$p['city'];
    if ($backTo=="city") {
        $data['street']=$p['type_for'] ?? $p['street'] ?? null;
        return route($routeName,$data);
    }
    $data['street']=$p['street'];
    if ($backTo=="street") {
        $data['type_for']=$p['type_for'] ??null;
        return route($routeName,$data);
    }
    return  route($routeName);
}

/**
 * @param Property $property
 * @param string|null $property_type
 * @return string
 */
function showProperty(Property $property, string $property_type = null): string
{
    $city = Region::find($property->location['city']);
    $region = Region::find($property->location['region']);
    return route('single.property', [
        "property_type" => $property->property_type,
        "property" => $property,
        "city" => $city->name,
        "region" => $region->name,
        "name" => $property->name,
        "type" => (preg_match('/rent/',($property_type ?? $property->guessType())))?'rent':'resale'
    ]);
}
