<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider{
    const NAMESPACE='App\Http\Controllers';
    protected $namespace = self::NAMESPACE;
    protected $namespaceDashboard = self::NAMESPACE.'\Dashboard';

    public const HOME = '/dashboard';
    public const DASHBOARD = '/dashboard';

    public function boot(){
        parent::boot();
    }

    public function map() {

        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapDashboardRoutes();

        //
    }

    protected function mapWebRoutes() {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    protected function mapApiRoutes() {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    protected function mapDashboardRoutes(){
        Route::middleware('web')
            ->namespace($this->namespaceDashboard)
            ->group(base_path('routes/dashboard.php'));
    }
}
