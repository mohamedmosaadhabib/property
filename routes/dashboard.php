<?php
use Illuminate\Support\Facades\Route;
Route::prefix('dashboard')->name('dashboard.')->middleware(['auth'])->group(function () {
    Route::post('/register', '\App\Http\Controllers\Auth\RegisterController@register')->name('register');

    Route::get('/', 'DashboardController@index')->name('home');

    Route::get('/contact/us', 'DashboardController@contactUs')->name('contact_us');

    Route::get('/change/password', 'DashboardController@change_password')->name('change_password');
    Route::post('/change/password/user', 'DashboardController@change_password_user')->name('change_password_user');

    Route::post('property/{property}/active/toggle','PropertyCtrl@toggleActive')->name('property.toggleActive');
    Route::post('contact/{contact}/status/read_at','ContactCtrl@toggleStatus')->name('contact.toggleStatus');

    Route::resource('developer','DevCtrl')->except(['show','store','update']);
    Route::resource('user','UserCtrl');
    Route::resource('project','ProjectCtrl');
    Route::resource('owner','OwnerCtrl');
    Route::resource('setting','SettingCtrl');
    Route::resource('property','PropertyCtrl');
    Route::resource('contact','ContactCtrl');
    Route::resource('region','RegionCtrl');
    Route::resource('blog','BlogCtrl');
    Route::resource('unitTypes','UnitTypesCtrl');
    Route::resource('slider','SliderController');

});

Route::get('{type}/{region}/{city}/{property:slug}/{name}','\App\Http\Controllers\FrontController@SingleProperty')->name('single.property')->where('type','resale|rent');

Route::get('rent/{region?}/{city?}/{street?}/{type_for?}','\App\Http\Controllers\FrontController@rant')->name('rent');
Route::get('resale/{region?}/{city?}/{street?}/{type_for?}','\App\Http\Controllers\FrontController@resale')->name('resale');

Route::get('/{location}/{developer:slug}/{project:slug?}','\App\Http\Controllers\FrontController@SingleProject')->name('developer_project.show');


Route::get('/{developer:slug}','\App\Http\Controllers\FrontController@show')->name('developer.show');
