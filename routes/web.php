<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['register'=>false]);

Route::redirect('/home', '/dashboard')->name('home');

Route::get('setLocale/{locale?}', 'FrontController@setLocale')->where('locale','en|ar')->name('locale');

Route::get('/','FrontController@main')->name('main');

Route::get('dashboard','FrontController@dashboard')->name('dashboard');
Route::get('pricing','FrontController@pricing')->name('pricing');
Route::get('location/{location:slug}','FrontController@location')->name('location');
Route::get('about','FrontController@about')->name('about_us');
Route::get('developer','FrontController@developers_page')->name('developer.index');
//Route::get('developer/{developer:slug}/projects/{project}','FrontController@SingleProject')->name('developer_project.show');
//Route::get('developer/{developer:slug}','FrontController@show')->name('developer.show');


Route::get('contact','FrontController@contact_create')->name('contact_us');

Route::post('contact_project_post/{project}','FrontController@contact_project_post')->name('contact_project_post');

Route::post('contact_property_post/{property}','FrontController@contact_property_post')->name('contact_property_post');

Route::get('project','FrontController@project')->name('project');
Route::get('property','FrontController@SubmitProperty')->name('SubmitProperty');
//Route::get('property/{property:slug}','FrontController@SingleProperty')->name('single.property');
//Route::get('project/{project:slug}','FrontController@SingleProject')->name('single.project');
Route::get('blog/{blog:slug}','FrontController@SingleBlog')->name('single.blog');
Route::get('blogs/','FrontController@blogs')->name('blogs');
//Route::get('rent','FrontController@rant')->name('rent');
//Route::get('resale','FrontController@resale')->name('resale');

Route::get('autocomplete','FrontController@autocomplete')->name('autocomplete');

Route::get('/home', 'HomeController@index')->name('change_locale');

