
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.owner_edit')])
        <livewire:owner-create edit="true" :owner="$owner"/>
    @endcomponent


@stop
