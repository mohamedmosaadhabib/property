
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.owner_create')])
        <livewire:owner-create />
    @endcomponent


@stop
