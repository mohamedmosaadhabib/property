
@extends('layouts.dashboard.dashboard')
@push('css')
    <link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endpush
@push('js')
    <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
@endpush
@section('content')
    @component('card-component',['title'=>ucfirst(trans('main.sliders'))])

        @component('components.data-table',['tableID'=>"tableslider"])
            @slot('thead')
                <tr>
                    <th> @lang('main.id') </th>
                    <th>@lang('main.order_by')</th>
                    <th>@lang('main.image')</th>
                    <th> @lang('main.edit') </th>
                    <th> @lang('main.delete') </th>
                </tr>
            @endslot
            @slot('tbody')
                @foreach ($sliders as $slider)
                    <tr>
                        <td>{{$slider->id}}</td>
                        <td>{{$slider->order_by}}</td>
                        <td><img height="100" src="{{$slider->imagePath}}" alt=""></td>
                        <td>
                            <a href="{{route('dashboard.slider.edit',$slider)}}" class="btn btn-sm btn-warning">@lang('main.edit')</a>
                        </td>
                        <td>
                            <form name="sliderDelete{{$slider->id}}" action="{{route('dashboard.slider.destroy',$slider)}}" method="post">@csrf @method('DELETE')</form>
                            <button type="button" onclick="confirm(' @lang('main.slider_delete') ')?sliderDelete{{$slider->id}}.submit():''" class="btn btn-sm btn-danger">@lang('main.delete')</button>
                        </td>
                    </tr>
                @endforeach
            @endslot

        @endcomponent

        <div class="text-right"> <div class="d-inline-block"></div> </div>

        @push('js')
            <script>
                $(document).ready(function() {
                    $('#tableslider').DataTable({
                        // paginate:false
                    });
                });
            </script>
        @endpush


    @endcomponent


@stop
