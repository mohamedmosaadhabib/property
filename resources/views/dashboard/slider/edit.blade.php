
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.slider_edit')])
        <form action="{{route('dashboard.slider.update',$slider)}}" method="post" enctype="multipart/form-data">@csrf @method('PUT')
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="image">@lang('main.image')</label>
                        <input type="file" onchange="readURL(this,'imgPre')" class="form-control" name="image" id="image" aria-describedby="helpId" placeholder="@lang('main.image')">
                        <x-errors.validation-error name="image"/>
                    </div>
                    <div class="form-group">
                        <label for="order_by">@lang('main.order_by')</label>
                        <input type="number" class="form-control" name="order_by" value="{{old('order_by',$slider->order_by)}}" id="order_by" aria-describedby="helpId" placeholder="@lang('main.order_by')">
                        <x-errors.validation-error name="order_by"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="img-thumbnail">
                        <img src="{{$slider->imagePath}}" id="imgPre" class="w-100" alt="">
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">@lang('main.save')</button>
        </form>


    @endcomponent


@stop
