
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.developer_edit')])
        <livewire:developer-create edit="true" :developer="$developer"/>
    @endcomponent


@stop
