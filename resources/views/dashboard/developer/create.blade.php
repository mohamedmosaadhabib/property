
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.developer_create')])
        <livewire:developer-create />
    @endcomponent


@stop
