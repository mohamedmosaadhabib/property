@push('css')
    <link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endpush
@push('js')
    <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
@endpush
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>ucfirst(trans('main.units'))])

        <x-property-index/>

    @endcomponent


@stop
