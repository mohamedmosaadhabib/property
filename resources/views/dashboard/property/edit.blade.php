
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.unit_edit')])
        <livewire:property-create edit="true" :property="$property"/>
    @endcomponent


@stop
