
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.unit_create')])
        <livewire:property-create />
    @endcomponent


@stop
