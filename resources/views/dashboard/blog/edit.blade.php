
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.blog_edit')])
        <x-blog-create edit="true" :blog="$blog"/>
    @endcomponent

@stop
