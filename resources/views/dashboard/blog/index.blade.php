
@extends('layouts.dashboard.dashboard')
@push('css')
    <link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endpush
@push('js')
    <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
@endpush
@section('content')
    @component('card-component',['title'=>ucfirst(trans('main.blogs'))])

        <x-blog-index :blogs="$blogs"/>

    @endcomponent


@stop
