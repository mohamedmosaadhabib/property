
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.blog_create')])
        <x-blog-create />
    @endcomponent


@stop
