
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.project_create')])
        <livewire:project-create />
    @endcomponent


@stop
