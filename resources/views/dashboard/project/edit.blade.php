
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.project_edit')])
        <livewire:project-create edit="true" :project="$project"/>
    @endcomponent


@stop
