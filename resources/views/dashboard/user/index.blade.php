
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>ucfirst(trans('main.users'))])

        <table class="table table-hover  table-sm">
            <thead class="thead-default">
            <tr>
                <th>@lang('main.id')</th>
                <th>@lang('main.name')</th>
                <th>@lang('main.email')</th>
                <th>@lang('main.is_admin')</th>
                <th>@lang('main.edit')</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users ?? \App\Models\User::latest()->get() as $user)
            <tr>
                <td scope="row">{{$loop->index+1}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td class="{{$user->is_admin?' text-success ':' text-danger '}}">{{$user->is_admin?__('main.admin'):__('main.not_admin')}}</td>
                <td><a href="{{route('dashboard.user.edit',$user)}}" class="btn btn-sm btn-warning">@lang('main.edit')</a></td>
            </tr>
            @endforeach
            </tbody>
        </table>

    @endcomponent


@stop
