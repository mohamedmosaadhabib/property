
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.user_create')])
        <form method="POST" action="{{ route('dashboard.register') }}">
            @csrf

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('main.name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"  placeholder="@lang('main.name')" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">@lang('main.email')</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="@lang('main.email')"  name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('main.password')</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="@lang('main.password')"  name="password" required autocomplete="new-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">@lang('main.password_confirmation')</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="@lang('main.password_confirmation')"  required autocomplete="new-password">
                </div>
            </div>


            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('main.is_admin') }}</label>

                <div class="col-md-6">
                    <div class="custom-control custom-radio custom-control-inline @error('is_admin') is-invalid @enderror">
                        <input type="radio" id="customRadioInline1" checked value="1" name="is_admin" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline1">@lang('main.yes')</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline @error('is_admin') is-invalid @enderror">
                        <input type="radio" id="customRadioInline2" name="is_admin" value="0" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline2">@lang('main.no')</label>
                    </div>

                    @error('is_admin')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>


            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('main.register') }}
                    </button>
                </div>
            </div>
        </form>
    @endcomponent


@stop
