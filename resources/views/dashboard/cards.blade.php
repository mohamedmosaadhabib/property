@extends('layouts.dashboard.dashboard')
@section('content')

    <!-- Begin Page Content -->

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Cards</h1>
    </div>

    <div class="row">


        <div class="col-lg-12">
            @component('card-component')
                @slot('title') title @endslot
                @slot('menu')
                        <div class="dropdown-header">Dropdown Header:</div>
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                @endslot
            @endcomponent

        </div>

    </div>

    <!-- /.container-fluid -->

@stop
