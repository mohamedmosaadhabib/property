
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.region_create')])
        <livewire:region-create />
    @endcomponent


@stop
