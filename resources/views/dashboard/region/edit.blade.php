
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.region_edit')])
        <livewire:region-create edit="true" :region="$region"/>
    @endcomponent


@stop
