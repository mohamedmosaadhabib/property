
@extends('layouts.dashboard.dashboard')

@section('content')

    @component('card-component',['title'=>ucfirst(trans('main.settings'))])

        <div class="row">

            @foreach (\App\Models\Setting::all() as $setting)

                <div class="col-md-4 mb-4 ">

                    <div class="card">

                        <div class="card-body pb-0">

                            <h4 class="card-title">{{ucwords(str_replace('_',' ',$setting->key))}}</h4>

                            <form method="POST" action="{{route('dashboard.setting.update',$setting)}}" class="row my-1">@csrf @method('put')

                                <textarea name="value" class="w-100 form-control" id="value" rows="3" >{{$setting->value}}</textarea>

                                <button class="btn btn-sm btn-success my-2">@lang('main.save')</button>

                            </form>

                        </div>

                    </div>

                </div>

            @endforeach

        </div>

    @endcomponent

@stop
