@extends('layouts.dashboard.dashboard')
@section('content')
    @component('card-component',['title'=>ucfirst(trans('main.change_password'))])
        <form action="{{route('dashboard.change_password_user')}}" method="post">@csrf
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('main.current_password') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control @error('current_password') is-invalid @enderror" name="current_password" required autocomplete="current_password">
                    @error('current_password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('main.password') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" minlength="8" required autocomplete="password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('main.confirm_password') }}</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" minlength="8" required autocomplete="password">
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('main.change_password') }}
                    </button>
                </div>
            </div>
        </form>
    @endcomponent
@stop
