
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.unit_types_create')])
        <livewire:unit-types-create />
    @endcomponent


@stop
