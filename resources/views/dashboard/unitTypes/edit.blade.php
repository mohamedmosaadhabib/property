
@extends('layouts.dashboard.dashboard')

@section('content')
    @component('card-component',['title'=>trans('main.unit_types_edit')])
        <livewire:unit-types-create edit="true" :unitTypes="$unitTypes"/>
    @endcomponent


@stop
