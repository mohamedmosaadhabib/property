<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">

            <span>@lang('main.copy_rigth',['year'=>date('Y'),'app_name'=>setting('app_name_'.app()->getLocale()),'company'=>"<a target='_blank' href='https://habib.tech'>".trans('main.web_site_dev')."</a>"])</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->
