<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion text-capitalize" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{url('/')}}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"> {{config('app.name')}} </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('dashboard.home')}}"> <i class="fas fa-fw fa-tachometer-alt"></i> <span>@lang('main.dashboard')</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading"> @lang('main.interface') </div>

    @component('collapse-menu',['id'=>"developers",'title'=>ucfirst(trans('main.developers'))])
        @slot('icon') <i class="fas fa-fw fa-cubes"></i> @endslot
        <h6 class="collapse-header">@lang('main.developers')</h6>
        <a class="collapse-item" href="{{route('dashboard.developer.index')}}">@lang('main.developers')</a>
        <a class="collapse-item" href="{{route('dashboard.developer.create')}}">@lang('main.create')</a>
    @endcomponent


    @component('collapse-menu',['id'=>"projects",'title'=>ucfirst(trans('main.projects'))])
        @slot('icon') <i class="fas fa-fw fa-cubes"></i> @endslot
        <h6 class="collapse-header">@lang('main.projects')</h6>
        <a class="collapse-item" href="{{route('dashboard.project.index')}}">@lang('main.projects')</a>
        <a class="collapse-item" href="{{route('dashboard.project.create')}}">@lang('main.create')</a>
    @endcomponent

    @component('collapse-menu',['id'=>"blogs",'title'=>ucfirst(trans('main.blogs'))])
        @slot('icon') <i class="fas fa-fw fa-cubes"></i> @endslot
        <h6 class="collapse-header">@lang('main.blogs')</h6>
        <a class="collapse-item" href="{{route('dashboard.blog.index')}}">@lang('main.blogs')</a>
        <a class="collapse-item" href="{{route('dashboard.blog.create')}}">@lang('main.create')</a>
    @endcomponent

    @component('collapse-menu',['id'=>"region",'title'=>ucfirst(trans('main.region'))])
        @slot('icon') <i class="fas fa-fw fa-cubes"></i> @endslot
        <h6 class="collapse-header">@lang('main.region')</h6>
        <a class="collapse-item" href="{{route('dashboard.region.index')}}">@lang('main.region')</a>
        <a class="collapse-item" href="{{route('dashboard.region.create')}}">@lang('main.create')</a>
    @endcomponent

    @component('collapse-menu',['id'=>"unit_types",'title'=>ucfirst(trans('main.unit_types'))])
        @slot('icon') <i class="fas fa-fw fa-cubes"></i> @endslot
        <h6 class="collapse-header">@lang('main.unit_types')</h6>
        <a class="collapse-item" href="{{route('dashboard.unitTypes.index')}}">@lang('main.unit_types')</a>
        <a class="collapse-item" href="{{route('dashboard.unitTypes.create')}}">@lang('main.create')</a>
    @endcomponent


    <!-- Divider -->
    <hr class="sidebar-divider">
    @component('collapse-menu',['id'=>"owners",'title'=>ucfirst(trans('main.owners'))])
        @slot('icon') <i class="fas fa-fw fa-cubes"></i> @endslot
        <h6 class="collapse-header">@lang('main.owners')</h6>
        <a class="collapse-item" href="{{route('dashboard.owner.index')}}">@lang('main.owners')</a>
        <a class="collapse-item" href="{{route('dashboard.owner.create')}}">@lang('main.create')</a>
    @endcomponent
    <!-- Divider -->

    @component('collapse-menu',['id'=>"properties",'title'=>ucfirst(trans('main.units'))])
        @slot('icon') <i class="fas fa-fw fa-cubes"></i> @endslot
        <h6 class="collapse-header">@lang('main.units')</h6>
        <a class="collapse-item" href="{{route('dashboard.property.index')}}">@lang('main.units')</a>
        <a class="collapse-item" href="{{route('dashboard.property.create')}}">@lang('main.create')</a>
    @endcomponent

    <hr class="sidebar-divider">

    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('dashboard.contact_us')}}"> <i class="fas fa-fw fa-chart-area"></i> <span>@lang('main.contact_title')</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    @component('collapse-menu',['id'=>"user",'title'=>ucfirst(trans('main.user'))])
        @slot('icon') <i class="fas fa-fw fa-cubes"></i> @endslot
        <h6 class="collapse-header">@lang('main.user')</h6>
        <a class="collapse-item" href="{{route('dashboard.user.index')}}">@lang('main.user')</a>
        <a class="collapse-item" href="{{route('dashboard.user.create')}}">@lang('main.create')</a>
    @endcomponent

    <hr class="sidebar-divider">
    @component('collapse-menu',['id'=>"sliders",'title'=>ucfirst(trans('main.sliders'))])
        @slot('icon') <i class="fas fa-fw fa-cubes"></i> @endslot
        <h6 class="collapse-header">@lang('main.sliders')</h6>
        <a class="collapse-item" href="{{route('dashboard.slider.index')}}">@lang('main.sliders')</a>
        <a class="collapse-item" href="{{route('dashboard.slider.create')}}">@lang('main.create')</a>
    @endcomponent
    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('dashboard.setting.index')}}"> <i class="fas fa-fw fa-cogs"></i> <span>@lang('main.settings')</span></a>
    </li>


<!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
