@component('components.data-table',['tableID'=>"tableproperty"])
    @slot('thead')
        <tr>
            <th> @lang('main.id') </th>
            <th> @lang('main.name') </th>
            <th> @lang('main.owner') </th>
            <th> @lang('main.property_type') </th>
            <th> @lang('main.property_views') </th>
            <th> @lang('main.active') </th>
            <th> @lang('main.edit') </th>
            <th> @lang('main.delete') </th>
        </tr>
    @endslot

    @slot('tbody')
        @foreach ($properties as $property)
            <tr>
                <td>{{$property->id}}</td>
                <td>{{$property->name}}</td>
                <td>{{$property->owner->name}}</td>
                <td>{{__('main.'.$property->property_type)}}</td>
                <td>{{__('main.'.$property->property_views)}}</td>
                <td>
                    <form name="toggleActive{{$loop->index}}" action="{{route('dashboard.property.toggleActive',$property)}}" method="post">@csrf</form>
                    <button onclick="confirm(' @lang('main.change_status') ')?toggleActive{{$loop->index}}.submit():''" class="btn btn-sm  {{$property->active?' btn-success ':'btn-danger'}} ">{{$property->active?__('main.active'):__('main.not_active')}}</button>
                </td>
                <td>
                    <a href="{{route('dashboard.property.edit',$property)}}" class="btn btn-sm btn-warning">@lang('main.edit')</a>
                </td>
                <td>
                    <form name="propertyDelete{{$property->id}}" action="{{route('dashboard.property.destroy',$property)}}" method="post">@csrf @method('DELETE')</form>
                    <button type="button" onclick="confirm(' @lang('main.property_delete') ')?propertyDelete{{$property->id}}.submit():''" class="btn btn-sm btn-danger">@lang('main.delete')</button>
                </td>
            </tr>
        @endforeach
    @endslot

@endcomponent

<div class="text-right"> <div class="d-inline-block"></div> </div>

@push('js')
    <script>
        $(document).ready(function() {
            $('#tableproperty').DataTable({
                // paginate:false
            });
        });
    </script>
@endpush
