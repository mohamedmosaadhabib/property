
<!-- ============================ Page Title Start================================== -->
<div class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">

                <h2 class="ipt-title">{!! ucfirst($title) !!}</h2>
                @isset($subtitle)
                    <span class="ipn-subtitle">{!! $subtitle !!}</span>
                @endisset
            </div>
        </div>
    </div>
</div>
<!-- ============================ Page Title End ================================== -->
