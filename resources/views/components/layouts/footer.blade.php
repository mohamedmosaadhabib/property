<!-- ============================ Footer Start ================================== -->
<footer class="dark-footer skin-dark-footer">
    <div>
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h4 class="widget-title">@lang('main.about_work')</h4>
                        <p>{{setting('about_work_'.app()->getLocale(),'Themez Hub is a team of young professionals that has been successfully creating Creative Website templates already for several years.')}}</p>

                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h4 class="widget-title">@lang('main.useful_links')</h4>
                        <ul class="footer-menu">
                            <li><a href="{{route('about_us')}}">@lang('main.about_us')</a></li>
                            <li><a href="{{route('contact_us')}}">@lang('main.contact_us')</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h4 class="widget-title">@lang('main.get_in_touch')</h4>
                        <div class="fw-address-wrap">
                            <div class="fw fw-location">{{setting('address_'.app()->getLocale(),'7744 North Park, New York')}}</div>
                            <div class="fw fw-mail">
                                {{setting('email','support@drizvato77.com')}}
                            </div>
                            <div class="fw fw-call"> {{setting('phone','+91 235 548 7548')}} </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h4 class="widget-title">@lang('main.follow_us')</h4>
                        <p>@lang('main.follow_us_descripion')</p>
                        <ul class="footer-bottom-social">
                            @if (!empty(setting('facebook','')) && !is_null(setting('facebook','')))
                                <li><a href="{{setting('facebook','')}}"><i class="ti-facebook"></i></a></li>
                            @endif
                            @if (!empty(setting('twitter','')) && !is_null(setting('twitter','')))
                                <li><a href="{{setting('twitter','')}}"><i class="ti-twitter"></i></a></li>
                            @endif
                            @if (!empty(setting('instagram','')) && !is_null(setting('instagram','')))
                                <li><a href="{{setting('instagram','')}}"><i class="ti-instagram"></i></a></li>
                            @endif
                            @if (!empty(setting('linkedin','')) && !is_null(setting('linkedin','')))
                                <li><a href="{{setting('linkedin','')}}"><i class="ti-linkedin"></i></a></li>
                            @endif
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-lg-12 col-md-12 text-center">
                    <p class="mb-0">@lang('main.copy_rigth',['year'=>date('Y'),'app_name'=>setting('app_name_'.app()->getLocale())])</p>
                </div>

            </div>
        </div>
    </div>
</footer>
<!-- ============================ Footer End ================================== -->
@auth
    <!-- Log In Modal -->
    <div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logoutmodal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered login-pop-form" role="document">
            <div class="modal-content" id="logoutmodal">
                <span class="mod-close" data-dismiss="modal" aria-hidden="true"><i class="ti-close"></i></span>
                <div class="modal-body">
                    <h4 class="modal-header-title">@lang('main.log_out')</h4>
                    <div class="login-form">
                        <form action="{{route('logout')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <button type="submit" class="btn btn-md full-width p btn-danger">@lang('main.are_you_sure')</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

@endauth
<a id="back2Top" class="top-scroll" title="@lang('main.back_to_top')" href="#"><i class="ti-arrow-up"></i></a>

