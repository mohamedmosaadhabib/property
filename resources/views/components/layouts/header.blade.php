<!-- Start Navigation -->
<div class="header header-light">
    <nav class="headnavbar">
        <div class="nav-header">
            <h3 class="brand w-auto">
                <a href="{{url('/')}}">
                    {{setting('app_name_'.app()->getLocale())}}
                </a>
            </h3>
            <button class="toggle-bar"><span class="ti-align-justify"></span></button>
        </div>
        <ul class="menu">

            <x-nav-link :href="url('/')" :hasMenu="false" :linkName="trans('main.home_page')" />
            <x-nav-link :href="route('contact_us')" :hasMenu="false" :linkName="trans('main.contact')" />
            <x-nav-link :href="route('blogs')" :hasMenu="false" :linkName="trans('main.blogs')" />
            <x-nav-link :href="route('rent')" :hasMenu="false" :linkName="trans('main.rent')" />
            <x-nav-link :href="route('resale')" :hasMenu="false" :linkName="trans('main.resale')" />
            <x-nav-link :href="route('developer.index')" :hasMenu="false" :linkName="trans('main.developers')" />
            <x-nav-link :href="route('about_us')" :hasMenu="false" :linkName="trans('main.about_us')" />
            <x-nav-link :href="route('locale',app()->getLocale()==='ar'?'en':'ar')" :hasMenu="false" :linkName="ucfirst(app()->getLocale()==='ar'?'en':'عربي')" />

        </ul>
        <ul class="attributes">
            @auth
                <li class="login-attri theme-log"><a class="bg-danger" href="#" data-toggle="modal" data-target="#logout">@lang('main.logout')</a></li>
            @endauth
        </ul>

    </nav>
</div>
<!-- End Navigation -->
