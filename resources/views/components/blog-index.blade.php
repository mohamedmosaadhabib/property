@component('components.data-table',['tableID'=>"tableblog"])
    @slot('thead')
        <tr>
            <th> @lang('main.id') </th>
            <th> @lang('main.name') </th>
            <th> @lang('main.description') </th>
            <th> @lang('main.image') </th>
            <th> @lang('main.edit') </th>
            <th> @lang('main.delete') </th>
        </tr>
    @endslot
    @slot('tbody')
        @foreach ($blogs as $blog)
            <tr>
                <td>{{$blog->id}}</td>
                <td>{{$blog->name}}</td>
                <td>{{$blog->description}}</td>
                <td>
                    <img src="{{$blog->imagePath}}" style="width:50px" />
                </td>
                <td>
                    <a href="{{route('dashboard.blog.edit',$blog)}}" class="btn btn-sm btn-warning">@lang('main.edit')</a>
                </td>
                <td>
                    <form name="blogDelete{{$blog->id}}" action="{{route('dashboard.blog.destroy',$blog)}}" method="post">@csrf @method('DELETE')</form>
                    <button type="button" onclick="confirm(' @lang('main.blog_delete') ')?blogDelete{{$blog->id}}.submit():''" class="btn btn-sm btn-danger">@lang('main.delete')</button>
                </td>
            </tr>
        @endforeach
    @endslot

@endcomponent

<div class="text-right"> <div class="d-inline-block"></div> </div>

@push('js')
    <script>
        $(document).ready(function() {
            $('#tableblog').DataTable({
                // paginate:false
            });
        });
    </script>
@endpush
