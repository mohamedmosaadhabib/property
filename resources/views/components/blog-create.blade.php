<form method="POST" class="row" action="{{$edit?route('dashboard.blog.update',$blog):route('dashboard.blog.store')}}" enctype="multipart/form-data">@csrf @if($edit) @method('put') @endif
    <div class="form-group col-md-6">
        <label for="name">@lang('main.name_ar')</label>
        <input type="text" minlength="3" class="form-control" name="name[ar]" value="{{old('name.ar',$name['ar']??'')}}" id="name" aria-describedby="helpId" placeholder="@lang('main.name_ar')">
        <x-errors.validation-error name="name.ar"/>
        <x-errors.validation-error name="name"/>
    </div>

    <div class="form-group col-md-6">
        <label for="name">@lang('main.name_en')</label>
        <input type="text" minlength="3" class="form-control" name="name[en]" value="{{old('name.en',$name['en']??'')}}" id="name" aria-describedby="helpId" placeholder="@lang('main.name_en')">
        <x-errors.validation-error name="name.en"/>
        <x-errors.validation-error name="name"/>
    </div>


    <div class="form-group col-md-6">
        <label for="description">@lang('main.description_ar')</label>
        <textarea type="text" minlength="3" class="form-control" name="description[ar]"  id="description" aria-describedby="helpId" placeholder="@lang('main.description_ar')">{{old('description.ar',$description['ar']??'')}}</textarea>
        <x-errors.validation-error name="description.ar"/>
        <x-errors.validation-error name="description"/>
    </div>


    <div class="form-group col-md-6">
        <label for="description">@lang('main.description_en')</label>
        <textarea type="text" minlength="3" class="form-control" name="description[en]"  id="description" aria-describedby="helpId" placeholder="@lang('main.description_en')">{{old('description.en',$description['en']??'')}}</textarea>
        <x-errors.validation-error name="description.en"/>
        <x-errors.validation-error name="description"/>
    </div>

    <div class="form-group col-md-12">
        <label for="">@lang('main.image')</label>
        <input type="file" class="form-control" name="image"/>
        <x-errors.validation-error name="image"/>
    </div>
    <div class="col-md-12">
        <button type="submit" class="btn {{$edit?'btn-warning':'btn-primary'}}">@lang('main.'.($edit?'edit':'save'))</button>
    </div>
    @if ($image)
        <div class="form-group col-md-12">
            <hr>

            <img src="{{$image}}" class="w-25" alt="">
        </div>
    @endif
</form>
