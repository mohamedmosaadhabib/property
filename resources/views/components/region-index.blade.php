@component('components.data-table',['tableID'=>"tableregion"])
    @slot('thead')
        <tr>
            <th> @lang('main.id') </th>
            <th>@lang('main.name')</th>
            <th>@lang('main.type')</th>
            <th>@lang('main.part_of')</th>
            <th>@lang('main.edit')</th>
            <th> @lang('main.delete') </th>
        </tr>
    @endslot
    @slot('tbody')
        @foreach ($regions as $region)
            <tr>
                <td>{{$region->id}}</td>
                <td>{{$region->name}}</td>
                <td>{{$region->typeText}}</td>
                <td>{{$region->parent->name??  __('main.main')}}</td>
                <td> <a href="{{route('dashboard.region.edit',$region)}}" class="btn btn-sm btn-warning">@lang('main.edit')</a> </td>
                <td>
                    <form name="regionDelete{{$region->id}}" action="{{route('dashboard.region.destroy',$region)}}" method="post">@csrf @method('DELETE')</form>
                    <button type="button" onclick="confirm(' @lang('main.region_delete') ')?regionDelete{{$region->id}}.submit():''" class="btn btn-sm btn-danger">@lang('main.delete')</button>
                </td>
            </tr>
        @endforeach
    @endslot

@endcomponent

<div class="text-right"> <div class="d-inline-block"></div> </div>

@push('js')
    <script>
        $(document).ready(function() {
            $('#tableregion').DataTable({
                // paginate:false
            });
        });
    </script>
@endpush
