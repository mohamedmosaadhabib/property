<div class="property-listing property-2">

    <div class="listing-img-wrapper">
        <div class="list-img-slide">
            <div class="click">
                @foreach ($property->images??[] as $image)
                <div><a href="{{showProperty($property)}}"><img src="{{url($image)}}" class="img-fluid mx-auto" alt="" /></a></div>
                @endforeach
            </div>
        </div>
        <span class="property-type">@lang('main.for_sale')</span>
    </div>

    <div class="listing-detail-wrapper pb-0">
        <div class="listing-short-detail">
            <h4 class="listing-name"><a href="{{showProperty($property)}}">{{$property->name}}</a><i class="list-status ti-check"></i></h4>
        </div>
    </div>

    <div class="price-features-wrapper">
        <div class="listing-price-fx">
            <h6 class="listing-card-info-price price-prefix">{{$property->sale['value']}}</h6>
        </div>
        <div class="list-fx-features">
            <div class="listing-card-info-icon">
                <span class="inc-fleat inc-bed">{{$property->bed_rooms}} @lang('main.beds')</span>
            </div>
            <div class="listing-card-info-icon">
                <span class="inc-fleat inc-bath">{{$property->bathrooms}} @lang('main.bath')</span>
            </div>
        </div>
    </div>

</div>
