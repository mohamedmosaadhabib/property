@component('components.data-table',['tableID'=>"tableunitType"])
    @slot('thead')
        <tr>
            <th> @lang('main.id') </th>
            <th>@lang('main.name')</th>
            <th>@lang('main.edit')</th>
            <th> @lang('main.delete') </th>
        </tr>
    @endslot
    @slot('tbody')
        @foreach ($unitTypes as $unitType)
            <tr>
                <td>{{$unitType->id}}</td>
                <td>{{$unitType->name}}</td>
                <td> <a href="{{route('dashboard.unitTypes.edit',$unitType)}}" class="btn btn-sm btn-warning">@lang('main.edit')</a> </td>
                <td>
                    <form name="unitTypeDelete{{$unitType->id}}" action="{{route('dashboard.unitTypes.destroy',$unitType)}}" method="post">@csrf @method('DELETE')</form>
                    <button type="button" onclick="confirm(' @lang('main.unitType_delete') ')?unitTypeDelete{{$unitType->id}}.submit():''" class="btn btn-sm btn-danger">@lang('main.delete')</button>
                </td>
            </tr>
        @endforeach
    @endslot

@endcomponent

<div class="text-right"> <div class="d-inline-block"></div> </div>

@push('js')
    <script>
        $(document).ready(function() {
            $('#tableunitType').DataTable({
                // paginate:false
            });
        });
    </script>
@endpush
