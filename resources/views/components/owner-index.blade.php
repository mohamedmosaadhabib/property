@component('components.data-table',['tableID'=>"tableowner"])
    @slot('thead')
        <tr>
            <th> @lang('main.id') </th>
            <th>@lang('main.name')</th>
            <th>@lang('main.email')</th>
            <th>@lang('main.phone')</th>
            <th> @lang('main.modal') </th>
            <th> @lang('main.edit') </th>
            <th> @lang('main.delete') </th>
        </tr>
    @endslot
    @slot('tbody')
        @foreach ($owners as $owner)
            <tr>
                <td>{{$owner->id}}</td>
                <td>{{$owner->name}}</td>
                <td>{{$owner->email}}</td>
                <td>{{$owner->phone}}</td>
                <td>
                    <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#Model{{$loop->index}}">model</button>
                    @push('modals')
                        @component('modal',['id'=>'Model'.$loop->index,'title'=>ucfirst($owner->name)])
                            <h4>@lang('main.address_ar')</h4>
                            <p> {{$owner->getTranslation('address','ar')}} </p>
                            <hr>
                            <h4>@lang('main.address_en')</h4>
                            <p> {{$owner->getTranslation('address','en')}} </p>
                        @endcomponent
                    @endpush
                </td>
                <td>
                    <a href="{{route('dashboard.owner.edit',$owner)}}" class="btn btn-sm btn-warning">@lang('main.edit')</a>
                </td>
                <td>
                    <form name="ownerDelete{{$owner->id}}" action="{{route('dashboard.owner.destroy',$owner)}}" method="post">@csrf @method('DELETE')</form>
                    <button type="button" onclick="confirm(' @lang('main.owner_delete') ')?ownerDelete{{$owner->id}}.submit():''" class="btn btn-sm btn-danger">@lang('main.delete')</button>
                </td>
            </tr>
        @endforeach
    @endslot

@endcomponent

<div class="text-right"> <div class="d-inline-block"></div> </div>

@push('js')
    <script>
        $(document).ready(function() {
            $('#tableowner').DataTable({
                // paginate:false
            });
        });
    </script>
@endpush
