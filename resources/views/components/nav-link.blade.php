<li class="{{$hasMenu==true?' dropdown ':''}} {{$class ??''}} ">
    <a class="{{($href??'')==url()->current()?'active':''}}  {{$linkClass??''}} text-capitalize" href="{{$href??'#'}}">{{$linkName}}</a>
</li>
