@component('components.data-table',['tableID'=>"tableproject"])
    @slot('thead')
        <tr>
            <th> @lang('main.id') </th>
            <th> @lang('main.name') </th>
            <th> @lang('main.price') </th>
            <th> @lang('main.modal') </th>
            <th> @lang('main.edit') </th>
            <th> @lang('main.delete') </th>
        </tr>
    @endslot
    @slot('tbody')
        @foreach ($projects as $project)
            <tr>
                <td>{{$project->id}}</td>
                <td>{{$project->name}}</td>
                <td>{{$project->price}}</td>
                <td>
                    <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#Model{{$loop->index}}">@lang('main.model')</button>
                    @push('modals')
                        @component('modal',['id'=>'Model'.$loop->index,'title'=>ucfirst($project->name)])
                            <h4>@lang('main.description_ar')</h4>
                            <p> {{$project->getTranslation('description','ar')}} </p>
                            <hr>
                            <h4>@lang('main.description_en')</h4>
                            <p> {{$project->getTranslation('description','en')}} </p>
                        @endcomponent
                    @endpush
                </td>
                <td>
                    <a href="{{route('dashboard.project.edit',$project)}}" class="btn btn-sm btn-warning">@lang('main.edit')</a>
                </td>
                <td>
                    <form name="projectDelete{{$project->id}}" action="{{route('dashboard.project.destroy',$project)}}" method="post">@csrf @method('DELETE')</form>
                    <button type="button" onclick="confirm(' @lang('main.project_delete') ')?projectDelete{{$project->id}}.submit():''" class="btn btn-sm btn-danger">@lang('main.delete')</button>
                </td>
            </tr>
        @endforeach
    @endslot

@endcomponent

<div class="text-right"> <div class="d-inline-block"></div> </div>

@push('js')
    <script>
        $(document).ready(function() {
            $('#tableproject').DataTable({
                // paginate:false
            });
        });
    </script>
@endpush
