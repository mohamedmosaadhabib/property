@error($name ??'',$bag ?? '')
    @if (is_array($message))
        <ul>
            @foreach ($message as $m)
                <li class="text-danger">{{$m}}</li>
            @endforeach
        </ul>
    @else
        <small class="text-danger"> {{$message}} </small>
    @endif
@enderror
