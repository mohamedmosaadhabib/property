<section class="gray-bg">
    <div class="container ">

        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="sec-heading center">
                    <h2>@lang('main.find_by_loactions')</h2>
                    <p>@lang('main.find_by_loactions_desc')</p>
                </div>
            </div>
        </div>

        <div class="row">
            @foreach ($regions as $region)
                <div class="col-lg-{{$loop->first ? '8':'4'}} col-md-{{$loop->first ? '8':'4'}}">
                    <a href="{{route('location',$region)}}" class="img-wrap">
                        <div class="img-wrap-content visible"> <h4>{{$region->name}}</h4> </div>
                        <div class="img-wrap-background" style="background-image: url('{{$region->imagePath}}');"></div>
                    </a>
                </div>
            @endforeach
        </div>

    </div>
</section>
