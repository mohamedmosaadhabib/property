@component('components.data-table',['tableID'=>"tablecontact"])
    @slot('thead')
        <tr>
            <th> @lang('main.id') </th>
            <th>@lang('main.name')</th>
            <th>@lang('main.phone')</th>
            <th>@lang('main.email')</th>
            <th>@lang('main.message')</th>
            <th>@lang('main.go_to')</th>
            <th>@lang('main.status')</th>
            <th> @lang('main.delete') </th>
        </tr>
    @endslot
    @slot('tbody')
        @foreach ($contacts as $contact)
            <tr>
                <td>{{$contact->id}}</td>
                <td>{{$contact->name}}</td>
                <td>{{$contact->phone}}</td>
                <td>{{$contact->email}}</td>
                <td>{{$contact->message}}</td>
                <td>
                    @if ($contact->contactable && $contact->contactable->developer)
                        <a class="btn btn-sm btn-success" href="{{$contact->contactable->getUrl()}}">@lang('main.go_to')</a>
                    @else
                        <button disabled type="button" class="btn btn-sm btn-success disabled">@lang('main.go_to')</button>
                    @endif

                </td>
                <td>
                    <form name="toggleStatus{{$loop->index}}" action="{{route('dashboard.contact.toggleStatus',$contact)}}" method="post">@csrf</form>
                    <button onclick="confirm(' @lang('main.change_status') ')?toggleStatus{{$loop->index}}.submit():''" class="btn btn-sm  {{$contact->read_at?' btn-success ':'btn-danger'}} ">{{$contact->read_at?__('main.read_at'):__('main.not_read_at')}}</button>
                </td>
                <td>
                    <form name="contactDelete{{$contact->id}}" action="{{route('dashboard.contact.destroy',$contact)}}" method="post">@csrf @method('DELETE')</form>
                    <button type="button" onclick="confirm(' @lang('main.contact_delete') ')?contactDelete{{$contact->id}}.submit():''" class="btn btn-sm btn-danger">@lang('main.delete')</button>
                </td>
            </tr>
        @endforeach
    @endslot

@endcomponent

<div class="text-right"> <div class="d-inline-block">{{$contacts->links()}}</div> </div>

@push('js')
    <script>
        $(document).ready(function() {
            $('#tablecontact').DataTable({
                paginate:false
            });
        });
    </script>
@endpush
