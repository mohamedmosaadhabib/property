
<div class="property-listing property-2">

    <div class="listing-img-wrapper">
        <div class="list-img-slide">
            <div class="click">
                @foreach ($project->photos ??[] as $image)
                    <div><a href="{{showDeveloper($project->developer,$project)}}"><img src="{{url($image)}}" class="img-fluid mx-auto" alt="" /></a></div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="listing-detail-wrapper pb-0">
        <div class="listing-short-detail">
            <h4 class="listing-name"><a href="{{showDeveloper($project->developer,$project)}}">{{$project->name}}</a><i class="list-status ti-check"></i></h4>
        </div>
    </div>
    <div class="price-features-wrapper">

        <div class="listing-price-fx">
            @lang('main.start_price') : {{$project->price.' '.trans('main.'.$project->currency)}}
            {{--                                        <br>--}}
            {{--                                       @lang('main.down_payment') : {{$project->down_payment.' '.trans('main.'.$project->currency)}}--}}
            {{--                                        <br>--}}
            {{--                                       @lang('main.installments_years') : {{$project->installments_years}}--}}
        </div>
    </div>

</div>
