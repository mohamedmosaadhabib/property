{{--
@push('css')
    <link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endpush
@push('js')
    <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <script src="{{asset('js/demo/datatables-demo.js')}}"></script>
@endpush
--}}

<div class="table-responsive">
    <table class="table table-bordered" id="{{$tableID??'tableID'}}" width="100%" cellspacing="0">
        @isset($thead)
            <thead>
            {{$thead}}
            </thead>
        @endisset
        @isset($tfoot)
            <tfoot>
            {{$tfoot}}
            </tfoot>
        @endisset
        @isset($tbody)
            <tbody>
            {{$tbody}}
            </tbody>
        @endisset
    </table>
</div>
