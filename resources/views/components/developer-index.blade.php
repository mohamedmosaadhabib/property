@component('components.data-table',['tableID'=>"tabledeveloper"])
    @slot('thead')
        <tr>
            <th> @lang('main.id') </th>
            <th>@lang('main.name')</th>
            <th>@lang('main.image')</th>
            <th> @lang('main.edit') </th>
            <th> @lang('main.delete') </th>
        </tr>
    @endslot
    @slot('tbody')
        @foreach ($developers as $developer)
            <tr>
                <td>{{$developer->id}}</td>
                <td>{{$developer->name}}</td>
                <td><img src="{{$developer->imagePath}}" width="50" height="50" alt=""></td>

                <td>
                    <a href="{{route('dashboard.developer.edit',$developer)}}" class="btn btn-sm btn-warning">@lang('main.edit')</a>
                </td>
                <td>
                    <form name="developerDelete{{$developer->id}}" action="{{route('dashboard.developer.destroy',$developer)}}" method="post">@csrf @method('DELETE')</form>
                    <button type="button" onclick="confirm(' @lang('main.developer_delete') ')?developerDelete{{$developer->id}}.submit():''" class="btn btn-sm btn-danger">@lang('main.delete')</button>
                </td>
            </tr>
        @endforeach
    @endslot

@endcomponent

<div class="text-right"> <div class="d-inline-block"></div> </div>

@push('js')
    <script>
        $(document).ready(function() {
            $('#tabledeveloper').DataTable({
                // paginate:false
            });
        });
    </script>
@endpush
