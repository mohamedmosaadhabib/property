<div>
    @if ($owners->count()>0)
        <table class="table">
            <thead>
            <tr class="text-capitalize">
                <th>#</th>
                <th>@lang('main.name')</th>
                <th>@lang('main.email')</th>
                <th>@lang('main.phone')</th>
                <th>@lang('main.details')</th>
                <th>@lang('main.edit')</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($owners as $owner)
                <tr>
                    <td scope="row">{{$loop->index+1}}</td>
                    <td>{{$owner->name}}</td>
                    <td>{{$owner->email}}</td>
                    <td>{{$owner->phone}}</td>
                    <td>
                        <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#Model{{$loop->index}}">model</button>
                        @push('modals')
                            @component('modal',['id'=>'Model'.$loop->index,'title'=>ucfirst($owner->name)])
                                <h4>@lang('main.address_ar')</h4>
                                <p> {{$owner->getTranslation('address','ar')}} </p>
                                <hr>
                                <h4>@lang('main.address_en')</h4>
                                <p> {{$owner->getTranslation('address','en')}} </p>
                            @endcomponent
                        @endpush
                    </td>
                    <td>
                        <a href="{{route('dashboard.owner.edit',$owner)}}" class="btn btn-sm btn-success">@lang('main.edit')</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$owners->links()}}
    @else
        <div class="alert alert-info w-100 text-center" role="alert">
            <strong>@lang('main.no_data')</strong>
        </div>
    @endif

</div>
