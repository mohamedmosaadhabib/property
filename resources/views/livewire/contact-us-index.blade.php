<div>
    @if ($contacts->count()>0)
    <table class="table">
        <thead>
        <tr class="text-capitalize">
            <th>#</th>
            <th>@lang('main.name')</th>
            <th>@lang('main.email')</th>
            <th>@lang('main.subject')</th>
            <th>@lang('main.message')</th>
            <th>@lang('main.mark_as_read')</th>
            <th>@lang('main.type')</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($contacts as $contact)
            <tr>
                <td scope="row">{{$loop->index+1}}</td>
                <td>{{$contact->name}}</td>
                <td>{{$contact->email}}</td>
                <td>{{$contact->subject}}</td>
                <td>{{$contact->message}}</td>
                <td>
                    <button class="btn btn-sm btn-success">@lang('main.mark_as_read')</button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
        {{$contacts->links()}}
    @else
        <div class="alert alert-info w-100 text-center" role="alert">
            <strong>@lang('main.no_data')</strong>
        </div>
    @endif

</div>
