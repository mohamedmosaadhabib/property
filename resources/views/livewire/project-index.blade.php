<div>
    @if ($projects->count()>0)
        <table class="table">
            <thead>
            <tr class="text-capitalize">
                <th>#</th>
                <th>@lang('main.name')</th>
                <th>@lang('main.min_area')</th>
                <th>@lang('main.price')</th>
                <th>@lang('main.details')</th>
                <th>@lang('main.edit')</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($projects as $project)
                <tr>
                    <td scope="row">{{$loop->index+1}}</td>
                    <td>{{$project->name}}</td>
                    <td>{{$project->min_area}}</td>
                    <td>{{$project->price}}</td>
                    <td>

                    </td>
                    <td>
                        <a href="{{route('dashboard.project.edit',$project)}}" class="btn btn-sm btn-success">@lang('main.edit')</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$projects->links()}}
    @else
        <div class="alert alert-info w-100 text-center" role="alert">
            <strong>@lang('main.no_data')</strong>
        </div>
    @endif

</div>
