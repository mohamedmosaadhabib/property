<div>
    <form wire:submit.prevent="{{$edit?'updateProperty':'createProperty'}}" class="row w-100">
        <div class="w-100">
            @csrf
            @if ($errors->any())
                <ul class="col-md-12">
                    @foreach ($errors->all() as $e)
                        <li class="text-danger">{!! $e !!}</li>
                    @endforeach
                </ul>
            @endif
            @if (session('success'))
                <div class="col-md-12 alert text-center alert-success" role="alert">
                    <strong>{!! session('success') !!}</strong>
                </div>
            @endif
        </div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link {{($tap=='base' ? ' active ':'')}}" id="base-tab" data-toggle="tab" href="#base" wire:click="changeTap('base')" role="tab" aria-controls="base" aria-selected="{{($tap=='images' ? 'true':'false')}}">base</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{($tap=='images' ? ' active ':'')}}" id="images-tab" data-toggle="tab" href="#images" wire:click="changeTap('images')" role="tab" aria-controls="images" aria-selected="{{($tap=='images' ? 'true':'false')}}">images</a>
        </li>
    </ul>
    <div class="tab-content w-100" id="myTabContent">
        <div class="tab-pane fade {{($tap=='base' ? ' show active ':'')}}" id="base" role="tabpanel" aria-labelledby="base-tab">
            <div class="row">
            <div class="col-lg-6 col-md-6 my-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"> <i class="fa fa-user"></i></span>
                    </div>

                    <input minlength="3" type="text" class="form-control" wire:model.lazy="name.ar"  placeholder="@lang('main.name_ar')">
                    <div class="w-100">
                        <x-errors.validation-error name="name"/>
                        <x-errors.validation-error name="name.ar"/>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 my-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"> <i class="fa fa-user"></i></span>
                    </div>
                    <input minlength="3" type="text" class="form-control" wire:model.lazy="name.en"  placeholder="@lang('main.name_en')">
                    <div class="w-100">
                        <x-errors.validation-error name="name"/>
                        <x-errors.validation-error name="name.en"/>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 my-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button data-toggle="modal" data-target="#ownerCreate" type="button" class="input-group-text bg-primary text-white" id="basic-addon2"> <i class="fa fa-user-astronaut"></i></button>
                    </div>
                @push('modals')
                    <!-- owner Create Modal-->
                        @component('modal',['id'=>'ownerCreate','size'=>"modal-lg",'title'=>trans('main.owner_create')])
                            <livewire:owner-create/>
                        @endcomponent
                    @endpush
                    <select name="owner_id" wire:model="owner_id"  class="form-control" id="owner_id">
                        <option value="">@lang('main.select_owner')</option>
                        @foreach ($owners as $owner)
                            <option value="{{$owner->id}}">{{$owner->name}}({{$owner->id}})</option>
                        @endforeach
                    </select>
                    <div class="w-100">
                        <x-errors.validation-error name="owner_id"/>
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-md-6 my-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon2"> <i class="fa fa-home"></i></span>
                    </div>
                    <select name="property_type" wire:model="property_type"  class="form-control" id="property_type">
                        <option value="">@lang('main.select_property_type')</option>
                        @foreach ([ "apartment", "twin_house", "chalet", "cafe", "office", "i_villa", "penthouse", "studio", "town_house", "ground_floor", "duplex", "villa",] as $item)
                            <option value="{{$item}}">@lang('main.'.$item)</option>
                        @endforeach
                    </select>
                    <div class="w-100">
                        <x-errors.validation-error name="property_type"/>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 my-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon2"> <i class="fa fa-landmark"></i></span>
                    </div>
                    <select name="property_views" wire:model="property_views"  class="form-control" id="property_views">
                        <option value="">@lang('main.select_property_views')</option>
                        @foreach (["landscape", "pool", "sea", "golf", "lake", "open_view", "club", "garden", "nile", ] as $item)
                            <option value="{{$item}}">@lang('main.'.$item)</option>
                        @endforeach
                    </select>
                    <div class="w-100">
                        <x-errors.validation-error name="property_views"/>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 my-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon2"> <i class="fa fa-money-bill"></i></span>
                    </div>
                    <select name="currency" wire:model="currency"  class="form-control" id="currency">
                        <option value="">@lang('main.select_currency')</option>
                        <option value="egp">@lang('main.egp')</option>
                        <option value="usd">@lang('main.usd')</option>
                    </select>
                    <div class="w-100">
                        <x-errors.validation-error name="currency"/>
                    </div>
                </div>
            </div>


            <div class="col-md-12 row border p-3">
            @push('modals')
                <!-- region Create Modal-->
                    @component('modal',['id'=>'regionCreate','size'=>"modal-lg",'title'=>trans('main.region_create')])
                        <livewire:region-create/>
                    @endcomponent
                @endpush
                <h4 class="col-md-12">
                    @lang('main.location')
                    <button data-toggle="modal" data-target="#regionCreate" type="button" class="btn bg-primary text-white" id="basic-addon2"> <i class="fa fa-location-arrow"></i></button>
                </h4>

                <div class="col-lg-4 col-md-6 my-2">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon2"> <i class="fa fa-hockey-puck"></i></span>
                        </div>
                        <input minlength="3" type="number" class="form-control" wire:model.lazy="building_number"  placeholder="@lang('main.building_number')">
                        <div class="w-100">
                            <x-errors.validation-error name="building_number"/>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6 my-2">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon2"> <i class="fa fa-disease"></i></span>
                        </div>
                        <input minlength="3" type="number" class="form-control" wire:model.lazy="floor_number"  placeholder="@lang('main.floor_number')">
                        <div class="w-100">
                            <x-errors.validation-error name="floor_number"/>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6 my-2">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon2"> <i class="fa fa-dice-one"></i></span>
                        </div>
                        <input minlength="3" type="number" class="form-control" wire:model.lazy="apartment_number"  placeholder="@lang('main.apartment_number')">
                        <div class="w-100">
                            <x-errors.validation-error name="apartment_number"/>
                        </div>
                    </div>
                </div>

                <div class="form-group row col-md-12">

                    <div class="col-md-12 row p-0">

                        <div class="col-md-4">
                            <div class="input-group">
                                <select class="form-control w-100" name="location" wire:model="location.region" id="locationregion">
                                    <option value="" selected>@lang('main.select_region')</option>
                                    @foreach (\App\Models\Region::latest()->whereNull('region_id')->get() as $region)
                                        <option value="{{$region->id}}">{{$region->name}}</option>
                                    @endforeach
                                </select>
                                <div class="w-100">
                                    <x-errors.validation-error name="location.region"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control w-100 " @if (collect([])->merge($cities)->count() === 0) disabled @endif name="location" wire:model="location.city" id="locationcity">
                                <option value="" selected>@lang('main.select_city')</option>
                                @foreach ($cities as $region)
                                    <option value="{{$region->id}}">{{$region->name}}</option>
                                @endforeach
                            </select>
                            <x-errors.validation-error name="location.city"/>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control w-100 "  @if (collect([])->merge($streets)->count() === 0) disabled @endif  name="location" wire:model="location.street" id="locationstreet">
                                <option value="" selected>@lang('main.select_streets_and_compounds')</option>
                                @foreach ($streets as $region)
                                    <option value="{{$region->id}}">{{$region->name}}</option>
                                @endforeach
                            </select>
                            <x-errors.validation-error name="location.street"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <x-errors.validation-error name="location"/>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 my-2">
                    <label for="">@lang('main.address_en')</label>
                    <div class="input-group">
                        <textarea minlength="3" class="form-control" wire:model.lazy="address.en" name="address[en]" id="" rows="3"></textarea>
                        <div class="w-100">
                            <x-errors.validation-error name="address.en"/>
                        </div>
                    </div>
                </div>

                <hr>

            </div>

            <div class="col-lg-6 col-md-6 my-2">
                <label for="description_ar">@lang('main.description_ar')</label>
                <div class="input-group">
                    <textarea minlength="3" class="form-control" wire:model.lazy="description.ar" name="description[ar]" id="description_ar" rows="3"></textarea>
                    <div class="w-100">
                        <x-errors.validation-error name="description.ar"/>
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-md-6 my-2">
                <label for="description_en">@lang('main.description_en')</label>
                <div class="input-group">
                    <textarea minlength="3" class="form-control" wire:model.lazy="description.en" name="description[en]" id="description_en" rows="3"></textarea>
                    <div class="w-100">
                        <x-errors.validation-error name="description.en"/>
                    </div>
                </div>
            </div>

            <div class="row col-md-12 my-3">
                <h4 class="col-md-12">@lang('main.property_setting')</h4>
                <div class="col-lg-6 col-md-6 my-2">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon2"> <i class="fa fa-bed"></i></span>
                        </div>
                        <select name="bed_rooms" wire:model="bed_rooms"  class="form-control" id="bed_rooms">
                            <option value=""> @lang('main.bed_rooms') </option>
                            @for ($i = 1; $i <= 10; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                        <div class="w-100">
                            <x-errors.validation-error name="bed_rooms"/>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 my-2">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon2"> <i class="fa fa-bath"></i></span>
                        </div>
                        <select name="bathrooms" wire:model="bathrooms"  class="form-control" id="bathrooms">
                            <option value=""> @lang('main.bathrooms') </option>
                            @for ($i = 1; $i <= 10; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                        <div class="w-100">
                            <x-errors.validation-error name="bathrooms"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 my-2">

                    <label for="resale">@lang('main.resale')</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="resale[check]" class="custom-control-input" wire:model="resale.check"  id="resaleCheck1">
                                    <label class="custom-control-label" for="resaleCheck1"></label>
                                </div>
                            </div>
                        </div>
                        <input type="date"  name="resale[delivery_date]"   class="form-control" placeholder="{{trans('main.delivery_date')}}"  wire:model.lazy="resale.delivery_date">
                        <input type="number" min="0" step="any"  name="resale[value]" class="form-control" placeholder="{{trans('main.resale_price')}}"  wire:model.lazy="resale.value">
                        <input type="number" min="0" step="any"  name="resale[been_paid]" class="form-control" placeholder="{{trans('main.resale_been_paid')}}"  wire:model.lazy="resale.been_paid">
                        @error('resale')
                        <div class="w-100">
                            <x-errors.validation-error name="resale.delivery_date"/>
                            <x-errors.validation-error name="resale.value"/>
                            <x-errors.validation-error name="resale.been_paid"/>
                        </div>
                        @enderror
                    </div>
                </div>
                @foreach (['sale','rent_unfurnished','rent_furnished','rent_semi_furnished'] as $rent)
                    <div class="col-lg-6 col-md-6 my-2">

                        <label for="">@lang('main.'.$rent)</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="{{$rent}}[check]" class="custom-control-input" wire:model.lazy="{{$rent}}.check"  id="{{$rent}}Check1">
                                        <label class="custom-control-label" for="{{$rent}}Check1"></label>
                                    </div>
                                </div>
                            </div>
                            <input type="number" min="0" step="any"  name="{{$rent}}[value]" class="form-control" placeholder="{{trans('main.'.$rent.'_price')}}"  wire:model.lazy="{{$rent}}.value">
                            @error('{{$rent}}')
                            <div class="w-100">
                                <x-errors.validation-error name="{{$rent}}.value"/>
                                <x-errors.validation-error name="{{$rent}}.check"/>
                            </div>
                            @enderror
                        </div>
                    </div>
                @endforeach


                <div class="col-lg-12 col-md-12 my-2">
                    <label for="">@lang('main.additional_rooms') </label>
                    @if ($rooms)
                        @foreach ($rooms as $key =>  $room)
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <select  class="form-control" name="rooms[{{$key}}][name]" wire:model.lazy="rooms.{{$key}}.name">
                                            <option value="roof">@lang('main.roof')</option>
                                            <option value="balcony">@lang('main.balcony')</option>
                                            <option value="garden">@lang('main.garden')</option>
                                            <option value="toilet">@lang('main.toilet')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <select class="form-control" name="rooms[{{$key}}][value]" wire:model.lazy="rooms.{{$key}}.value">
                                            @for ($i = 1; $i <= 10; $i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" wire:click="deleteroom({{$key}})"  wire:loading.attr="disabled" wire:target="deleteroom" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash-alt"></i>
                                    </button>
                                    <div wire:loading wire:target="deleteroom" class="spinner-border border-danger" role="status">
                                        <span class="sr-only">@lang('main.loading')...</span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <x-errors.validation-error name="active"/>
                                </div>
                            </div>
                        @endforeach
                        <div wire:loading wire:target="addroom" class="spinner-border" role="status">
                            <span class="sr-only">@lang('main.loading')...</span>
                        </div>
                    @endif
                    <hr>

                    <div class="text-right">
                        <button  wire:loading.add.class="disabled" type="button" class="btn btn-sm btn-success" wire:click="addroom"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 my-2">
                    <label for="">@lang('main.tags') </label>
                    <input class="form-control d-inline-block w-25" type="text" wire:model.lazy="tag"/>
                    <button  wire:loading.add.class="disabled" type="button" class="btn btn-sm btn-success" wire:click="addtags"><i class="fa fa-plus"></i></button>
                    <x-errors.validation-error name="tag"/>

                    @if ($tags)
                        <div class="col-md-12 mx-2"><br></div>
                        @foreach ($tags as $key =>  $tag)

                            <span class="badge badge-pill badge-primary">
                                {{$tag}}
                                <button type="button" wire:click="deletetags({{$key}})"  wire:loading.attr="disabled" wire:target="deletetags" style="font-size: 10px;" class="btn  btn-sm">
                                    <i class="fa fa-times-circle"></i>
                                </button>
                            </span>

                        @endforeach
                        <div wire:loading wire:target="addtags" class="spinner-border" role="status">
                            <span class="sr-only">@lang('main.loading')...</span>
                        </div>
                    @endif
                </div>

                <div class="col-lg-6 col-md-6 my-2">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" wire:model.lazy="active"  id="activeCheck1">
                        <label class="custom-control-label" for="activeCheck1">@lang('main.active')</label>
                    </div>
                    <div class="w-100">
                        <x-errors.validation-error name="active"/>
                    </div>
                </div>
            </div>
            </div>



        </div>
        <div class="tab-pane fade {{($tap=='images' ? ' show active ':'')}}" id="images" role="tabpanel" aria-labelledby="images-tab">
            <div class="row">
                <div class="col-lg-12 col-md-12 my-2">
                    <div class="input-group">
                        <input type="file" accept="image/*" multiple class="form-control" wire:model.lazy="images">
                        <div class="w-100">
                            <div wire:loading wire:target="images" class="spinner-border" role="status">
                                <span class="sr-only">@lang('main.loading')...</span>
                            </div>
                            <x-errors.validation-error name="images"/>
                        </div>
                    </div>
                </div>
                @if ($photos || count($photos))
                    <h4 class="my-2">@lang('main.images')</h4>
                    <div class="row my-3">
                        @foreach ($photos as $key => $photo)
                            @php( $selected=$key== $image)
                            <div class="col-md-3" >
                                @if (count($photos)>1)
                                    <button type="button" wire:click="ImageDelete({{$key}});" class="btn btn-sm mb-1 btn-danger"><i class="fa fa-trash-alt"></i></button>
                                @endif
                                <button type="button" @if (!$selected) wire:click="primaryImage({{$key}})" @endif class="btn btn-sm mb-1 btn-primary {{$selected ? ' disabled ' :''}} "><i class="fa fa-image"></i></button>

                                <img class="w-100" @if ($selected) style="border: 10px solid #4e73df !important" @endif src="{{$photo instanceof Livewire\TemporaryUploadedFile ?$photo->temporaryUrl() :url($photo)}}">
                            </div>
                        @endforeach
                    </div>
                @endif
                @if ($images || (is_array($images) && count($images)))

                    <hr>
                    <h4 class="my-2">@lang('main.new_images')</h4>
                    <div class="row my-3">
                        @foreach ($images as $key => $photo)

                            @php( $selected=$key== $image)
                            <div class="col-md-3" >
                                @if (count($photos)>1)
                                    <button type="button" wire:click="NewImageDelete({{$key}})" data-dismiss="modal" aria-label="Close" class="btn btn-sm mb-1 btn-danger"><i class="fa fa-trash-alt"></i></button>
                                @endif
                                @if (!$edit)
                                        <button type="button" @if (!$selected) wire:click="primaryImage({{$key}})" @endif class="btn btn-sm mb-1 btn-primary {{$selected ? ' disabled ' :''}} "><i class="fa fa-image"></i></button>
                                @endif
                                <img style="border: 4px solid transparent" class="w-100 {{$selected ? 'border-primary' :''}}" src="{{$photo instanceof Livewire\TemporaryUploadedFile ?$photo->temporaryUrl() :url($photo)}}">
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>

        </div>
    </div>
        <div class="col-md-12">
            <button  wire:loading.add.class="disabled"  type="submit" class="btn btn-success">@lang('main.save')</button>
        </div>
    </form>
</div>
