<form wire:submit.prevent="{{$edit?'updateProject':'createProject'}}"  class="row">
    <div class="w-100">
        @csrf
        @if ($errors->any())
            <ul class="col-md-12">
                @foreach ($errors->all() as $e)
                    <li class="text-danger">{!! $e !!}</li>
                @endforeach
            </ul>
        @endif
        @if (session('success'))
            <div class="col-md-12 alert text-center alert-success" role="alert">
                <strong>{!! session('success') !!}</strong>
            </div>
        @endif
    </div>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link {{($tap=='base' ? 'active ':'')}}" id="base-tab" data-toggle="tab" href="#base" wire:click="changeTap('base')" role="tab" aria-controls="base" aria-selected="{{($tap=='images' ? 'true':'false')}}">base</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{($tap=='images' ? ' active ':'')}}" id="images-tab" data-toggle="tab" href="#images" wire:click="changeTap('images')" role="tab" aria-controls="images" aria-selected="{{($tap=='images' ? 'true':'false')}}">images</a>
        </li>
    </ul>

    <div class="tab-content w-100" id="myTabContent">
        <div class="tab-pane fade p-4 {{($tap=='base' ? ' show active ':'')}}" id="base" role="tabpanel" aria-labelledby="base-tab">
            <div class="row">
                <div class="form-group col-md-6">
                    <label class="col-md-12" for="name_ar">@lang('main.name_ar')</label>
                    <div class="col-md-6"> <input wire:loading.add.class="disabled" type="text" minlength="3" class="form-control" name="name[ar]" wire:model.lazy="name.ar" placeholder="@lang('main.name_ar')"/> </div>
                    <div class="col-md-2">
                        <x-errors.validation-error name="name.ar"/>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-12" for="name_en">@lang('main.name_en')</label>
                    <div class="col-md-6"> <input wire:loading.add.class="disabled" type="text" minlength="3" class="form-control" name="name[en]" wire:model.lazy="name.en" placeholder="@lang('main.name_en')"/> </div>
                    <div class="col-md-2">
                        <x-errors.validation-error name="name.en"/>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-12" for="price">@lang('main.price')</label>
                    <div class="col-md-6"> <input wire:loading.add.class="disabled" type="number" step="any"  class="form-control" name="price" wire:model.lazy="price" placeholder="@lang('main.price')"/> </div>
                    <div class="col-md-2">
                        <x-errors.validation-error name="price"/>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-12" for="down_payment">@lang('main.down_payment')</label>
                    <div class="col-md-6"> <input wire:loading.add.class="disabled" type="number" step="any"  class="form-control" name="down_payment" wire:model.lazy="down_payment" placeholder="@lang('main.down_payment')"/> </div>
                    <div class="col-md-2">
                        <x-errors.validation-error name="down_payment"/>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-12" for="installments_years">@lang('main.installments_years')</label>
                    <div class="col-md-6"> <input wire:loading.add.class="disabled" type="number" minlength="4" step="any"  class="form-control" name="installments_years" wire:model.lazy="installments_years" placeholder="@lang('main.installments_years')"/> </div>
                    <div class="col-md-2">
                        <x-errors.validation-error name="installments_years"/>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-12" for="image">@lang('main.delivery_date')</label>
                    <div class="col-md-6"> <input wire:loading.add.class="disabled" type="date" class="form-control" name="delivery_date" wire:model.lazy="delivery_date" placeholder="@lang('main.delivery_date')"/> </div>
                    <div class="col-md-2">
                        <x-errors.validation-error name="delivery_date"/>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-12" for="description_ar">@lang('main.description_ar')</label>
                    <div class="col-md-6"> <textarea type="text" class="form-control" name="description[ar]" wire:model.lazy="description.ar" placeholder="@lang('main.description_ar')"></textarea> </div>
                    <div class="col-md-2">
                        <x-errors.validation-error name="description.ar"/>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-12" for="description_en">@lang('main.description_en')</label>
                    <div class="col-md-6"> <textarea type="text" class="form-control" name="description[en]" wire:model.lazy="description.en" placeholder="@lang('main.description_en')"></textarea> </div>
                    <div class="col-md-2">
                        <x-errors.validation-error name="description.en"/>
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label class="col-md-12" for="developer_id">@lang('main.developers')</label>
                    <div class="col-md-6">
                        <select class="w-100" name="developer_id" wire:model="developer_id" id="developer_id">
                            <option value="" selected>@lang('main.selected')</option>
                            @foreach (\App\Models\Developer::all() as $developer)
                                <option value="{{$developer->id}}">{{$developer->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <x-errors.validation-error name="developer_id"/>
                    </div>
                </div>


                <div class="form-group col-md-6">
                    <label class="col-md-12" for="unit_types">@lang('main.currency')</label>
                    <div class="col-md-6">
                        <select class="w-100" name="currency" wire:model="currency" id="currency">
                            <option value="" selected>@lang('main.selected')</option>
                            <option value="egp" selected>@lang('main.egp')</option>
                            <option value="usd" selected>@lang('main.usd')</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <x-errors.validation-error name="currency"/>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label class="col-md-12" for="location">@lang('main.location')</label>
                    <div class="col-md-6 row p-0">
                        <div class="col-md-6 p-0">
                            <select class="w-100" name="location" wire:model="location.region" id="location">
                                <option value="" selected>@lang('main.selected')</option>
                                @foreach (\App\Models\Region::latest()->where('type',4)->get() as $region)
                                    <option value="{{$region->id}}" selected>{{$region->name}}</option>
                                @endforeach
                            </select>
                            <x-errors.validation-error name="location.region"/>
                        </div>
                        <div class="col-md-6">
                            <select class="w-100 " name="location" wire:model="location.city" id="location">
                                <option value="" selected>@lang('main.selected')</option>
                                @foreach (\App\Models\Region::latest()->where('type',2)->get() as $region)
                                    <option value="{{$region->id}}" selected>{{$region->name}}</option>
                                @endforeach
                            </select>
                            <x-errors.validation-error name="location.city"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <x-errors.validation-error name="location.*"/>
                    </div>
                </div>
                @if (count($types ??[]))
                    <table  class="col-md-12 mt-3 table">
                        <tr>
                            <th>@lang('main.unit_type')</th>
                            <th>@lang('main.min_area')</th>
                            <th>@lang('main.max_area')</th>
                            <th>@lang('main.price')</th>
                            <th>@lang('main.delete')</th>
                        </tr>
                        @foreach ($types??[] as $key => $type)
                            <tr>
                                <td>{{optional($unit_types->where('id',$type['unit_type'] ??0)->first())->name ?? __('main.unit_type')}}</td>
                                <td>{{$type['min'] ?? __('main.min')}}</td>
                                <td>{{$type['max'] ?? __('main.max')}}</td>
                                <td>{{$type['price'] ?? __('main.price')}}</td>
                                <td>
                                    <div class="col-md-1 text-right">
                                        <button type="button"  wire:loading.add.class="disabled" wire:click="deleteType({{$key}})" class="btn btn-sm btn-danger"> <i class="fa fa-minus" aria-hidden="true"></i></button>
                                    </div>
                                </td>
                            </tr>

                        @endforeach
                    </table>

                @endif

                <div class="row col-md-12 border-top pt-3">
                    <div class="col-md-3">
                        <select class="w-100" name="unit_type" wire:model="type.unit_type"  id="unit_type">
                            <option value="" selected>@lang('main.selected')</option>
                            @foreach ($unit_types as $unit)
                                <option value="{{$unit->id}}" selected>{{$unit->name}}</option>
                                @if ($unit->units()->count())
                                    <optgroup label="{{$unit->name}}">
                                        @foreach ($unit->units as $u)
                                            <option value="{{$u->id}}" selected>{{$u->name}}</option>
                                        @endforeach
                                    </optgroup>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <input wire:loading.add.class="disabled" class="w-100" type="number" pattern="[0-9]+" wire:model.lazy="type.min"  placeholder="@lang('main.min_area')" id="">
                    </div>
                    <div class="col-md-3">
                        <input wire:loading.add.class="disabled" class="w-100" type="number" pattern="[0-9]+" wire:model.lazy="type.max"  placeholder="@lang('main.max_area')" id="">
                    </div>
                    <div class="col-md-3">
                        <input wire:loading.add.class="disabled" class="w-100" type="number" pattern="[0-9]+" wire:model.lazy="type.price"  placeholder="@lang('main.price')" id="">
                    </div>
                </div>
                <div class="col-md-12 text-right">
                    <button type="button"  wire:loading.add.class="disabled" wire:click="addType" class="btn btn-sm btn-success"> <i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
        <div class="tab-pane fade p-4 {{($tap=='images' ? ' show active ':'')}}" id="images" role="tabpanel" aria-labelledby="images-tab">
            <div class="row">
                <div class="col-lg-12 col-md-12 my-2">
                    <div class="input-group">
                        <input wire:loading.add.class="disabled" type="file" accept="image/*" multiple class="form-control" wire:model.lazy="photos">
                        <div class="w-100">
                            <div wire:loading wire:target="photos" class="spinner-border" role="status">
                                <span class="sr-only">@lang('main.loading')...</span>
                            </div>
                            <x-errors.validation-error name="images"/>
                        </div>
                    </div>
                </div>
                @if ($images || (is_array($images) && count($images)))
                    <h4 class="my-2">@lang('main.images')</h4>
                    <div class="row my-3">
                        @foreach ($images as $key => $photo)
                            @php( $selected=$key== $image)
                            <div class="col-md-3" >
                                <button type="button" wire:click="ImageDelete({{$key}})" class="btn btn-sm mb-1 btn-danger"  data-dismiss="modal" aria-label="Close"><i class="fa fa-trash-alt"></i></button>
                                <button type="button" @if (!$selected) wire:click="primaryImage({{$key}})" @endif class="btn btn-sm mb-1 btn-primary {{$selected ? ' disabled ' :''}} "><i class="fa fa-image"></i></button>
                                @if (!is_array($photo))
                                    <img class="w-100" @if ($selected) style="border: 10px solid #4e73df !important" @endif  src="{{$photo instanceof Livewire\TemporaryUploadedFile ?$photo->temporaryUrl() :url($photo)}}">
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endif
                @if ($photos || (is_array($photos) && count($photos)))
                    <hr>
                    <h4 class="my-2">@lang('main.new_images')</h4>
                    <div class="row my-3">
                        @foreach ($photos as $key => $photo)
                            <div class="col-md-3" >
                                @if ((is_array($photos) && count($photos)>1) )
                                    <button type="button" wire:click="NewImageDelete({{$key}})" data-dismiss="modal" aria-label="Close" class="btn btn-sm mb-1 btn-danger"><i class="fa fa-trash-alt"></i></button>
                                @endif

                                <img class="w-100" src="{{$photo instanceof Livewire\TemporaryUploadedFile ?$photo->temporaryUrl() :url($photo)}}">
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>

        </div>

    </div>

    <div class="col-md-12 mt-5">
        <button  wire:loading.add.class="disabled"  type="submit"  class="btn btn-primary">@lang('main.save')</button>
    </div>
</form>
