<div>
    <form wire:submit.prevent="{{$edit ? 'editDeveloper': 'addDeveloper'}}" class="row w-100">@csrf
        @if ($errors->any())
            <ul class="col-md-12">
                @foreach ($errors->all() as $e)
                    <li class="text-danger">{{$e}}</li>
                @endforeach
            </ul>
        @endif
        @if (session('success'))
            <div class="col-md-12 alert text-center alert-success" role="alert">
                <strong>{{session('success')}}</strong>
            </div>
        @endif

        <div class="col-lg-6 col-md-6 my-2">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"> <i class="fa fa-user"></i></span>
                </div>

                <input minlength="3" type="text" class="form-control" wire:model.lazy="name.ar"  placeholder="@lang('main.name_ar')">
                <div class="w-100">
                <x-errors.validation-error name="name.ar"/>

                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 my-2">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"> <i class="fa fa-user"></i></span>
                </div>

                <input minlength="3" type="text" class="form-control" wire:model.lazy="name.en"  placeholder="@lang('main.name_en')">
                <div class="w-100">
                <x-errors.validation-error name="name.en"/>

                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 my-2">
            <div class="input-group">
                <input type="file" class="form-control" wire:model.lazy="image">
                <div class="w-100">
                <div wire:loading wire:target="image">@lang('main.uploading')...</div>
                    <x-errors.validation-error name="image"/>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 my-2">
            <label for="">@lang('main.description_ar')</label>
            <div class="input-group">
                <textarea minlength="3" class="form-control" wire:model.lazy="description.ar" id="" rows="3">{{old('description')}}</textarea>
                <div class="w-100">
                    <x-errors.validation-error name="description.ar"/>
                </div>
            </div>
        </div>


        <div class="col-lg-12 col-md-12 my-2">
            <label for="">@lang('main.description_en')</label>
            <div class="input-group">
                <textarea minlength="3" class="form-control" wire:model.lazy="description.en" id="" rows="3">{{old('description')}}</textarea>
                <div class="w-100">
                    <x-errors.validation-error name="description.en"/>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <button wire:loading.add.class="disabled"  type="submit" class="btn btn-success">@lang('main.save')</button>
        </div>

    </form>
</div>
