<form wire:submit.prevent="{{$edit?'updateUnitTypes':'createUnitTypes'}}" >@csrf
    @if (session('success'))
        <div class="alert text-center w-100 alert-success" role="alert">
            <strong>{{session('success')}}</strong>
        </div>
    @endif

    <div class="form-group row">
        <label for="name_ar" class="col-md-3">@lang('main.name_ar')</label>
        <div class="col-md-6"> <input type="text" class="form-control" name="name[ar]" wire:model.lazy="name.ar" placeholder="@lang('main.name_ar')"/> </div>
        <div class="col-md-3">
            <x-errors.validation-error name="name.ar"/>
        </div>
    </div>

    <div class="form-group row">
        <label for="name_en" class="col-md-3">@lang('main.name_en')</label>
        <div class="col-md-6"> <input type="text" class="form-control" name="name[en]" wire:model.lazy="name.en" placeholder="@lang('main.name_en')"/> </div>
        <div class="col-md-3">
            <x-errors.validation-error name="name.en"/>
        </div>
    </div>

    <div class="form-group row">
        <label for="name_en" class="col-md-3">@lang('main.unit_types')</label>
        <div class="col-md-6">
            <select class="w-100" wire:model.lazy="unit_types_id" name="unit_types_id" id="unit_types_id">
                <option value="">@lang('main.select')</option>
                @foreach (\App\Models\UnitTypes::latest()->get() as $unitTypesA)
                    <option value="{{$unitTypesA->id}}">{{$unitTypesA->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3">
            <x-errors.validation-error name="region_id"/>
        </div>
    </div>


    <button  wire:loading.add.class="disabled"  type="submit" class="btn btn-primary">@lang('main.save')</button>
</form>
