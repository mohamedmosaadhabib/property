<div>
    <!-- ============================ Hero Banner  Start================================== -->
    <div class="search-header-banner">
        <div class="container">
            <div class="full-search-2 transparent">
                <div class="hero-search">
                    <h1>@lang('main.search_your_unit')</h1>
                </div>
                <form method="GET" class="hero-search-content">

                    <div class="row">

                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <div class="input-with-icon">
                                    <input type="text" class="form-control" name="search" name="search" value="{{request('search')}}" placeholder="@lang('main.neighborhood')">
                                    <i class="ti-search"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <div class="input-with-icon">
                                    <select  class="form-control" name="region_id" name="region_id">
                                        <option value="">@lang('main.all_cities')</option>
                                        @foreach (\App\Models\Region::latest()->get() as $region)
                                            <option value="{{$region->id}}">{{$region->name}}</option>
                                        @endforeach
                                    </select>
                                    <i class="ti-briefcase"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-6">
                            <div class="form-group">
                                <div class="input-with-icon">
                                    <input type="number" step="any" class="form-control" name="minimum"  name="minimum" value="{{request('minimum')}}" placeholder="@lang('main.minimum')">
                                    <i class="ti-money"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-6">
                            <div class="form-group">
                                <div class="input-with-icon">
                                    <input type="number" step="any" class="form-control" name="maximum"  name="maximum" value="{{request('maximum')}}" placeholder="@lang('main.maximum')">
                                    <i class="ti-money"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <div class="input-with-icon">
                                    <select name="ptypes" class="form-control">
                                        <option value="">@lang('main.unit_type')</option>
                                        @foreach ($types as $type)
                                            <option value="{{$type}}">@lang('main.'.$type)</option>
                                        @endforeach
                                    </select>
                                    <i class="ti-briefcase"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <div class="input-with-icon">
                                    <select name="bedrooms" name="bedrooms" class="form-control">
                                        <option value="">@lang('main.bedrooms')</option>
                                        @for ($i = 1; $i <= 10; $i++)
                                            <option value="{{$i}}" {{request('bedrooms',null)==$i?' selected ':''}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                    <i class="fas fa-bed"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <div class="input-with-icon">
                                    <select name="bathrooms" name="bathrooms" class="form-control">
                                        <option value="">@lang('main.bathrooms')</option>
                                        @for ($i = 1; $i <= 10; $i++)
                                            <option value="{{$i}}" {{request('bathrooms',null)==$i?' selected ':''}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                    <i class="fas fa-bath"></i>
                                </div>
                            </div>
                        </div>

                    </div>



                    <div class="row">

                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn search-btn-outline">@lang('main.search_result')</button>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>
    <!-- ============================ Hero Banner End ================================== -->

    <!-- =========================== All Property =============================== -->
    <section>
        <div class="container">

            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h4>@lang('main.total_property_find_is'): <span class="theme-cl">{{$counter}}</span></h4>
                </div>
            </div>

            <div class="row">
            @foreach ($properties as $property)
                <!-- Single Property -->
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="property-listing property-2">

                            <div class="listing-img-wrapper">
                                <div class="list-img-slide">
                                    <div class="click">
                                        @foreach ($property->images ?? [] as $image)
                                            <div><a href="{{route('$property)}}"><img src="{{url($image)}}" class="img-fluid mx-auto" alt="" /></a></div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="listing-detail-wrapper pb-0">
                                <div class="listing-short-detail">
                                    <h4 class="listing-name"><a href="{{showProperty($property)}}">{{$property->name}}</a><i class="list-status ti-check"></i></h4>
                                </div>
                            </div>
                            <div class="price-features-wrapper">

                                <div class="listing-price-fx">
                                    <ul class="list-style-none">
                                         <li><h6 class="listing-card-info-price price-prefix" content="{{__('main.'.$property->currency)}}">{{(!empty($ptypes) && isset($property->{$ptypes}['value'])) ?(($property->{$ptypes}['value']??'').' '.$ptypes) :  $property->getLowPrice()}}</h6> </li>

                                    </ul>
                                </div>

{{--                                <div class="listing-price-fx">--}}
{{--                                    <ul>--}}
{{--                                        @if ( isset($property->resale['check']) && $property->resale['check'])--}}
{{--                                            <li> @lang('main.resale')<h6 class="listing-card-info-price price-prefix" content="{{__('main.'.$property->currency)}}">{{ $property->resale['value']?? '' }}<span class="price-suffix"></span></h6> </li>--}}
{{--                                        @endif--}}
{{--                                        @foreach (['sale','rent_unfurnished','rent_furnished','rent_semi_furnished'] as $key)--}}
{{--                                            @if ( isset($property->{$key}['check']) && $property->{$key}['check'])--}}
{{--                                                <li> @lang('main.'.$key)<h6 class="listing-card-info-price price-prefix" content="{{__('main.'.$property->currency)}}">{{ $property->{$key}['value']?? '' }}<span class="price-suffix">@if($key!='sale')/@lang('main.month') @endif</span></h6> </li>--}}
{{--                                            @endif--}}
{{--                                        @endforeach--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
                                <div class="list-fx-features">
                                    <div class="listing-card-info-icon">
                                        <span class="inc-fleat inc-bed">{{$property->bed_rooms}} @lang('main.bedrooms')</span>
                                    </div>
                                    <div class="listing-card-info-icon">
                                        <span class="inc-fleat inc-bath">{{$property->bathrooms}} @lang('main.bathrooms')</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- End Single Property -->
                @endforeach

            </div>

            <!-- Pagination -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    {{$properties->links()}}
                </div>
            </div>

        </div>
    </section>
    <!-- =========================== All Property =============================== -->

</div>
