<div>
    @if ($developers->count()>0)
        <table class="table">
            <thead>
            <tr class="text-capitalize">
                <th>#</th>
                <th>@lang('main.name')</th>
                <th>@lang('main.email')</th>
                <th>@lang('main.phone')</th>
                <th>@lang('main.details')</th>
                <th>@lang('main.edit')</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($developers as $developer)
                <tr>
                    <td scope="row">{{$loop->index+1}}</td>
                    <td>{{$developer->name}}</td>
                    <td>{{$developer->email}}</td>
                    <td>{{$developer->phone}}</td>
                    <td>
                        <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#Model{{$loop->index}}">model</button>
                        @push('modals')
                            @component('modal',['id'=>'Model'.$loop->index,'title'=>ucfirst($developer->name)])
                                <h4>@lang('main.description_ar')</h4>
                                <p> {{$developer->getTranslation('description','ar')}} </p>
                                <hr>
                                <h4>@lang('main.description_en')</h4>
                                <p> {{$developer->getTranslation('description','en')}} </p>
                                <hr>
                                <ul class="list-unstyled text-center" >
                                    @if ($developer->facebook)
                                        <li class="d-inline-block mx-2"><a class="facebook" href="{{$developer->facebook}}"><i class="fab fa-3x fa-facebook"></i></a></li>
                                    @endif
                                    @if ($developer->twitter)
                                        <li class="d-inline-block mx-2"><a class="twitter" href="{{$developer->twitter}}"><i class="fab fa-3x fa-twitter"></i></a></li>
                                    @endif
                                    @if ($developer->instagram)
                                        <li class="d-inline-block mx-2"><a class="instagram" href="{{$developer->instagram}}"><i class="fab fa-3x fa-instagram"></i></a></li>
                                    @endif
                                    @if ($developer->facebook)
                                        <li class="d-inline-block mx-2"><a class="linkedin" href="{{$developer->linkedin}}"><i class="fab fa-3x fa-linkedin"></i></a></li>
                                    @endif
                                </ul>
                            @endcomponent
                        @endpush
                    </td>
                    <td>
                        <a href="{{route('dashboard.developer.edit',$developer)}}" class="btn btn-sm btn-success">@lang('main.edit')</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$developers->links()}}
    @else
        <div class="alert alert-info w-100 text-center" role="alert">
            <strong>@lang('main.no_data')</strong>
        </div>
    @endif

</div>
