<form wire:submit.prevent="{{$edit?'updateRegion':'createRegion'}}" enctype="multipart/form-data">
    @csrf
    @if (session('success'))
        <div class="alert text-center w-100 alert-success" role="alert">
            <strong>{{session('success')}}</strong>
        </div>
    @endif
    @if ($errors->any())
        <ul>
            @foreach ($errors->all() as $error)
                <li class="list-group-item-danger">{{$error}}</li>
            @endforeach
        </ul>
    @endif
    <div class="form-group row">
        <label for="name_ar" class="col-md-3">@lang('main.name_ar')</label>
        <div class="col-md-6"> <input type="text" class="form-control" name="name[ar]" wire:model.lazy="name.ar" placeholder="@lang('main.name_ar')"/> </div>
        <div class="col-md-3">
            <x-errors.validation-error name="name.ar"/>
        </div>
    </div>

    <div class="form-group row">
        <label for="name_en" class="col-md-3">@lang('main.name_en')</label>
        <div class="col-md-6"> <input type="text" class="form-control" name="name[en]" wire:model.lazy="name.en" placeholder="@lang('main.name_en')"/> </div>
        <div class="col-md-3">
            <x-errors.validation-error name="name.en"/>
        </div>
    </div>

    <div class="form-group row">
        <label for="type" class="col-md-3">@lang('main.type')</label>
        <div class="col-md-6">
            <select class="w-100" wire:model.lazy="type" name="type" id="type">
                <!--<option value="1">@lang('main.region')</option>-->
                <option value="2">@lang('main.city')</option>
                <option value="3">@lang('main.streets_and_compounds')</option>
                <option value="4">@lang('main.governorate')</option>
            </select>
        </div>
        <div class="col-md-3">
            <x-errors.validation-error name="type"/>
        </div>
    </div>

    <div class="form-group row">
        <label for="region_id" class="col-md-3">@lang('main.region')</label>
        <div class="col-md-6">
            <select class="w-100" wire:model.lazy="region_id" name="region_id" id="region_id">
                <option value="">@lang('main.select')</option>
                @foreach(\App\Models\Region::all() as $region)
                    <option value="{{$region->id}}">{{$region->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3">
            <x-errors.validation-error name="region_id"/>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-3">
            <button  class="btn btn-primary btn-sm" {{!isset($region->imagePath) && !$image?' disabled ':''}}  data-toggle="modal" data-target="#ModelImage" type="button"  wire:loading.add.class="disabled"    >@lang('main.image')</button>
        </div>
        @component('modal',['id'=>'ModelImage','title'=>__('main.image')])
            @if ( isset($region->imagePath) || $image)
                <h4 class="my-2">@lang('main.image')</h4>
                <div class="col-md-12">
                    <img src="{!! $image instanceof Livewire\TemporaryUploadedFile? $image->temporaryUrl():  $region->imagePath  ?? url($image)!!}" id="preview" class="w-100" alt="">
                </div>
            @endif
        @endcomponent

        <div class="col-md-6">
            <div class="input-group">
                <input type="file" accept="image/*"  class="form-control" wire:model.lazy="image">
                <div class="w-100">
                    <div wire:loading wire:target="image" class="spinner-border" role="status">
                        <span class="sr-only">@lang('main.loading')...</span>
                    </div>
                    <x-errors.validation-error name="image"/>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <x-errors.validation-error name="image"/>
        </div>
    </div>

    <div class="form-group row">
        <div class="custom-control col-md-9 custom-checkbox">
            <input type="checkbox" class="custom-control-input" wire:model="show"  id="showCheck1">
            <label class="custom-control-label" for="showCheck1">@lang('main.show')</label>
        </div>
        <div class="col-md-3">
            <x-errors.validation-error name="show"/>
        </div>
    </div>

    <button type="submit"  wire:loading.add.class="disabled"  class="btn btn-primary">@lang('main.save')</button>
</form>
