<form wire:submit.prevent="goUrl">
    <div class="row">

        <div class="col-lg-2 col-md-6 col-sm-12 small-padd">
            <div class="form-group">
                <div class="input-with-icon">
                    <input type="text" class="form-control" name="search" wire:model.lazy="search" placeholder="@lang('main.search')">
                    <i class="ti-search"></i>
                </div>
            </div>
        </div>


        <div class="col-lg-2 col-md-6 col-sm-12 small-padd">
            <div class="form-group">
                <div class="input-with-icon">
                    <select name="select" class="form-control" wire:model.lazy="select">
                        <option value="property">@lang('main.property')</option>
                        <option value="project">@lang('main.project')</option>
                    </select>
                    <i class="ti-briefcase"></i>
                </div>
            </div>
        </div>

        @if ($select==="property")
            <div class="col-lg-2 col-md-6 col-sm-12 small-padd">
                <div class="form-group">
                    <div class="input-with-icon">
                        <select wire:model.lazy="ptypes" class="form-control">
                            <option value="">@lang('main.unit_type')</option>
                            @foreach (\App\Models\Property::typesStatic() as $type)
                                <option value="{{$type}}">@lang('main.'.$type)</option>
                            @endforeach
                        </select>
                        <i class="ti-briefcase"></i>
                    </div>
                </div>
            </div>
        @endif

        @if ($select==="project")
            <div class="col-lg-2 col-md-6 col-sm-12 small-padd">
                <div class="form-group">
                    <div class="input-with-icon">
                        <select wire:model.lazy="ptypes" name="ptypes" class="form-control">
                            <option value="">&nbsp;</option>
                            @foreach (\App\Models\UnitTypes::latest()->get() as $UnitTypes)
                                <option value="{{$UnitTypes->id}}">{{$UnitTypes->name}}</option>
                            @endforeach
                        </select>
                        <i class="ti-briefcase"></i>
                    </div>
                </div>
            </div>
        @endif

        <div class="col-lg-2 col-md-6 col-sm-12 small-padd">
            <div class="form-group">
                <div class="input-with-icon">
                    <select name="city" wire:model.lazy="city" class="form-control">
                        <option value="">@lang('main.all_cities')</option>
                        @foreach (\App\Models\Region::latest()->get() as $region)
                            <option value="{{$region->id}}">{{$region->name}}</option>
                        @endforeach
                    </select>
                    <i class="ti-briefcase"></i>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-6 col-sm-6 small-padd">
            <div class="form-group">
                <div class="input-with-icon">
                    <input type="number" step="any" wire:model.lazy="maximum" class="form-control" name="maximum" placeholder="@lang('main.maximum')">
                    <i class="ti-money"></i>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-6 col-sm-6 small-padd">
            <div class="form-group">
                <div class="input-with-icon">
                    <input type="number" step="any" wire:model.lazy="minimum" class="form-control" name="minimum" placeholder="@lang('main.minimum')">
                    <i class="ti-money"></i>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 small-padd ">
            <div class="form-group my-auto">
                <div class="form-group">
                    <button type="submit" class="btn reset-btn">@lang('main.search_result')</button>
                </div>
            </div>
        </div>

    </div>
</form>
