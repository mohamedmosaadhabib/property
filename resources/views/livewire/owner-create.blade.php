<form wire:submit.prevent="{{$edit?'updateOwner':'createOwner'}}" >
    @csrf
    @if ($errors->any())
        <ul class="col-md-12">
            @foreach ($errors->all() as $e)
                <li class="text-danger">{{$e}}</li>
            @endforeach
        </ul>
    @endif
    @if (session('success'))
        <div class="col-md-12 alert text-center alert-success" role="alert">
            <strong>{{session('success')}}</strong>
        </div>
    @endif

    <div class="form-group row">
        <label for="name_ar" class="col-md-3">@lang('main.name_ar')</label>
        <div class="col-md-6"> <input type="text" class="form-control" name="name[ar]" wire:model.lazy="name.ar" placeholder="@lang('main.name_ar')"/> </div>
        <div class="col-md-3">
            <x-errors.validation-error name="name.ar"/>
        </div>
    </div>

    <div class="form-group row">
        <label for="name_en" class="col-md-3">@lang('main.name_en')</label>
        <div class="col-md-6"> <input type="text" class="form-control" name="name[en]" wire:model.lazy="name.en" placeholder="@lang('main.name_en')"/> </div>
        <div class="col-md-3">
            <x-errors.validation-error name="name.en"/>
        </div>
    </div>
    <div class="form-group row">
        <label for="phone" class="col-md-3">@lang('main.phone')</label>
        <div class="col-md-6"> <input type="tel" class="form-control" name="phone" wire:model.lazy="phone" placeholder="@lang('main.phone')"/> </div>
        <div class="col-md-3">
            <x-errors.validation-error name="phone"/>
        </div>
    </div>

    <div class="form-group row">
        <label for="email" class="col-md-3">@lang('main.email')</label>
        <div class="col-md-6"> <input type="email" class="form-control" name="email" wire:model.lazy="email" placeholder="@lang('main.email')"/> </div>
        <div class="col-md-3">
            <x-errors.validation-error name="email"/>
        </div>
    </div>


    <button  wire:loading.add.class="disabled"  type="submit" class="btn btn-primary">@lang('main.save')</button>
</form>
