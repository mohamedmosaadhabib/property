<div class="container">
<div class="row mb-3">

    <div class="col-lg-12 col-md-12">
        <div class="form-group">
            <div class="input-with-icon">
                <input type="text" class="form-control" wire:model.lazy="search" placeholder="@lang('main.developer_search')">
                <i class="ti-search"></i>
            </div>
        </div>
    </div>
{{--    <div class="col-lg-2 col-md-3">--}}
{{--        <button type="button" class="w-100 btn search-btn">@lang('main.developer_find')</button>--}}
{{--    </div>--}}

</div>
<!-- /row -->

<div class="row">
    @if ($developers->count())
        @foreach ($developers as $developer)
            <div class="col-md-3 p-2">
                <div class="card border-0 ">
                    <img src="{{$developer->imagePath}}" alt="" class="" height="200" />
                    <div class="card-footer bg-transparent border-0 text-center">
                        <h4 class="card-title"><a href="{{route('developer.show',$developer)}}">{{$developer->name}} </a></h4>

                    </div>
                </div>
            </div>

        @endforeach
    @else
        <div class="col-md-12 p-3 text-center alert alert-info" role="alert">
            <strong> @lang('main.no_data') </strong>
        </div>
    @endif
</div>


<!-- Pagination -->
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        {{$developers->links('vendor.pagination.web')}}
    </div>

</div>
</div>
