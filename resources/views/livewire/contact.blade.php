<form wire:submit.prevent="addContact" class="col-lg-7 col-md-7">
    @if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        <strong>{{session('success')}}</strong>
    </div>
    @endif
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label>@lang('main.name')</label>
                <input type="text" wire:model.lazy="name" class="form-control simple">
                <x-errors.validation-error name="name"/>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label>@lang('main.email')</label>
                <input type="email" wire:model.lazy="email" class="form-control simple">
                <x-errors.validation-error name="email"/>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label>@lang('main.subject')</label>
        <input type="text"  wire:model.lazy="subject" class="form-control simple">
        <x-errors.validation-error name="subject"/>
    </div>

    <div class="form-group">
        <label>@lang('main.message')</label>
        <textarea  wire:model.lazy="message" class="form-control simple"></textarea>
        <x-errors.validation-error name="message"/>
    </div>

    <div class="form-group">
        <button class="btn btn-theme" type="submit">@lang('main.submit_contact')</button>
    </div>

</form>
