<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse{{$id=$id??uniqid('',false) }}" aria-expanded="true" aria-controls="collapse{{$id}}">
        @isset($icon)
            {{$icon}}
        @endisset
        <span>{{$title}}</span>
    </a>
    <div id="collapse{{$id}}" class="collapse" aria-labelledby="heading{{$id}}" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            {{$slot}}
        </div>
    </div>
</li>
