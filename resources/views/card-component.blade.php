<div class="card shadow my-4 border-0">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        @isset($title)
            <h6 class="m-0 font-weight-bold text-primary"> {{$title}} </h6>
        @endisset
        <div class="dropdown no-arrow">
            @isset($menu)
                <a class="dropdown-toggle" href="#" role="button" id="dropdown{{$dropdown=uniqid('',false)}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdown{{$dropdown}}"> {{$menu}} </div>
            @endisset
        </div>
    </div>

    <div class="card-body"> {{ $slot }} </div>
</div>
