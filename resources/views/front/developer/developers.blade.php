@extends('layouts.layout')
@section('content')

        <!-- ============================ Page Title Start================================== -->

        @php($home="<a href='".url('')."'>".__('main.home_page')."</a>".config('app.mark','/'))
        <x-layouts.title :title="$home.trans('main.developers')" :subtitle="__('main.developers_subtitle')"/>
        <!-- ============================ Page Title End ================================== -->

        <!-- ============================ Agency List Start ================================== -->
        <section>

            <livewire:developers/>

        </section>
        <!-- ============================ Agency List End ================================== -->
@stop
