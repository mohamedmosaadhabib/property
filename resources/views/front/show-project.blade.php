@extends('layouts.layout')
@push('css')
    <style>
        .breadcrumb-item+.breadcrumb-item::before{
            content: '\\' !important;

        }
        .slick-slide img {
                display: block;
                width: 100%;
                height: 500px;
            }
    </style>
@endpush
@section('content')
        <!-- ============================ Hero Banner  Start================================== -->
        <div class="featured-slick">
            <div class="featured-slick-slide">
                @foreach ($project->photos ?? [] as $image)
                    <div><a href="{{url($image)}}" class="mfp-gallery"><img  src="{{url($image)}}" alt="" /></a></div>
                @endforeach
            </div>
        </div>

        <section class="spd-wrap">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12 col-md-12">

                        <div class="slide-property-detail">

                            <div class="slide-property-first">
                                <div class="pr-price-into">
                                    <h2>{{number_format($project->start_price)}} / {{strtoupper(__('main.'.$project->currency))}}</h2>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- ============================ Hero Banner End ================================== -->

        <!-- ============================ Project Detail Start ================================== -->
        <section class="gray">
            <div class="container">
                <div class="row">

                    <nav class="col-sm-12 text-capitalize" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('main.home_page')</a></li>
                            <li class="breadcrumb-item"><a href="{{route('developer.show',$project->developer)}}">{{$project->developer->name}}</a></li>
                            <li class="breadcrumb-item"><a href="#">{{$project->name}}</a></li>
                        </ol>
                    </nav>
                    <!-- property main detail -->
                    <div class="col-lg-8 col-md-12 col-sm-12">

                        <!-- Single Block Wrap -->
                        <div class="block-wrap">

                            <div class="block-header">
                                <h4 class="block-title">@lang('main.project_info')</h4>
                            </div>

                            <div class="block-body">
                                <ul class="dw-proprty-info">
                                    <li><strong>@lang('main.installments_years')</strong>{{$project->installments_years}} @lang('main.years')</li>
                                    <li><strong>@lang('main.down_payment')</strong>{{$project->down_payment}} %</li>
                                    <li><strong>@lang('main.delivery_date')</strong>{{\Carbon\Carbon::parse($project->delivery_date)->format('Y')}}</li>
                                    <li><strong>@lang('main.start_price')</strong>{{number_format($project->start_price)}} / {{strtoupper(__('main.'.$project->currency))}}</li>
{{--                                    <li><strong>City</strong>{{$property->region->name}}</li>--}}
                                </ul>
                            </div>

                        </div>

                        <!-- Single Block Wrap -->
                        <div class="block-wrap">

                            <div class="block-header">
                                <h4 class="block-title">@lang('main.description')</h4>
                            </div>

                            <div class="block-body">
                                <p>{!! $project->description !!}</p>
                            </div>

                        </div>

                        @if($types->count())
                        <!-- Single Block Wrap -->
                        <div class="block-wrap">
{{--                            @dd($parents,$types)--}}
                            <div class="block-body">
                                <div class="accordion" id="floor-option">
                                        <div class="card">
                                                @if ($parents->count())
                                                    @foreach ($parents ?? [] as $parent)

                                                            <div class="card-header" id="firstFloor{{$loop->index}}">
                                                                <h2 class="mb-0">
                                                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#firstfloor{{$loop->index}}">{{$parent->name ??' '}}</button>
                                                                </h2>
                                                            </div>
                                                            <div id="firstfloor{{$loop->index}}" class="collapse" aria-labelledby="firstFloor{{$loop->index}}" data-parent="#floor-option">
                                                                        <div class="card-body">
                                                                            @foreach ($types as $type)
                                                                                @if (isset($type->parent) && $type->parent->id==$parent->id)
                                                                            <div class="accordion" id="second">
                                                                                <div class="card">
                                                                                    <div class="card-header" id="secondFloor{{$loop->index}}">
                                                                                        <h2 class="mb-0">
                                                                                            <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#secondfloor{{$loop->index}}">{{$type->name ??' '}}</button>
                                                                                        </h2>
                                                                                    </div>
                                                                                    <div id="secondfloor{{$loop->index}}" class="collapse" aria-labelledby="secondFloor{{$loop->index}}" data-parent="#secod">
                                                                                        <div class="card-body">
                                                                                            <ul>
                                                                                                @php
                                                                                                    /** @var $project */
                                                                                                    /** @var $type */
                                                                                                    $data=$project->types->firstWhere('unit_type',$type->id)
                                                                                                @endphp
                                                                                                @if (isset($data['min']) && $data['min'])
                                                                                                    <li>@lang('main.min_area') : {{$data['min']??''}}</li>
                                                                                                @endif
                                                                                                @if (isset($data['max']) && $data['max'])
                                                                                                    <li>@lang('main.max_area') : {{$data['max']??''}}</li>
                                                                                                @endif
                                                                                                @if (isset($data['price']))
                                                                                                    <li> @lang('main.start_price') : {{number_format($data['price'])}}</li>
                                                                                                @endif
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                                @endif
                                                                            @endforeach

                                                                        </div>
                                                                    </div>

                                                    @endforeach
                                                @else
                                                <div class="accordion" id="second">
                                                    @foreach ($types as $type)
                                                    <div class="card">
                                                        <div class="card-header" id="secondFloor{{$loop->index}}">
                                                            <h2 class="mb-0">
                                                                <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#secondfloor{{$loop->index}}">{{$type->name ??' '}}</button>
                                                            </h2>
                                                        </div>
                                                        <div id="secondfloor{{$loop->index}}" class="collapse" aria-labelledby="secondFloor{{$loop->index}}"nddata-parent="#secod">
                                                            <div class="card-body">
                                                                <ul>
                                                                    @php
                                                                        /** @var $project */
                                                                        /** @var $type */
                                                                           $data=$project->types->firstWhere('unit_type',$type->id)
                                                                    @endphp
                                                                    @if (isset($data['min']) && $data['min'])
                                                                        <li>@lang('main.min_area') : {{$data['min']}}</li>
                                                                    @endif
                                                                    @if (isset($data['max']) && $data['max'])
                                                                        <li>@lang('main.max_area') : {{$data['max']}}</li>
                                                                    @endif
                                                                    <li> @lang('main.start_price') : {{$data['price']}}</li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    @endforeach
                                                </div>
                                                @endif

                                        </div>
                                    </div>

                                </div>
                            </div>
                            @endif

                        <!-- Single Block Wrap -->
                        <div class="block-wrap">

                            <div class="block-header">
                                <h4 class="block-title">@lang('main.location')</h4>
                            </div>
                            <div class="block-body">
{{--                                <div class="map-container">--}}
{{--                                    <div id="singleMap" data-latitude="40.7427837" data-longitude="-73.11445617675781" data-mapTitle="Our Location"></div>--}}
{{--                                </div>--}}
                            @foreach ($project->location ??[] as $key =>  $location)
                                 @lang('main.'.$key) : {{\App\Models\Region::find($location)->name}} @if (!$loop->last) , @endif
                            @endforeach
                            </div>

                        </div>



                    </div>

                    <!-- property Sidebar -->
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="page-sidebar">

                            <!-- Agent Detail -->
                            <form action="{{route('contact_project_post',$project)}}" class="agent-widget" method="POST"> @csrf
                                <div class="agent-title">
                                    <div class="agent-photo"><img src="{{url($project->developer->image ?? 'https://via.placeholder.com/400x400')}}" alt=""></div>
                                    <div class="agent-details" style="top: auto !important;transform:none;margin: 0;display: inline-block;padding-{{app()->getLocale()=='ar'?'right':'left'}}: 20px;">
                                        <h4>
                                            <a href="{{route('developer.show',$project->developer)}}">{{$project->developer->name}}</a>
                                        </h4>
                                        <span><i class="lni-phone-handset"></i><a href="tel:{{setting('phone')}}" >{{setting('phone')}}</a></span>
                                        <span class="d-md-none d-sm-block"><i class="lni-whatsapp"></i><a href="https://api.whatsapp.com/send?phone={{setting('whatsapp')}}&text={{urlencode(url()->current().' '.$project->description)}}" >{{setting('whatsapp')}}</a></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <input type="text" name="name" maxlength="50" required  class="form-control" placeholder="@lang('main.your_name')"/>
                                    <x-errors.validation-error name="name"/>
                                </div>

                                <div class="form-group">
                                    <input type="email" name="email" maxlength="50" required  class="form-control" placeholder="@lang('main.your_email')"/>
                                    <x-errors.validation-error name="email"/>
                                </div>

                                <div class="form-group">
                                    <input type="tel" name="phone" minlength="11" maxlength="11" required class="form-control" placeholder="@lang('main.your_phone')"/>
                                    <x-errors.validation-error name="phone"/>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" minlength="5" required name="message" placeholder="{{__('main.project_contact_us_placeholder')}}"></textarea>
                                    <x-errors.validation-error name="message"/>

                                </div>
                                <button class="btn btn-theme full-width">@lang('main.send_message')</button>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- ============================ Project Detail End ================================== -->
@stop

@push('js')
<script type="text/javascript">
        $('.featured-slick-slide').slick({
        rtl:window.isRtl,
		centerMode: true,
		centerPadding: '80px',
		slidesToShow:1,
		responsive: [
		{
		breakpoint: 768,
		settings: {
		arrows:true,
		centerMode: true,
		centerPadding: '60px',
		slidesToShow:1
		}
		},
		{
		breakpoint: 480,
		settings: {
		arrows: false,
		centerMode: true,
		centerPadding: '40px',
		slidesToShow: 1
		}
		}
		]
	});


    </script>
@endpush
