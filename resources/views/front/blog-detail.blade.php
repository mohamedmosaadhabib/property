@extends('layouts.layout')
@section('content')
<div class="clearfix"></div>
			<!-- ============================================================== -->
			<!-- Top header  -->
			<!-- ============================================================== -->

			<!-- ============================ Page Title Start================================== -->
            @php($home="<a href='".url('')."'>".__('main.home_page')."</a>".config('app.mark','/'))
            @php($blogs="<a href='".route('blogs')."'>".__('main.blogs')."</a>".config('app.mark','/'))
            <x-layouts.title :title="$home.$blogs.$blog->name" :subtitle="__('main.blogs_sub_title')"/>
			<!-- ============================ Page Title End ================================== -->

			<!-- ============================ Agency List Start ================================== -->
			<section>

				<div class="container">

					<!-- row Start -->
					<div class="row">

						<!-- Blog Detail -->
						<div class="col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="blog-details single-post-item format-standard">
								<div class="post-details  border-0 shadow-sm bg-white">

									<div class="post-featured-img">
										<img class="img-fluid" src="{{$blog->imagePath??'https://via.placeholder.com/1300x850'}}" alt="">
									</div>

{{--									<h2 class="post-title">{{$blog->name}}</h2>--}}
									<p>{!! $blog->description !!}</p>

								</div>
							</div>

						</div>

					</div>
					<!-- /row -->

				</div>

			</section>
			<!-- ============================ Agency List End ================================== -->

@stop


