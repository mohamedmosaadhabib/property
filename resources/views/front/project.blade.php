@extends('layouts.layout')
@section('content')
			<!-- ============================ Page Title Start================================== -->
{{--			<div class="page-title">--}}
{{--				<div class="container">--}}
{{--					<div class="row">--}}
{{--						<div class="col-lg-12 col-md-12">--}}

{{--							<h2 class="ipt-title">@lang('main.all_projects')</h2>--}}
{{--							<span class="ipn-subtitle">@lang('main.projects_list_subtitle')</span>--}}

{{--						</div>--}}
{{--					</div>--}}
{{--				</div>--}}
{{--			</div>--}}
            @php($home="<a href='".url('')."'>".__('main.home_page')."</a>".config('app.mark','/'))
            @php($Projects=isset($location) ? "<a href='".route('project')."'>".__('main.projects')."</a>".config('app.mark','/').$location->name : __('main.projects'))
            <x-layouts.title :title="$home.$Projects"/>
			<!-- ============================ Page Title End ================================== -->

			<!-- ============================ Project List Start ================================== -->
			<section>

				<div class="container">

					<!-- row Start -->
					<form action="{{url()->current()}}" method="GET">
                        <div class="row">

                            <div class="col-lg-2 col-md-3">
                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <input type="text" name="search" class="form-control" value="{{isset($location)?$location->name:request('search')}}" placeholder="@lang('main.search_projects')">
                                        <i class="ti-search"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-6 col-sm-12 small-padd">
                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <select value="{{request('unit_type_id')}}" name="unit_type_id" class="form-control">
                                            <option value="">@lang('main.unit_type')</option>
                                            @foreach (\App\Models\UnitTypes::latest()->get() as $UnitTypes)
                                                <option value="{{$UnitTypes->id}}" {{request('unit_type_id')==$UnitTypes->id?' selected ':''}}>{{$UnitTypes->name}}</option>
                                            @endforeach
                                        </select>
                                        <i class="ti-briefcase"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-6 col-sm-12 small-padd">
                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <select name="city" value="{{request('city')}}" class="form-control">
                                            <option value="">@lang('main.all_cities')</option>
                                            @foreach (\App\Models\Region::latest()->get() as $region)
                                                <option value="{{$region->id}}" {{request('region_id')==$region->id?' selected ':''}}>{{$region->name}}</option>
                                            @endforeach
                                        </select>
                                        <i class="ti-briefcase"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-6 col-sm-6 small-padd">
                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <input type="number" step="any" value="{{request('maximum')}}" class="form-control" name="maximum" placeholder="@lang('main.maximum')">
                                        <i class="ti-money"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-6 col-sm-6 small-padd">
                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <input type="number" step="any" value="{{request('minimum')}}" class="form-control" name="minimum" placeholder="@lang('main.minimum')">
                                        <i class="ti-money"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-3">
                                <button type="submit" class="btn search-btn">@lang('main.find_projects')</button>
                            </div>

                        </div>
					</form>
					<!-- /row -->

					<div class="row">
                        @if ($projects->count())
                            @foreach ($projects as $project)
                                <!-- Single Project -->
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <x-project-component :project="$project"/>
                                </div>
                            @endforeach
                        @else
                            <div class="alert alert-info col-md-12 mt-4 text-center" role="alert">
                               <h3><strong> @lang('main.no_result') </strong> </h3>
                            </div>
                        @endif

					</div>
				</div>

			</section>
			<!-- ============================ Project List End ================================== -->
@stop
