
@extends('layouts.layout')

@section('content')

    <!-- ============================ Page Title Start================================== -->
    @php($home="<a href='".url('')."'>".__('main.home_page')."</a>".config('app.mark','/'))
    <x-layouts.title :title="$home.trans('main.about_us')"/>
    <!-- ============================ Page Title End ================================== -->

    <!-- ============================ Our Story Start ================================== -->
    <section>

        <div class="container">

            <!-- row Start -->
            <div class="row align-items-center">

                <div class="col-lg-6 col-md-6">
                    <img src="{{asset('assets/img/sb.png')}}" class="img-fluid" alt="" />
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="story-wrap explore-content">

                        <h2>{{setting('our_story_'.app()->getLocale(),trans('main.our_story'))}}</h2>
                        <p>{{setting('about_us_content_'.app()->getLocale(),trans('main.about_us_content'))}}</p>
                    </div>
                </div>

            </div>
            <!-- /row -->

        </div>

    </section>
    <!-- ============================ Our Story End ================================== -->


@stop
