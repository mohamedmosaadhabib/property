@extends('layouts.layout')
@section('content')
    @php($home="<a href='".url('')."'>".__('main.home_page')."</a>".config('app.mark','/'))
    @php($home.="<a href='".route('developer.index')."'>".__('main.developers')."</a>".config('app.mark','/').$developer->name)
    <x-layouts.title :title="$home"/>
    <!-- ============================ Agency Name Start================================== -->
    <div class="agency-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="agency agency-list shadow-0 my-2 row">

                        <span  class="agency-avatar col-md-2 col-sm-12">
                            <img src="{{$developer->imagePath}}"  alt="">
                        </span>

                        <div class="agency-content  col-md-10  col-sm-12">
                            <div class="agency-name">
                                <h4>{{$developer->name}}</h4>
                            </div>

                            <div class="agency-desc">
                                <p>{!! $developer->description !!}</p>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================ Agency Name ================================== -->

    <!-- ============================ About Agency ================================== -->
    <section class="gray">
        <div class="container">
            <div class="row">

                <!-- property main detail -->
                <div class="col-lg-12 col-md-12 col-sm-12">

                    <!-- Single Block Wrap -->
                    <div class="block-wraps">

                        <div class="block-header">
                            <ul class="nav nav-tabs customize-tab" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="project-tab" data-toggle="tab" href="#project" role="tab" aria-controls="project" aria-selected="true">@lang('main.projects')</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="location-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">@lang('main.location')</a>
                                </li>
                            </ul>
                        </div>

                        <div class="block-body">
                            <div class="tab-content" id="myTabContent">

                                <div class="tab-pane fade show active" id="project" role="tabpanel" aria-labelledby="project-tab">

                                    <!-- row -->
                                    <div class="row">
                                        @foreach ($projects=$developer->projects()->paginate() as $project)
                                        <div class="col-lg-6 col-md-6 col-sm-6   list-layout" id="sale-tab">
                                            <!-- Single Listings -->
                                            <div class="property-listing property-1">

                                                <div class="listing-img-wrapper">
                                                    <a href="{{showDeveloper($project->developer,$project)}}">
                                                        <img src="{{$project->imagePath}}" class="img-fluid mx-auto" alt="" />
                                                    </a>

                                                </div>

                                                <div class="listing-content">

                                                    <div class="listing-detail-wrapper">
                                                        <div class="listing-short-detail">
                                                            <h4 class="listing-name" style="font-size: 14px;"><a href="{{showDeveloper($project->developer,$project)}}">{{$project->name}}</a></h4>
                                                            <span class="listing-location"><i class="ti-location-pin"></i>{{(isset($project->location['region']) ? \App\Models\Region::find((int)$project->location['region'])->name :' ') .' , '.(isset($project->location['city']) ? \App\Models\Region::find((int)$project->location['city'])->name :' ')}}</span>
                                                        </div>

                                                    </div>

                                                    <div class="listing-footer-wrapper">
                                                        <div class="listing-price">
                                                            <h6 class="list-pr">@lang('main.start_price') : {{number_format($project->start_price)}} @lang('main.'.$project->currency??'')</h6>
                                                        </div>
                                                        <div class="listing-detail-btn">
                                                            <a href="{{showDeveloper($project->developer,$project)}}" class="more-btn">@lang('main.more_info')</a>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            <!-- End Single Listings -->

                                        </div>
                                        @endforeach
                                    </div>
                                    <!-- // row -->

                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            {{$projects->links()}}
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade" id="location" role="tabpanel" aria-labelledby="sale-tab">
                                    <x-loactions :regions="$regions" />
                                </div>

                            </div>
                        </div>

                    </div>

                </div>


            </div>
        </div>
    </section>
    <!-- ============================ About Agency End ================================== -->

@stop
