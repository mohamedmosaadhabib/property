@extends('layouts.layout')
@section('content')
@php($home="<a href='".url('')."'>".__('main.home_page')."</a>".config('app.mark','/'))

<div>
    <!-- ============================ Hero Banner  Start================================== -->
    <div class="search-header-banner">
        <div class="container">
            <div class="full-search-2 transparent">
                <div class="hero-search">
                    <h1>@lang('main.search_your_unit')</h1>
                </div>
                <form method="GET" class="hero-search-content">

                    <div class="row">

                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <div class="input-with-icon">
                                    <input type="text" class="form-control" name="search" name="search" value="{{request('search')}}" placeholder="@lang('main.neighborhood')">
                                    <i class="ti-search"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-6">
                            <div class="form-group">
                                <div class="input-with-icon">
                                    <input type="number" step="any" class="form-control" name="minimum"  name="minimum" value="{{request('minimum')}}" placeholder="@lang('main.minimum')">
                                    <i class="ti-money"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-6">
                            <div class="form-group">
                                <div class="input-with-icon">
                                    <input type="number" step="any" class="form-control" name="maximum"  name="maximum" value="{{request('maximum')}}" placeholder="@lang('main.maximum')">
                                    <i class="ti-money"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <div class="input-with-icon">
                                    <select name="bedrooms" name="bedrooms" class="form-control">
                                        <option value="">@lang('main.bedrooms')</option>
                                        @for ($i = 1; $i <= 10; $i++)
                                            <option value="{{$i}}" {{request('bedrooms',null)==$i?' selected ':''}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                    <i class="fas fa-bed"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <div class="input-with-icon">
                                    <select name="bathrooms" name="bathrooms" class="form-control">
                                        <option value="">@lang('main.bathrooms')</option>
                                        @for ($i = 1; $i <= 10; $i++)
                                            <option value="{{$i}}" {{request('bathrooms',null)==$i?' selected ':''}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                    <i class="fas fa-bath"></i>
                                </div>
                            </div>
                        </div>

                    </div>



                    <div class="row">

                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn search-btn-outline">@lang('main.search_result')</button>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
            @if (isset($regions) && $regions->count()>0)
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            @foreach ($regions as $r)
                                @php($routeHandler = routeHandler($word,$r,$region,$city,$street,$type_for))
                                <div class="col-md-3 my-2"><a href="{{route($routeHandler['route'],$routeHandler['data'])}}">{{$r->name}}</a></div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            @if (isset($properties_types) &&$properties_types->count()>0)
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            @foreach ($properties_types as $properties_type)
                                <div class="col-md-3 my-2"><a href="{{route(($word=='rent' ? 'rent':$word),['region'=>$properties_type.'-for-'.$word])}}">{{__('main.'.$properties_type)}}</a></div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <!-- ============================ Hero Banner End ================================== -->

    <!-- =========================== All Property =============================== -->
    <section>
        <div class="container">
            <div class="row">
            <div class="col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('main.home_page')</a></li>
                    <li class="breadcrumb-item"><a href="{{route(($word=='rent' ? 'rent':$word))}}">{{$pageTitle}}</a></li>
                    @isset($region)
                        <li class="breadcrumb-item"><a href="{{handelBackRoute('region')}}">{{$region->name ?? $region}}</a></li>
                    @endisset
                    @isset($city)
                        <li class="breadcrumb-item"><a href="{{handelBackRoute('city')}}">{{$city->name ?? $city}}</a></li>
                    @endisset
                    @isset($street)
                        <li class="breadcrumb-item"><a href="{{handelBackRoute('street')}}">{{$street->name ?? $street}}</a></li>
                    @endisset
                    @isset($type_for)
                        <li class="breadcrumb-item"><a href="{{handelBackRoute('type_for')}}">{{ $type_for}}</a></li>
                    @endisset
                </ol>
            </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h4>@lang('main.total_property_find_is'): <span class="theme-cl">{{$counter}}</span></h4>
                </div>
            </div>

            <div class="row">
            @foreach ($properties as $property)
                <!-- Single Property -->
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="property-listing property-2">

                            <div class="listing-img-wrapper">
                                <div class="list-img-slide">
                                    <div class="click">
                                        <div><a href="{{showProperty($property,$type)}}"><img src="{{url($property->image_path)}}" class="img-fluid mx-auto" alt="" /></a></div>
                                    @foreach ($property->images ?? [] as $image)
                                            <div><a href="{{showProperty($property,$type)}}"><img src="{{url($image)}}" class="img-fluid mx-auto" alt="" /></a></div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="listing-detail-wrapper pb-0">
                                <div class="listing-short-detail">
                                    <h4 class="listing-name"><a href="{{showProperty($property,$type)}}">{{$property->name}}</a><i class="list-status ti-check"></i></h4>
                                </div>
                            </div>
                            <div class="price-features-wrapper">

                                <div class="listing-price-fx">
                                    <ul class="list-style-none">
                                        <li><h6 class="listing-card-info-price price-prefix" content="{{__('main.'.$property->currency)}}">{{(!empty(request('ptypes')) && isset($property->{request('ptypes')}['value'])) ?((number_format($property->{request('ptypes')}['value']??'0')).' '.__('main.'.request('ptypes'))) :  number_format($property->getLowPrice())}}</h6> </li>

                                    </ul>
                                </div>

                                {{--                                <div class="listing-price-fx">--}}
                                {{--                                    <ul>--}}
                                {{--                                        @if ( isset($property->resale['check']) && $property->resale['check'])--}}
                                {{--                                            <li> @lang('main.resale')<h6 class="listing-card-info-price price-prefix" content="{{__('main.'.$property->currency)}}">{{ $property->resale['value']?? '' }}<span class="price-suffix"></span></h6> </li>--}}
                                {{--                                        @endif--}}
                                {{--                                        @foreach (['sale','rent_unfurnished','rent_furnished','rent_semi_furnished'] as $key)--}}
                                {{--                                            @if ( isset($property->{$key}['check']) && $property->{$key}['check'])--}}
                                {{--                                                <li> @lang('main.'.$key)<h6 class="listing-card-info-price price-prefix" content="{{__('main.'.$property->currency)}}">{{ $property->{$key}['value']?? '' }}<span class="price-suffix">@if($key!='sale')/@lang('main.month') @endif</span></h6> </li>--}}
                                {{--                                            @endif--}}
                                {{--                                        @endforeach--}}
                                {{--                                    </ul>--}}
                                {{--                                </div>--}}
                                <div class="list-fx-features">
                                    <div class="listing-card-info-icon">
                                        <span class="inc-fleat inc-bed">{{$property->bed_rooms}} @lang('main.bedrooms')</span>
                                    </div>
                                    <div class="listing-card-info-icon">
                                        <span class="inc-fleat inc-bath">{{$property->bathrooms}} @lang('main.bathrooms')</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- End Single Property -->
                @endforeach

            </div>

            <!-- Pagination -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    {{$properties->links()}}
                </div>
            </div>

        </div>
    </section>
    <!-- =========================== All Property =============================== -->

</div>


@stop
@push('js')
    <script !src="">
        $(function () {

            $('#bedrooms').select2({
                placeholder: "@lang('main.bedrooms')",
                allowClear: true
            });

            // Select Bathrooms
            $('#bathrooms').select2({
                placeholder: "@lang('main.bathrooms')",
                allowClear: true
            });

        })
    </script>
@endpush
