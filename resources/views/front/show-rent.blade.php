@extends('layouts.layout')
@section('content')
        <!-- ============================ Hero Banner  Start================================== -->
        <div class="featured-slick">
            <div class="featured-slick-slide">
                @foreach ($property->images ?? [] as $image)
                    <div><a href="{{url($image)}}" class="mfp-gallery"><img src="{{url($image)}}" style="height: 440px;width: 100%;" class="w-100 img-fluid mx-auto" alt="" /></a></div>
                @endforeach
            </div>
        </div>

        <section class="spd-wrap">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12 col-md-12">

                        <div class="slide-property-detail">
                            <div class="slide-property-first">
                                <div class="pr-price-into">
                                    <h2>{{$property->name}}</h2>
                                    <span>
                                        <i class="lni-map-marker"></i>
                                        @if ($location = \App\Models\Region::find($property->location['region']??0))
                                            {{$location->name}}
                                            /
                                        @endif

                                        @if ($location = \App\Models\Region::find($property->location['city']??0))
                                            {{$location->name}}
                                        @endif
                                        @if ($location = \App\Models\Region::find($property->location['street']??0))
                                        /
                                            {{$location->name}}
                                        @endif
                                    </span>
                                </div>
                            </div>
{{--                            <div class="slide-property-first">--}}
{{--                                <div class="pr-price-into">--}}
{{--                                    <div class="row w-100">--}}
{{--                                    @if ( isset($property->resale['check']) && $property->resale['check'])--}}
{{--                                        <div class="col-md-2"> <h2><span style="font-size: 25px" class="prt-type rent">@lang('main.resale')</span></h2></div>--}}
{{--                                        <div class="col-md-2"><h6>{{__('main.price')}}<br>{{$property->resale['value']}}{{__('main.'.$property->currency)}}</h6></div>--}}
{{--                                        <div class="col-md-2"> <h6>{{__('main.delivery_date')}}<br>{{$property->resale['delivery_date'] ??''}}  <i class="fa fa-calendar-alt"></i></h6></div>--}}
{{--                                        <div class="col-md-3"> <h6>{{__('main.resale_been_paid')}}<br>{{$property->resale['been_paid'] ??''}} {{__('main.'.$property->currency)}}</h6></div>--}}
{{--                                        <div class="col-md-3"> <h6>{{__('main.the_remaining_amounts')}}<br>{{$property->resale['value'] -  $property->resale['been_paid'] ??''}} {{__('main.'.$property->currency)}}</h6></div>--}}
{{--                                    @else--}}
{{--                                        @foreach (['sale','rent_unfurnished','rent_furnished','rent_semi_furnished'] as $key)--}}
{{--                                            @if ( isset($property->{$key}['check']) && $property->{$key}['check'])--}}
{{--                                                <h4 class="col-md-4">{{__('main.'.$property->currency)}}{{$property->{$key}['value']}} <span>@if($key!='sale')/@lang('main.month') @endif</span> <span class="prt-type rent">@lang('main.'.$key)</span></h4>--}}
{{--                                            @endif--}}
{{--                                        @endforeach--}}
{{--                                    @endif--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}


                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- ============================ Hero Banner End ================================== -->


        <!-- ============================ Property Detail Start ================================== -->
        <section class="gray">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <nav class="col-sm-12" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                @if ($region=\App\Models\Region::find($property->location['region']))
                                    @php($routeHandlerRegion = routeHandler($type,$region,$property->property_type.'-for-'.$type))
                                    <li class="breadcrumb-item"><a href="{{route((preg_match('/sale/',$routeHandlerRegion['route'])?'resale':'rent'),$routeHandlerRegion['data'])}}">{{$region->name}}</a></li>
                                    @if ($city=\App\Models\Region::find($property->location['city']))
                                        @php($routeHandlerCity = routeHandler($type,$city,$region,$property->property_type.'-for-'.$type))
                                        <li class="breadcrumb-item"><a href="{{route((preg_match('/sale/',$routeHandlerCity['route'])?'resale':'rent'),$routeHandlerCity['data'])}}">{{$city->name}}</a></li>
                                        @if ($street=\App\Models\Region::find($property->location['street']))
                                            @php($routeHandlerStreet = routeHandler($type,$street,$region,$city,$property->property_type.'-for-'.$type))
                                            <li class="breadcrumb-item"><a href="{{route((preg_match('/sale/',$routeHandlerStreet['route'])?'resale':'rent'),$routeHandlerStreet['data'])}}">{{$city->name}}</a></li>
                                        @endif
                                    @endif
                                @endif


                            </ol>
                        </nav>
                    </div>
                    <!-- property main detail -->
                    <div class="col-lg-8 col-md-12 col-sm-12">

                        <!-- Single Block Wrap -->
                        <div class="block-wrap">

                            <div class="block-header">
                                <h4 class="block-title">@lang('main.unit_info')</h4>
                            </div>

                            <div class="block-body">
                                <ul class="dw-proprty-info">

                                    <li><strong>@lang('main.property_type')</strong>{{$property->property_type}}</li>
                                    <li><strong>@lang('main.property_views')</strong>{{$property->property_views}}</li>
{{--                                    <li><strong>@lang('main.building_number')</strong>{{$property->building_number}}</li>--}}
                                    <li><strong>@lang('main.floor_number')</strong>{{$property->floor_number}}</li>
{{--                                    <li><strong>@lang('main.apartment_number')</strong>{{$property->apartment_number}}</li>--}}
                                    <li><strong> {{ucfirst(__('main.region'))}}</strong>{{($location=\App\Models\Region::find($property->location['region']??0))?$location->name:__('main.deleted')}}</li>
{{--                                    @foreach ($property->location as $key =>  $location)--}}
{{--                                        <li><strong> {{ucfirst(__('main.'.$key))}}</strong>{{($location=\App\Models\Region::find($location))?$location->name:__('main.deleted')}}</li>--}}
{{--                                    @endforeach--}}
                                </ul>
                            </div>
                        </div>
                        <!-- Single Block Wrap -->
                        <div class="block-wrap">

                            <div class="block-header">
                                <h4 class="block-title">@lang('main.price')</h4>
                            </div>

                            <div class="block-body">
                                <ul class="dw-proprty-info">
                                    @if ( isset($property->resale['check']) && $property->resale['check'])
                                        <li> @lang('main.resale')<h6 class="listing-card-info-price price-prefix" content="{{__('main.'.$property->currency)}}">{{ number_format($property->resale['value'])?? '' }}<span class="price-suffix"></span></h6> </li>
                                    @endif
                                    @foreach (['sale','rent_unfurnished','rent_furnished','rent_semi_furnished'] as $key)
                                        @if ( isset($property->{$key}['check']) && $property->{$key}['check'])
                                            <li> @lang('main.'.$key)<h6 class="listing-card-info-price price-prefix" content="{{__('main.'.$property->currency)}}">{{ number_format($property->{$key}['value'])?? '' }}<span class="price-suffix">@if($key!='sale')/@lang('main.month') @endif</span></h6> </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- Single Block Wrap -->
                        <div class="block-wrap">

                            <div class="block-header">
                                <h4 class="block-title">@lang('main.description')</h4>
                            </div>

                            <div class="block-body">
                                <p>{!! $property->description !!}</p>
                            </div>

                        </div>



                        <!-- Single Block Wrap -->
                        <div class="block-wrap">

                            <div class="block-header">
                                <h4 class="block-title">@lang('main.structure')</h4>
                            </div>

                            <div class="block-body">
                                <ul  class="dw-proprty-info">
                                    <li><strong>@lang('main.bedrooms')</strong>{{$property->bed_rooms}}</li>
                                    <li><strong>@lang('main.bathrooms')</strong>{{$property->bathrooms}}</li>
                                @foreach ($property->rooms??[] as $key =>  $room)
                                    @if (isset($room['name']) && isset($room['value']))
                                        <li>
                                            <strong> @lang('main.'.$room['name']??'')</strong>  {{$room['value']??''}}
                                        </li>
                                    @endif
                                @endforeach
                                </ul>
                            </div>

                        </div>


                        <!-- Single Block Wrap -->
                        <div class="block-wrap">

                            <div class="block-header">
                                <h4 class="block-title">@lang('main.address_details')</h4>
                            </div>

                            <div class="block-body">
                                {{$property->address}}
                            </div>

                        </div>
                    </div>

                    <!-- property Sidebar -->
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="page-sidebar">
                            <!-- Agent Detail -->
                            <!-- Agent Detail -->
                            <form action="{{route('contact_property_post',$property)}}" class="agent-widget" method="POST"> @csrf
                                <div class="agent-title">
{{--                                    <div class="agent-photo"><img src="https://avatars.dicebear.com/api/initials/{{$property->id}}.svg?backgroundColors[]=green&radius=50&width=500&height=500" alt=""></div>--}}
                                    <div class="text-success" style="float: none;top: auto !important;transform:none;margin: 0;display: inline-block;padding-{{app()->getLocale()=='ar'?'right':'left'}}: 20px;">
                                        <h4><a href="#">@lang('main.id') : {{$property->id}}</a></h4>
                                        <span style="padding: 10px;background-color: #ff5a5f;color: azure;border-radius: 38px;" ><i style="padding-right: 5px;" class="lni-phone-handset"></i><a href="tel:{{setting('phone')}}" >{{setting('phone')}}</a></span>
                                        <span style="padding: 10px;background-color: #01e675;color: azure;border-radius: 38px;" class="d-md-none d-sm-block"><i style="padding-right: 5px;" class="lni-whatsapp"></i><a href="https://api.whatsapp.com/send?phone={{setting('whatsapp')}}&text={{urlencode(url()->current().' '.$property->description)}}" >{{setting('whatsapp')}}</a></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="name" maxlength="50" required  class="form-control" placeholder="@lang('main.your_name')"/>
                                    <x-errors.validation-error name="name"/>
                                </div>

                                <div class="form-group">
                                    <input type="email" name="email" maxlength="50" required  class="form-control" placeholder="@lang('main.your_email')"/>
                                    <x-errors.validation-error name="email"/>
                                </div>
                                <div class="form-group">
                                    <input type="tel" name="phone" minlength="11" maxlength="11" required class="form-control" placeholder="@lang('main.your_phone')"/>
                                    <x-errors.validation-error name="phone"/>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" minlength="5" required name="message" placeholder="{{__('main.project_contact_us_placeholder')}}"></textarea>
                                    <x-errors.validation-error name="message"/>

                                </div>
                                <button class="btn btn-theme full-width">@lang('main.send_message')</button>
                            </form>
                            <!-- Single Block Wrap -->
                            <div class="block-wrap">

                                <div class="block-header">
                                    <h4 class="block-title">@lang('main.tags')</h4>
                                </div>

                                <div class="block-body">
                                    @foreach ($property->tags ?? [] as $tag)
                                        <span class="prt-type rent"> {{$tag}}</span>
                                    @endforeach
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- ============================ Property Detail End ================================== -->
@stop
