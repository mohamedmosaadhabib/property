@extends('layouts.layout')
@push('css')
    <style>
        .overslider {
            position: absolute;
            display: block;
            width: 70%;
            left: 15%;
            text-align: center;
            margin-bottom: 50px;
            top: 350px;
        }
        @media only screen and (max-width: 600px) {
            .overslider {
            position: absolute;
            display: block;
            width: 80%;
            left: 15%;
            text-align: center;
            margin-bottom: 50px;
            top: 134px;
        }

        }

        .input-with-icon .form-control, .input-with-shadow .form-control, .input-with-shadow .select2-container, .input-with-icon .select2-container {
            border: none;
            border-radius: 5px;
            padding-left: 45px;
            height: 45px;
            background: #ffffff;
            overflow: hidden;
            box-shadow: 0 0 6px 1px rgba(62,28,131,0.1);
            -webkit-box-shadow: 0 0 6px 1px rgba(62,28,131,0.1);
        }
        select.form-control:not([size]):not([multiple]) {
                height: 45px;
            }
            .btn.search-btn {
                background: #fd5332;
                padding: 9px;
                border-radius: 5px;
                box-shadow: 0 5px 24px rgba(31, 37, 59, 0.15);
                color: #ffffff;
                width: 100%;
                font-size: 1.2rem;
                height: 45px;
            }

    </style>
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
@endpush
@push('js')
    <script src="{{asset('js/jquery-ui.js')}}"></script>
    <script>
        $(function () {
            $('#ptypes').select2({ placeholder: "@lang('main.property_types')", allowClear: true });

            $('.autocomplete').autocomplete({
                source: function(request, response)  {
                    var e =$($(this)[0].element[0]);
                    $.ajax({ url:'{{route('autocomplete')}}', data:{ type:e.data('search'), search:e.val() }, success: function (res) { return  response(res); } });
                },
            });

        });
    </script>

@endpush

@section('content')

    <!-- ============================ Hero Banner  Start================================== -->

{{--    <div class="image-cover large-banner" style="background:url('{{asset('assets/css/slider1.jpg')}}')" no-repeat;>--}}
{{--        <div class="container">--}}

{{--            <h1 class="italian-header-capt">@lang('main.home_banner_msg')</h1>--}}
{{--            <div class="full-search-2 italian-search">--}}
{{--                <div class="hero-search-content">--}}

{{--                   <livewire:search-box />--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!--Carousel Wrapper-->
    <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
        <!--Indicators-->
        <ol class="carousel-indicators">
            @foreach ($sliders??[] as $slider)
                <li data-target="#carousel-example-2" data-slide-to="{{$slider->id}}" class=" {{$loop->first?' active ':''}}"></li>
            @endforeach
        </ol>
        <!--/.Indicators-->
        <!--Slides-->
        <div class="carousel-inner" role="listbox">
            @foreach ($sliders??[] as $slider)
            <div class="carousel-item {{$loop->first?' active ':''}}">
                <div class="view">
                    <img style="height: 600px;" class="d-block w-100" src="{{$slider->imagePath}}" alt="{{$slider->imagePath}}"/>
                    <div class="mask rgba-black-light"></div>
                </div>

            </div>
            @endforeach

        </div>
    <!--/.Slides-->
    <!--Controls-->
    <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">@lang('main.previous')</span>
    </a>
    <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">@lang('main.next')</span>
    </a>
    <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->
    <!--/.Carousel Wrapper-->
    <div class="row overslider d-flex justify-content-center">

        <div class="hero-search-wrap full-width" style="background-color:">

            <div class="hero-search-content" data-select2-id="6">
                <div class="mb-3">

                    <ul class="list-group list-inline list-group-horizontal" id="list-tab" role="tablist">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <li style="background-color:#00000075" class="list-group-item list-group-item-action btn search-btn active" id="project-list" data-toggle="list" href="#project" role="tab" aria-controls="home">@lang('main.search_in_project')</li>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <li style="background-color:#00000075" class="list-group-item list-group-item-action btn search-btn" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">@lang('main.search_in_rent')</li>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <li style="background-color:#00000075" class="list-group-item list-group-item-action btn search-btn" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">@lang('main.search_in_sale')</li>
                            </div>

                        </div>
                    </ul>
                </div>

                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="project" role="tabpanel" aria-labelledby="project-list">
                        <form action="{{route('project')}}" method="get">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <div class="input-with-icon">
                                            <input type="text" class="form-control autocomplete" data-search="project" name="search" value="{{request('search')}}" placeholder="@lang('main.project_name')">
                                            <i class="ti-search"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <div class="input-with-icon">
                                            <select name="city" class="form-control " tabindex="-3" aria-hidden="true">
                                                <option value="">@lang('main.all_cities')</option>
                                                @foreach (\App\Models\Region::latest()->get() as $region)
                                                    <option value="{{$region->id}}">{{$region->name}}</option>
                                                @endforeach
                                            </select>
                                            <i class="ti-home"></i>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <div class="input-with-icon">
                                            <select name="unit_type_id" class="form-control">
                                                <option value="">@lang('main.unit_type')</option>
                                                @foreach (\App\Models\UnitTypes::latest()->get() as $type)
                                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                                @endforeach
                                            </select>
                                            <i class="ti-briefcase"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <div class="input-with-icon">
                                            <select class="form-control" name="minimum" value="{{request('minimum')}}" placeholder="@lang('main.minimum')">
                                                <option selected value="">@lang('main.minimum')</option>
                                                @for ($i = floatval(setting('project_start_min_number',100000)); $i <= floatval(setting('project_min_number',1000000)) ; $i+=floatval(setting('project_min_number_plus_rang',50000)))
                                                    <option value="{{$i}}">{{number_format($i)}}</option>
                                                @endfor
                                            </select>
                                            <i class="ti-money"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <div class="input-with-icon">
                                            <select class="form-control" name="maximum" value="{{request('maximum')}}" placeholder="@lang('main.maximum')">
                                                <option selected value="">@lang('main.maximum')</option>
                                                @for ($i = floatval(setting('project_start_max_number',100000)); $i <= floatval(setting('project_max_number',1000000)) ; $i+=floatval(setting('project_max_number_plus_rang',50000)))
                                                    <option value="{{$i}}">{{number_format($i)}}</option>
                                                @endfor
                                            </select>
                                            <i class="ti-money"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn search-btn">@lang('main.search_result')</button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <div class="tab-pane fade " id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">

                        <form action="{{route('rent')}}" method="get">
                            <input type="hidden" name="ptypes" value="rent_unfurnished">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <div class="input-with-icon">
                                            <input type="text" class="form-control autocomplete" data-search="rent" name="search" value="{{request('search')}}" placeholder="@lang('main.search')">
                                            <i class="ti-search"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <div class="input-with-icon">
                                            <select name="city" class="form-control " tabindex="-3" aria-hidden="true">
                                                <option value="">@lang('main.all_cities')</option>
                                                @foreach (\App\Models\Region::latest()->get() as $region)
                                                    <option value="{{$region->id}}">{{$region->name}}</option>
                                                @endforeach
                                            </select>
                                            <i class="ti-home"></i>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-4 col-sm-6"> </div>

                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <div class="input-with-icon">
                                            <select class="form-control" name="minimum" value="{{request('minimum')}}" placeholder="@lang('main.minimum')">
                                                <option selected value="">@lang('main.minimum')</option>
                                                @for ($i = floatval(setting('rent_start_min_number',100000)); $i <= floatval(setting('rent_min_number',1000000)) ; $i+=floatval(setting('rent_min_number_plus_rang',50000)))
                                                    <option value="{{$i}}">{{number_format($i)}}</option>
                                                @endfor
                                            </select>
                                            <i class="ti-money"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <div class="input-with-icon">
                                            <select class="form-control" name="maximum" value="{{request('maximum')}}" placeholder="@lang('main.maximum')">
                                                <option selected value="">@lang('main.maximum')</option>
                                                @for ($i = floatval(setting('rent_start_max_number',100000)); $i <= floatval(setting('rent_max_number',1000000)) ; $i+=floatval(setting('rent_max_number_plus_rang',50000)))
                                                    <option value="{{$i}}">{{number_format($i)}}</option>
                                                @endfor
                                            </select>
                                            <i class="ti-money"></i>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn search-btn">@lang('main.search_result')</button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                    <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
                            <form action="{{route('resale')}}" method="get">
                                <input type="hidden" name="ptypes" value="sale">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <div class="input-with-icon">
                                            <input type="text" class="form-control autocomplete" data-search="sale" name="search" value="{{request('search')}}" placeholder="@lang('main.search')">
                                            <i class="ti-search"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <div class="input-with-icon">
                                            <select name="city" class="form-control " tabindex="-3" aria-hidden="true">
                                                <option value="">@lang('main.all_cities')</option>
                                                @foreach (\App\Models\Region::latest()->get() as $region)
                                                    <option value="{{$region->id}}">{{$region->name}}</option>
                                                @endforeach
                                            </select>
                                            <i class="ti-home"></i>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-4 col-sm-6"> </div>

                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <div class="input-with-icon">
                                            <select class="form-control" name="minimum" value="{{request('minimum')}}" placeholder="@lang('main.minimum')">
                                                <option selected value="">@lang('main.minimum')</option>
                                                @for ($i = floatval(setting('sale_start_min_number',100000)); $i <= floatval(setting('sale_min_number',1000000)) ; $i+=floatval(setting('sale_min_number_plus_rang',50000)))
                                                    <option value="{{$i}}">{{number_format($i)}}</option>
                                                @endfor
                                            </select>
                                            <i class="ti-money"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <div class="input-with-icon">
                                            <select class="form-control" name="maximum" value="{{request('maximum')}}" placeholder="@lang('main.maximum')">
                                                <option selected value="">@lang('main.maximum')</option>
                                                @for ($i = floatval(setting('sale_start_max_number',100000)); $i <= floatval(setting('sale_max_number',1000000)) ; $i+=floatval(setting('sale_max_number_plus_rang',50000)))
                                                    <option value="{{$i}}">{{number_format($i)}}</option>
                                                @endfor
                                            </select>
                                            <i class="ti-money"></i>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn search-btn">@lang('main.search_result')</button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div>
    <!-- ============================ Hero Banner End ================================== -->

    <!-- ============================ Slide Property Start ================================== -->
    <section>
        <div class="container">

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="sec-heading center">
                        <h2>@lang('main.new_project')</h2>
                        <p>@lang('main.home_new_prject')</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="property-slide">
                        @foreach (\App\Models\Project::latest()->with('developer')->take(6)->get() as $project)
                        <!-- Single Property -->
                            <div class="single-items">
                                <div class="property-listing property-2">

                                    <div class="listing-img-wrapper">
                                        <div class="list-img-slide">
                                            <div class="click">
                                                @foreach ($project->photos ??[] as $image)
                                                    <div><a href="{{showDeveloper($project->developer,$project)}}"><img src="{{url($image)}}" class="img-fluid mx-auto" alt="" /></a></div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <span class="property-type"><a style="color:#fff;" href="{{route('developer.show',$project->developer)}}">{{$project->developer->name ?? ''}}</a></span>
                                    </div>

                                    <div class="listing-detail-wrapper pb-0">
                                        <div class="listing-short-detail">
                                            <h4 class="listing-name"><a href="{{showDeveloper($project->developer,$project)}}">{{$project->name}}</a><i class="list-status ti-check"></i></h4>
                                        </div>
                                    </div>
                                    <div class="price-features-wrapper">
                                        <div class="listing-price-fx">
                                            @lang('main.start_price') <h6 class="listing-card-info-price price-prefix"> {{number_format($project->price).' '.trans('main.'.$project->currency)}}</h6>
                                        </div>
{{--                                        <div class="list-fx-features">--}}
{{--                                            <div class="listing-card-info-icon">--}}
{{--                                                <span class="inc-fleat inc-bed">2 Beds</span>--}}
{{--                                            </div>--}}
{{--                                            <div class="listing-card-info-icon">--}}
{{--                                                <span class="inc-fleat inc-bath">2 Bath</span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                    </div>
                                    <div class="price-features-wrapper">

                                        <div class="listing-price-fx">

                                            {{--                                        <br>--}}
                                            {{--                                       @lang('main.down_payment') : {{$project->down_payment.' '.trans('main.'.$project->currency)}}--}}
                                            {{--                                        <br>--}}
                                            {{--                                       @lang('main.installments_years') : {{$project->installments_years}}--}}
                                        </div>
                                    </div>

                                </div>
                            </div>

                            @endforeach

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="text-center mt-4">
                        <a href="{{route('project')}}" class="btn btn-theme-2">@lang('main.browse_more_project')</a>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- ============================ Slide Property End ================================== -->
    <!-- ============================ Property Location Start ================================== -->
    <x-loactions :regions="$regions" />

    <!-- ============================ Property Location End ================================== -->
    <section>
        <div class="container">

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="sec-heading center">
                        <h2>@lang('main.find_blog')</h2>
                        <p>@lang('main.find_blog_sub_title')</p>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach ($blogs as $blog)
                    <!-- Single blog Grid -->
                        <div class="col-lg-4 col-md-6">
                            <div class="blog-wrap-grid">

                                <div class="blog-thumb">
                                    <a href="{{route('single.blog',$blog)}}"><img src="{{$blog->imagePath}}" class="img-fluid" alt="" /></a>
                                </div>

                                <div class="blog-info">
                                    <span class="post-date"><i class="ti-calendar"></i>{{$blog->created_at->format('Y M D')}}</span>
                                </div>

                                <div class="blog-body">
                                    <h4 class="bl-title"><a href="{{route('single.blog',$blog)}}">{{$blog->name}}</a></h4>
                                    <p>{{substr(strip_tags($blog->decription),0,75)}}</p>
                                    <a href="{{route('single.blog',$blog)}}" class="bl-continue">@lang('main.continue')</a>
                                </div>

                            </div>
                        </div>
                @endforeach

            </div>

        </div>
    </section>

@stop
