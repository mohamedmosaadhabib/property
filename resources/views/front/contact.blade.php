@extends('layouts.layout')

@section('content')
    @php($home="<a href='".url('')."'>".__('main.home_page')."</a>".config('app.mark','/'))
    <x-layouts.title :title="$home.trans('main.contact_title')"/>
    <!-- ============================ Agency List Start ================================== -->
    <section>

        <div class="container">

            <!-- row Start -->
            <div class="row">

                <livewire:contact/>
                <div class="col-lg-5 col-md-5">
                    <div class="contact-info">

                        <h2>@lang('main.get_in_touch')</h2>
                        <p>@lang('main.contact_content') </p>

                        <div class="cn-info-detail">
                            <div class="cn-info-icon">
                                <i class="ti-home"></i>
                            </div>
                            <div class="cn-info-content">
                                <h4 class="cn-info-title">@lang('main.reach_us')</h4>
                                {!! setting('address_'.app()->getLocale(),'2512, New Market,Eliza Road, Sincher 80 CA,Canada, USA') !!}
                            </div>
                        </div>

                        <div class="cn-info-detail">
                            <div class="cn-info-icon">
                                <i class="ti-email"></i>
                            </div>
                            <div class="cn-info-content">
                                <h4 class="cn-info-title">@lang('main.drop_a_mail')</h4>
                                {{setting('email','support@drizvato77.com')}}
                            </div>
                        </div>

                        <div class="cn-info-detail">
                            <div class="cn-info-icon">
                                <i class="ti-mobile"></i>
                            </div>
                            <div class="cn-info-content">
                                <h4 class="cn-info-title">@lang('main.call_us')</h4>
                                {{setting('phone','+91 235 548 7548')}}
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <!-- /row -->

        </div>

    </section>
    <!-- ============================ Agency List End ================================== -->
@stop
