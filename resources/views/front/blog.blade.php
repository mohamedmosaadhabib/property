@extends('layouts.layout')
@section('content')

<div class="clearfix"></div>
<!-- ============================================================== -->
<!-- Top header  -->
<!-- ============================================================== -->

<!-- ============================ Page Title Start================================== -->

@php($home="<a href='".url('')."'>".__('main.home_page')."</a>".config('app.mark','/'))
<x-layouts.title :title="$home.__('main.blogs')" :subtitle="__('main.blogs_sub_title')"/>
<!-- ============================ Page Title End ================================== -->

<!-- ============================ Agency List Start ================================== -->
<section>

    <div class="container">

        <div class="row">
            <div class="col text-center">
                <div class="sec-heading center">
                    <h2>@lang('main.latest_news')</h2>
                    <p>@lang('main.latest_news_sub_title')</p>
                </div>
            </div>
        </div>

        <!-- row Start -->
        <div class="row">
            @foreach ($blogs as $blog)
            <!-- Single blog Grid -->
            <div class="col-lg-4 col-md-6">
                <div class="blog-wrap-grid">

                    <div class="blog-thumb">
                        <a href="{{route('single.blog',$blog)}}"><img src="{{$blog->imagePath}}" class="img-fluid" alt="" /></a>
                    </div>

                    <div class="blog-info">
                        <span class="post-date"><i class="ti-calendar"></i>{{$blog->created_at->format('Y M D')}}</span>
                    </div>

                    <div class="blog-body">
                        <h4 class="bl-title"><a href="{{route('single.blog',$blog)}}">{{$blog->name}}</a></h4>
                        <p>{{substr(strip_tags($blog->decription),0,75)}}</p>
                        <a href="{{route('single.blog',$blog)}}" class="bl-continue">@lang('main.continue')</a>
                    </div>

                </div>
            </div>
            @endforeach

        </div>
        <!-- /row -->

        <!-- Pagination -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                {{$blogs->links()}}
            </div>
        </div>

    </div>

</section>
<!-- ============================ Agency List End ================================== -->
@stop
