<div class="modal fade {{$modelClass??''}} " id="{{$id}}" tabindex="{{$tabindex??'-1'}}" role="dialog" aria-labelledby="{{$titleId=uniqid('',false)}}ModalLabel" aria-hidden="{{$hidden??'true'}}">
    <div class="modal-dialog {{$size ??''}}" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @isset($title)
                    <h5 class="modal-title" id="{{$titleId}}ModalLabel">{{$title}}</h5>
                @endisset
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">{{$slot}}</div>
            @isset($footer)
                <div class="modal-footer">
                    {{$footer}}
                </div>
            @endisset
        </div>
    </div>
</div>
