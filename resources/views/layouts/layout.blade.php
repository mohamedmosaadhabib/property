<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"
      dir="{{ str_replace('_', '-', app()->getLocale())=="ar"?'rtl':'ltr' }}" xmlns:wire="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
{{--    <title>{{ config('app.name', 'Laravel') }}</title>--}}
    {!! SEO::generate(true) !!}
 <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" data-turbolinks-permanent />

    <!-- All Plugins Css -->
    <link rel="stylesheet" href="{{asset('assets/css/plugins.css')}}">
    @if  (str_replace('_', '-', app()->getLocale())=="ar" )
        <link href="{{asset('assets/css/rtl/style.css')}}" rel="stylesheet">

        <link rel="stylesheet" href="{{asset('assets/css/rtl/nav.css')}}" />
        <!-- Custom CSS -->
        <link href="{{asset('assets/css/rtl/styles.css')}}" rel="stylesheet">

        <!-- Custom Color Option -->
        <link href="{{asset('assets/css/rtl/colors.css')}}" rel="stylesheet">
    @else
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

        <link rel="stylesheet" href="{{asset('assets/css/nav.css')}}" />
        <!-- Custom CSS -->
        <link href="{{asset('assets/css/styles.css')}}" rel="stylesheet">
        <!-- Custom Color Option -->
        <link href="{{asset('assets/css/colors.css')}}" rel="stylesheet">
    @endif

    @livewireStyles
    @livewireScripts

    @stack('css')
</head>
<body class="green-skin">
    <div id="app">

        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Preloader - style you can find in spinners.css -->
            <!-- ============================================================== -->
            <div id="preloader"><div class="preloader"><span></span><span></span></div></div>
            <!-- ============================================================== -->
            <!-- Top header  -->
            <!-- ============================================================== -->
            <x-layouts.header/>
            <div class="clearfix"></div>
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- Main wrapper - style you can find in pages.scss -->
            <!-- ============================================================== -->
            @yield('content')
            <x-layouts.footer/>

        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script !src="">
        $(function () {
            window.isRtl="{{app()->getLocale()=="ar"}}"==="1";
        })
    </script>
    <script src="{{asset('assets/js/rangeslider.js')}}"></script>
    <script src="{{asset('assets/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/js/aos.js')}}"></script>
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('assets/js/slick.js')}}"></script>
    <script src="{{asset('assets/js/slider-bg.js')}}"></script>
    <script src="{{asset('assets/js/lightbox.js')}}"></script>
    <script src="{{asset('assets/js/imagesloaded.js')}}"></script>
    <script src="{{asset('assets/js/isotope.min.js')}}"></script>
    <script src="{{asset('assets/js/coreNavigation.js')}}"></script>
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    @include('sweetalert::alert')
    @stack('js')

    <!--Start of Tawk.to Script-->
{{--    <script type="text/javascript">--}}
{{--        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();--}}
{{--        (function(){--}}
{{--            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];--}}
{{--            s1.async=true;--}}
{{--            s1.src='https://embed.tawk.to/5f268fba1a544e2a7275c9d2/default';--}}
{{--            s1.charset='UTF-8';--}}
{{--            s1.setAttribute('crossorigin','*');--}}
{{--            s1.localName='{{app()->getLocale()}}';--}}
{{--            s0.parentNode.insertBefore(s1,s0);--}}
{{--        })();--}}
{{--    </script>--}}
    <!--End of Tawk.to Script-->
</body>
</html>
