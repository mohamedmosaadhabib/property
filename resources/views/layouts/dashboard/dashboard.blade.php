<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{ str_replace('_', '-', app()->getLocale())=="ar"?'rtl':'ltr' }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" data-turbolinks-permanent>
    <!-- Custom fonts for this template-->
    <link href="{{asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    @if  (str_replace('_', '-', app()->getLocale())=="ar" )
        <link href="{{asset('css/sb-admin-2.rtl.css')}}" rel="stylesheet">
        <style>
            div.dataTables_wrapper div.dataTables_filter {
                text-align: left !important;
            }
        </style>
    @else
        <link href="{{asset('css/sb-admin-2.css')}}" rel="stylesheet">
    @endif

    @livewireStyles
    @livewireScripts

    @stack('css')
</head>
<body id="page-top">
    <div id="app">
        <div id="wrapper">

            <x-dashboard.sidebar/>
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <x-dashboard.header/>
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                </div>
                <x-dashboard.footer/>
            </div>

        </div>
    </div>


    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top"> <i class="fas fa-angle-up"></i> </a>

    <!-- Logout Modal-->
    @component('modal',['id'=>'logoutModal','title'=>"Ready to Leave?"])
        @slot('footer')
            <button class="btn btn-secondary" type="button" data-dismiss="modal">@lang('main.cancel')</button>
            <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@lang('main.logout')</a>
        @endslot
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        @lang('main.logout_msg')
    @endcomponent



    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('js/sb-admin-2.min.js')}}"></script>
    @include('sweetalert::alert')
    <script>

       $(function () {
           $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
           var url=window.location.href.split('?')[0].split('#')[0];
           $('ul#accordionSidebar .nav-item .nav-link').map(function (link,element) {
               element=$(element);
               if(!element.hasClass('collapsed') && url===element.attr('href')) element.parent().addClass('active');
               else{
                   element.parent().find('.collapse .collapse-item').map(function (key,NavLink) {
                       if(NavLink.href===url) $(NavLink).addClass('active').parent().parent().addClass('show').parent().addClass('active');
                   });
               }
           });

       });
       function readURL(input,image) {
           if (input.files && input.files[0]) {
               var reader = new FileReader();

               reader.onload = function(e) {
                   $('#'+image).attr('src', e.target.result);
               };

               reader.readAsDataURL(input.files[0]); // convert to base64 string
           }
       }
    </script>
    @stack('js')
    @stack('modals')

</body>
</html>
