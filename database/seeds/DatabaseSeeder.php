<?php

use App\Models\{Blog, Developer, Owner, Project, Property, Region, UnitTypes, User};
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $user =User::firstOrCreate([
            "email"=>"admin@app.com",
            "password" => "123456789",
            "name" => "Mohamed Habib",
            "is_admin" => true,
        ]);
        factory(\App\Models\Blog::class,30)->create(['user_id' => $user->id]);

//        factory(\App\Models\Blog::class,30)->create(['user_id' => $user->id]);
        factory(\App\Models\Owner::class,30)->create(['user_id' => $user->id]);
        factory(\App\Models\Developer::class,30)->create(['user_id' => $user->id]);
        factory(\App\Models\UnitTypes::class,30)->create(['user_id' => $user->id]);
        factory(\App\Models\Region::class,5)->create(['user_id' => $user->id,'region_id' => null,'type' => 2])->each(function (Region $region) use ($user) {
            factory(\App\Models\Region::class,5)->create(['user_id' => $user->id,'region_id' => $region->id,'type' => 3])->each(function (Region $city) use ( $user){
                factory(\App\Models\Region::class,5)->create(['user_id' => $user->id,'region_id' => $city->id,'type' => 4]);
            });
        });
        factory(\App\Models\Project::class,30)->create(['user_id' => $user->id]);
        factory(\App\Models\Property::class,100)->create(['user_id' => $user->id]);
        factory(\App\Models\Slider::class,30)->create(['user_id' => $user->id]);
    }
}
