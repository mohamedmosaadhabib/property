<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration{

    public function up() {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->nullableMorphs('contactable');
            $table->string('phone',20)->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('subject')->nullable();
            $table->mediumText('message')->nullable();
            $table->timestampTz('read_at')->nullable();
            $table->softDeletesTz();
            $table->timestampsTz();
        });
    }

    public function down() { Schema::dropIfExists('contacts'); }

}
