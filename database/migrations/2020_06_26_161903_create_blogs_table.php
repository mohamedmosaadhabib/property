<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration{

    public function up(){
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->mediumText('name');
            $table->mediumText('slug');
            $table->text('description');
            $table->string('image');
            $table->unsignedBigInteger('user_id');
            $table->softDeletesTz();
            $table->timestampsTz();
        });
    }


    public function down() { Schema::dropIfExists('blogs'); }

}
