<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->string('property_type')->nullable();
            $table->string('location')->nullable();
            $table->string('property_views')->nullable();
            $table->string('currency',3)->default('EGP');

            $table->string('resale')->nullable();
            $table->string('sale')->nullable();
            $table->string('rent_unfurnished')->nullable();
            $table->string('rent_furnished')->nullable();
            $table->string('rent_semi_furnished')->nullable();

            $table->smallInteger('building_number')->nullable();
            $table->smallInteger('floor_number')->nullable();
            $table->smallInteger('apartment_number')->nullable();
            $table->tinyInteger('bed_rooms')->default(1);
            $table->tinyInteger('bathrooms')->default(1);
            $table->text('address')->nullable();
            $table->text('images')->nullable();
            $table->text('image')->nullable();
            $table->text('description')->nullable();
            $table->text('rooms')->nullable();
            $table->text('tags')->nullable();
            $table->unsignedBigInteger('owner_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->boolean('active')->default(true);
            $table->softDeletesTz();
            $table->timestampsTz();
        });
    }

    public function down() { Schema::dropIfExists('properties'); }
}
