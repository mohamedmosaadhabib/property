<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration{

    public function up(){
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->mediumText('slug');
            $table->mediumText('name');
            $table->dateTimeTz('delivery_date')->nullable();
            $table->text('types')->nullable();
            $table->float('down_payment',30,2)->nullable();
            $table->float('price',30,2)->nullable();
            $table->text('description')->nullable();
            $table->integer('installments_years')->nullable();
            $table->string('image')->nullable();
            $table->string('currency',3)->nullable();
            $table->string('location')->nullable();
            $table->mediumText('photos')->nullable();
            $table->unsignedBigInteger('developer_id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->boolean('active')->default(true);
            $table->softDeletesTz();
            $table->timestampsTz();
        });
    }


    public function down() { Schema::dropIfExists('projects'); }

}
