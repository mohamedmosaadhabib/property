<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('image');
            $table->boolean('is_admin')->default(false);
            $table->string('phone')->nullable();
            $table->string('locale','5')->default(app()->getLocale());
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->softDeletesTz();
            $table->timestampsTz();
        });
    }

    public function down() { Schema::dropIfExists('users'); }

}
