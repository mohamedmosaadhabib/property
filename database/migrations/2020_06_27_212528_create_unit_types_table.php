<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitTypesTable extends Migration{

    public function up(){
        Schema::create('unit_types', function (Blueprint $table) {
            $table->id();
            $table->mediumText('name');
            $table->mediumText('slug');
            $table->unsignedBigInteger('unit_types_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->softDeletesTz();
            $table->timestampsTz();
        });
    }


    public function down() { Schema::dropIfExists('unit_types'); }

}
