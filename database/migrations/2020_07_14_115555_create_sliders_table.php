<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidersTable extends Migration{

    public function up(){
        Schema::create('sliders', function (Blueprint $table) {
            $table->id();
            $table->string('image');
            $table->unsignedBigInteger('order_by');
            $table->unsignedBigInteger('user_id');
            $table->softDeletesTz();
            $table->timestampsTz();
        });
    }


    public function down() { Schema::dropIfExists('sliders'); }

}
