<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegionsTable extends Migration{

    public function up(){
        Schema::create('regions', function (Blueprint $table) {
            $table->id();
            $table->mediumText('name');
            $table->mediumText('slug');
            $table->string('image')->nullable();
            $table->string('show')->default(false);
            $table->tinyInteger('type')->nullable();
            $table->unsignedBigInteger('region_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->softDeletesTz();
            $table->timestampsTz();
        });
    }


    public function down() { Schema::dropIfExists('regions'); }

}
