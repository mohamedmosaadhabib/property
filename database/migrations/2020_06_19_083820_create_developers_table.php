<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevelopersTable extends Migration{

    public function up(){
        Schema::create('developers', function (Blueprint $table) {
            $table->id();
            $table->mediumText('name');
            $table->mediumText('slug');
            $table->string('image')->nullable();
            $table->longText('description')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->boolean('active')->default(true);
            $table->softDeletesTz();
            $table->timestampsTz();
        });
    }


    public function down() { Schema::dropIfExists('developers'); }

}
