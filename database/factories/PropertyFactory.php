<?php

use App\Models\Owner;
use App\Models\Property;
use App\Models\Region;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Property::class, function (Faker $faker) {
    $property_type =[ "apartment", "twin_house", "chalet", "cafe", "office", "i_villa", "penthouse", "studio", "town_house", "ground_floor", "duplex", "villa",];
    $property_views =["landscape", "pool", "sea", "golf", "lake", "open_view", "club", "garden", "nile", ];
    if ($faker->boolean) {
        $sale =[
            "check"=>$faker->boolean,
            "value"=>$faker->randomFloat(2,100000,520000)
        ];

        $resale =[
            "check"=>$faker->boolean,
            "delivery_date"=>$faker->dateTimeBetween('-10 years','+10 years'),
            "value"=>$faker->randomFloat(2,100000,520000),
            "been_paid"=>$faker->randomFloat(2,10000,52000),
        ];
    }else{
        $rent_unfurnished =[
            "check"=>$faker->boolean,
            "value"=>$faker->randomFloat(2,100000,520000)
        ];

        $rent_furnished =[
            "check"=>$faker->boolean,
            "value"=>$faker->randomFloat(2,100000,520000)
        ];

        $rent_semi_furnished =[
            "check"=>$faker->boolean,
            "value"=>$faker->randomFloat(2,100000,520000)
        ];
    }

    return [
        'name' => $faker->name,
        'property_type' => $property_type[rand(0,count($property_type)-1)],
        'location' => [
            "region"=>\App\Models\Region::region()->inRandomOrder()->first()->id,
            "city"=>\App\Models\Region::city()->inRandomOrder()->first()->id,
            "street"=>\App\Models\Region::street()->inRandomOrder()->first()->id,
        ],
        'property_views' => $property_views[rand(0,count($property_views)-1)],
        'currency' => ['usd','egp'][$faker->boolean],
        'resale' => $resale??[
            'check'=>false,
            'delivery_date'=>null,
            'value'=>null,
            'been_paid'=>null,
        ],
        'sale' =>$sale??['check'=>false,'value'=>null] ,
        'rent_unfurnished' =>$rent_unfurnished??['check'=>false,'value'=>null] ,
        'rent_furnished' =>$rent_furnished??['check'=>false,'value'=>null] ,
        'rent_semi_furnished' =>$rent_semi_furnished??['check'=>false,'value'=>null] ,
        'building_number' => $faker->randomFloat(0,100,520),
        'floor_number' => $faker->randomFloat(0,100,500),
        'apartment_number' => $faker->randomFloat(0,100,500),
        'bed_rooms' => $faker->randomFloat(0,1,10),
        'bathrooms' => $faker->randomFloat(0,1,10),
        'address' => [
            "en"=>$faker->address
        ],
        'images' => [
            $faker->imageUrl(),
            $faker->imageUrl(),
            $faker->imageUrl(),
        ],
        'image' => 0,
        'description' => [
            "en"=>$faker->text,
            "ar"=>$faker->text,
        ],
        'rooms' => [
            "name"=>Property::TYPES_ROOMS[rand(0,count(Property::TYPES_ROOMS)-1)],
            "value"=>rand(1,10)
        ],
        'tags' => $faker->words(),
        'user_id' =>\App\Models\User::query()->inRandomOrder()->first()->id,
        'active' => $faker->boolean,
        'created_at' => now(),
        'updated_at' => now(),
        'owner_id' =>Owner::query()->inRandomOrder()->first()->id,
    ];
});
