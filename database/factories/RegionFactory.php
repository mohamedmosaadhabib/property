<?php

use App\Models\Region;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Region::class, function (Faker $faker) {
    return [
        'name' => [
            "ar"=>$faker->unique()->name,
            "en"=>$faker->unique()->name,
        ],
        'image' => $faker->imageUrl(),
        'show' => $faker->boolean,
        'type' => $type =rand(2,4),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
        "region_id"=>optional(Region::query()->inRandomOrder()->first())->id,
        "user_id"=>\App\Models\User::query()->inRandomOrder()->first()->id,
    ];
});
