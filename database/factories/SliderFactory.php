<?php

/** @var Factory $factory */

use App\Models\Slider;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Slider::class, function (Faker $faker) {
    return [
        "image" => $faker->imageUrl(),
        "order_by" => $faker->unique()->numberBetween(1,10000),
        'user_id' =>\App\Models\User::query()->inRandomOrder()->first()->id,
    ];
});
