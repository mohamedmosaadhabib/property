<?php

use App\Models\Owner;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Owner::class, function (Faker $faker) {
    return [
        "name"=>[
            "ar"=>$faker->unique()->name,
            "en"=>$faker->unique()->name
        ],
        "email"=>$faker->unique()->safeEmail,
        "phone"=>$faker->unique()->phoneNumber,
        "user_id" => \App\Models\User::query()->inRandomOrder()->first()->id,
    ];
});
