<?php

use App\Models\Developer;
use App\Models\Project;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Project::class, function (Faker $faker) {
    return [
        "name"=>[
            "ar"=>$faker->unique()->name,
            "en"=>$faker->unique()->name,
        ],
        "delivery_date"=>$faker->dateTimeBetween('+3 years','+5 years'),
        "location"=>[
            "region"=>\App\Models\Region::region()->inRandomOrder()->first()->id,
            "city"=>\App\Models\Region::city()->inRandomOrder()->first()->id,
        ],
        "down_payment"=>$faker->randomFloat(2,10000,10000000),
        "price"=>$faker->randomFloat(2,1000000,10000000),
        "types"=>[],
        "image"=>$faker->imageUrl(),
        "photos"=>[
            $faker->imageUrl(),
            $faker->imageUrl(),
            $faker->imageUrl(),
        ],
        "description"=>[
            "ar"=>$faker->sentences(rand(10,15),true),
            "en"=>$faker->sentences(rand(10,15),true),
        ],
        "developer_id"=>\App\Models\Developer::query()->inRandomOrder()->first()->id,
        "currency"=>['egp','usd'][$faker->boolean]??'usd',
        "installments_years"=>(int)(now()->addYears(rand(5,10))->format('Y')),

        "user_id" => \App\Models\User::query()->inRandomOrder()->first()->id,
    ];
});
