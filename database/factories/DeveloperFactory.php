<?php

use App\Models\Developer;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Developer::class, function (Faker $faker) {
    return [
        "name"=>[
            "en"=>$faker->unique()->word(),
            "ar"=>$faker->unique()->word(),
        ],
        "description"=>[
            "en"=>$faker->sentence(rand(8,10),true),
            "ar"=>$faker->sentence(rand(8,10),true),
        ],
        "image"=>$faker->imageUrl(),
        "active"=>$faker->boolean,
        "user_id" => \App\Models\User::query()->inRandomOrder()->first()->id,
    ];
});
