<?php

use App\Models\UnitTypes;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(UnitTypes::class, function (Faker $faker) {
    return [
        "name" => [
            "ar"=>$faker->unique()->word,
            "en"=>$faker->unique()->word,
        ],
        "unit_types_id"=>$faker->boolean ? optional(UnitTypes::query()->inRandomOrder()->first())->id :null
    ];
});
