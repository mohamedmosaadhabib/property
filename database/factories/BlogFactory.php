<?php

use App\Models\Blog;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Blog::class, function (Faker $faker) {
    return [
        'name' => [
            "ar"=>$faker->name,
            "en"=>$faker->name,
        ],
        'slug' => $faker->slug,
        'description' => [
            "ar"=>$faker->text,
            "en"=>$faker->text
        ],
        'image' => $faker->imageUrl(),
        'user_id' => \App\Models\User::query()->inRandomOrder()->first()->id ?? 1,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
    ];
});
